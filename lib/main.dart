import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/localization/form_builder_localizations.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:homie/client.dart';
import 'package:homie/screens/citas/calificaci%C3%B3n.dart';
import 'package:homie/screens/citas/citas_ejecucion/presupuesto.dart';
import 'package:homie/screens/historial_citas_diagnostico/citas_main.dart';
import 'package:homie/screens/inicio/home.dart';
import 'package:homie/screens/login_screens/login.dart';
import 'package:homie/screens/citas/citas_main.dart';
import 'package:homie/screens/notificaciones/lista_notificaciones.dart';
import 'package:homie/screens/onboarding/onboarding.dart';
import 'package:homie/screens/onboarding/splash.dart';
import 'package:homie/screens/pagos/pagos.dart';
import 'package:homie/screens/pagos/pagos_two.dart';
import 'package:homie/screens/presupuesto/presupuesto.dart';
import 'package:homie/screens/chat/chat_one.dart';
import 'package:homie/screens/trabajos/trabajos.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:flutter_stripe/flutter_stripe.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  Future<bool> checkIfLogged() async {
    OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

    OneSignal.shared.setAppId("72ed6734-e53b-4da5-99b1-1f6c004f6454");

    OneSignal.shared.promptUserForPushNotificationPermission().then((accepted) {
      print("Accepted permission: $accepted");
    });

    Stripe.publishableKey =
        "pk_live_51JxyYkIwBRbC9SlExouX8dwQAz9zLlxxLThOUiRIKnyCEpbTOQBcVTNSdPR1nwAvdK59r3vxsxSYQKO4hPg01r26002KnAWB6Z";
    Stripe.stripeAccountId = "acct_1JxyYkIwBRbC9SlE";
    await Stripe.instance.applySettings();

    return await Client().checkLogged();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: checkIfLogged(),
        builder: (context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const MaterialApp(home: SplashScreen());
          } else {
            return MaterialApp(
              routes: {
                '/loginScreen': (context) => const LoginScreen(),
                '/home': (context) => const HomeScreen(),
                '/citas': (context) => const CitasMainScreen(),
                '/citas/calificar': (context) => const CalificacionVisita(),
                '/pagos': (context) => Pagos(),
                '/chat': (context) => const Chat1(),
                '/notification': (context) => const Notification1(),
                '/citas_historial': (context) => const CitasHistorialScreen(),
                '/citas_ejecucion': (context) => const CitasEjecucion(),
                '/trabajos': (context) => const TrabajosMainScreen(),
                "/trabajos/calificar": (context) => const CalificacionVisita(),
                '/presupuesto': (context) => const PresupuestoMainScreen(),
                '/pago': (context) => const PagosTwo(),
              },
              debugShowCheckedModeBanner: false,
              theme: ThemeData(fontFamily: 'Gilroy'),
              localizationsDelegates: const [
                FormBuilderLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate
              ],
              locale: const Locale('es', 'ES'),
              supportedLocales: const [
                Locale('es', ''),
              ],
              home: snapshot.data != null && snapshot.data == true
                  ? const HomeScreen()
                  : const OnboardingScreen(),
            );
          }
        });
  }
}

void main() {
  runApp(const MyApp());
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark));
}
