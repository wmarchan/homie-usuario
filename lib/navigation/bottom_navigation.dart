import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/chat/chat_one.dart';
import 'package:homie/screens/citas/citas_main.dart';
import 'package:homie/screens/configuraci%C3%B3n/perfil.dart';
import 'package:homie/screens/notificaciones/lista_notificaciones.dart';
import 'package:homie/singletons/AppData.dart';

//Bottom Navigation
class BottomNav extends StatelessWidget {
  const BottomNav({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Container(
            height: 75.h,
            decoration: BoxDecoration(
                color: const Color(0xfff0f0f0),
                borderRadius: BorderRadius.circular(15.r)),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      IconButton(
                        icon: SvgPicture.asset('assets/images/1-item-icon.svg'),
                        //Navigate to List Page
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const CitasMainScreen()),
                          );
                        },
                      ),
                      SizedBox(
                        width: 25.w,
                      ),
                      IconButton(
                        icon: SvgPicture.asset('assets/images/2-item-icon.svg'),
                        //Navigate to Chat Page
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Chat1()),
                          );
                        },
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      IconButton(
                        icon: SvgPicture.asset('assets/images/3-item-icon.svg'),
                        onPressed: () {
                          //Navigate to Notification Page
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Notification1()),
                          );
                        },
                      ),
                      SizedBox(
                        width: 28.w,
                      ),
                      GestureDetector(
                        //Navigate to Profile Page
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Perfil()),
                          );
                        },
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Container(
                              width: 32.w,
                              height: 32.w,
                              decoration: const BoxDecoration(
                                color: Color(0xff009ade),
                                shape: BoxShape.circle,
                              ),
                            ),
                            Container(
                              width: 26.w,
                              height: 26.w,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: NetworkImage(
                                      '${appData.usuario.user!.foto}'),
                                  fit: BoxFit.fitHeight,
                                ),
                                shape: BoxShape.circle,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Positioned(
              left: 0,
              right: 0,
              bottom: 35,
              child: Center(child: const HomeButton()))
        ],
      ),
    );
  }
}
