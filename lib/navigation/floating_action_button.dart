import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:homie/screens/inicio/home.dart';

//Floating Action Button
class HomeButton extends StatelessWidget {
  const HomeButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
        );
      },
      child: SvgPicture.asset('assets/images/home-icon.svg'),
      elevation: 2.0,
      backgroundColor: Color(0xff21409a),
    );
  }
}
