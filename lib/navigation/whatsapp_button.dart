import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:homie/screens/inicio/home.dart';
import 'package:url_launcher/url_launcher.dart';

//Floating Action Button
class WhatsappButton extends StatelessWidget {
  const WhatsappButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        launch('https://wa.me/+525512834011');
      },
      child: Image.asset("assets/images/whatsapp.png"),
      elevation: 2.0,
    );
  }
}
