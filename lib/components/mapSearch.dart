import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:location/location.dart' as gps;
import 'package:geocoding/geocoding.dart' as geocoding;
import 'package:motion_toast/motion_toast.dart';

class MapSearch extends StatefulWidget {
  GlobalKey<FormBuilderState> formKey;
  MapSearch({Key? key, required this.formKey}) : super(key: key);

  @override
  State<MapSearch> createState() => _MapSearchState();
}

class _MapSearchState extends State<MapSearch> {
  @override
  void initState() {
    super.initState();
    getLocation();
  }

  final places =
      GoogleMapsPlaces(apiKey: "AIzaSyDfxwxpQsOSKNlIzqPe-KBlTCtwQEt_2xo");

  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> markers = {};
  TextEditingController controller = TextEditingController();
  gps.LocationData? locationData;

  void selectLocation(Prediction prediction) async {
    var location =
        await places.getDetailsByPlaceId(prediction.placeId.toString());
    Marker marker = Marker(
        draggable: true,
        onDragEnd: (pos) async {
          var locations = await geocoding.placemarkFromCoordinates(
              pos.latitude, pos.longitude);

          controller.text =
              '${locations.first.street}, ${locations.first.locality}, ${locations.first.country}';
          widget.formKey.currentState!.fields["map"]!.didChange(PlacesSearchResult(
              reference: "",
              name: "",
              placeId: "",
              formattedAddress:
                  '${locations.first.street}, ${locations.first.locality}, ${locations.first.country}',
              geometry: Geometry(
                  location: Location(lat: pos.latitude, lng: pos.longitude))));
        },
        markerId: MarkerId("1"),
        position: LatLng(location.result.geometry!.location.lat,
            location.result.geometry!.location.lng),
        infoWindow: InfoWindow(title: location.result.name));
    setState(() {
      markers = {marker};
    });
    final GoogleMapController mapController = await _controller.future;

    mapController.moveCamera(CameraUpdate.newLatLng(marker.position));

    MotionToast.info(
            title: Text("Ubicación seleccionada"),
            description: Text(
                "Puedes arrastrar el marcador para cambiar la ubicación exacta"))
        .show(context);
  }

  void getLocation() async {
    //get user location
    gps.Location location = new gps.Location();

    bool _serviceEnabled;
    gps.PermissionStatus _permissionGranted;
    gps.LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == gps.PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != gps.PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await location.getLocation();

    var locations = await geocoding.placemarkFromCoordinates(
        _locationData.latitude!.toDouble(),
        _locationData.longitude!.toDouble());

    Marker marker = Marker(
        draggable: true,
        onDragEnd: (pos) async {
          var locations = await geocoding.placemarkFromCoordinates(
              pos.latitude, pos.longitude);

          controller.text =
              '${locations.first.street}, ${locations.first.locality}, ${locations.first.country}';
          widget.formKey.currentState!.fields["map"]!.didChange(PlacesSearchResult(
              reference: "",
              name: "",
              placeId: "",
              formattedAddress:
                  '${locations.first.street}, ${locations.first.locality}, ${locations.first.country}',
              geometry: Geometry(
                  location: Location(lat: pos.latitude, lng: pos.longitude))));
        },
        markerId: MarkerId("1"),
        position: LatLng(_locationData.latitude!.toDouble(),
            _locationData.longitude!.toDouble()),
        infoWindow: InfoWindow(
            title:
                '${locations.first.street}, ${locations.first.locality}, ${locations.first.country}'));
    setState(() {
      markers = {marker};
      locationData = _locationData;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          flex: 0,
          child: Container(
            width: 319.0.w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: const Color(0xFFF9F9FC),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.16),
                  offset: Offset(5.0, 3.0),
                  blurRadius: 6.0,
                ),
              ],
            ),
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: 8.0.w, vertical: 18.0.h),
              //inside the card
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.0.w),
                    child: Text("Dirección del Servicio",
                        style: TextStyle(
                          color: Color(0xff3a3f47),
                          fontSize: 16.sp,
                          fontWeight: FontWeight.w800,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0.16,
                        )),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  FormBuilderField(
                    name: 'map',
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(context),
                    ]),
                    onChanged: (val) {},
                    builder: (FormFieldState field) {
                      return Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25.0),
                              border: field.hasError
                                  ? Border.all(color: Colors.red)
                                  : null),
                          child: TextField(
                              controller: controller,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(38.r),
                                ),
                                hintText: 'Dirección',
                              ),
                              onTap: () async {
                                const kGoogleApiKey =
                                    "AIzaSyDfxwxpQsOSKNlIzqPe-KBlTCtwQEt_2xo";

                                Prediction? p = await PlacesAutocomplete.show(
                                    context: context,
                                    offset: 0,
                                    radius: 1000,
                                    types: [],
                                    strictbounds: false,
                                    region: "mx",
                                    apiKey: kGoogleApiKey,
                                    mode: Mode.overlay, // Mode.fullscreen
                                    language: "es",
                                    components: [
                                      Component(Component.country, "mx")
                                    ]);

                                selectLocation(p!);
                                field.didChange(p);
                                controller.text = p.description.toString();
                              }));
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
        SizedBox(height: 10.h),
        if (locationData != null)
          Container(
            width: 319.0.w,
            height: 250.0.w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: const Color(0xFFF9F9FC),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.16),
                  offset: Offset(5.0, 3.0),
                  blurRadius: 6.0,
                ),
              ],
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 8.0.w, vertical: 8.0.h),
              //inside the card
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: GoogleMap(
                  initialCameraPosition: CameraPosition(
                    target: LatLng(
                      markers.first.position.latitude,
                      markers.first.position.longitude,
                    ),
                    zoom: 16,
                  ),
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                  gestureRecognizers: Set()
                    ..add(Factory<EagerGestureRecognizer>(
                        () => EagerGestureRecognizer())),
                  markers: markers,
                  padding: EdgeInsets.only(bottom: 0),
                  zoomControlsEnabled: false,
                  myLocationEnabled: true,
                  myLocationButtonEnabled: true,
                ),
              ),
            ),
          ),
      ],
    );
  }
}
