import 'dart:convert';
import 'dart:typed_data';

class AppData {
  static final AppData _appData = new AppData._internal();

  Usuario usuario = new Usuario();

  factory AppData() {
    return _appData;
  }

  AppData._internal();

  fromJson(Map<String, dynamic> json) {
    this.usuario = Usuario.fromJson(json);
  }

  Map<String, dynamic> toJson() {
    return this.usuario.toJson();
  }
}

final appData = AppData();

/// user : {"id":1,"nombre":"Demo","apellido":"User","email":"wilmeralejandromarchanrojas@gmail.com","foto":"https://placekitten.com/600/900","telefono":"12345678","fechaNacimiento":"1990-01-01T00:00:00.000Z","tia":"123456","puntos":0,"saldo":0,"notificaciones":true,"emails":true,"codigo_referido":"ABCDEF","Acceso":{"id":2,"nombre":"admin","createdAt":"1990-01-01T00:00:00.000Z","updatedAt":"1990-01-01T00:00:00.000Z"},"Membresium":{"id":1,"name":"Primera Clase","shortDesc":"Activa tu membresía y recibe una serie de beneficios","longDesc":"Activa tu membresía Primera Clase con una recarga mínima de $1000 al mes y obtén $1,100 en tu billetera virtual ademas de una serie de beneficios exclusivos:\n\n- Adquiere un Kit Viajero a precio especial\n- Recargas en billetera virtual mas un abono extra del 10% \n- Acumula dinero de tus referidos \n- Acumula puntos de tus compras a la carta\n- Sorpresa el día de tu cumpleaños \n- Promociones exclusivas","imageUrl":null,"precio":null,"createdAt":"1990-01-01T00:00:00.000Z","updatedAt":"1990-01-01T00:00:00.000Z"}}
/// access_token : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJ3aWxtZXJhbGVqYW5kcm9tYXJjaGFucm9qYXNAZ21haWwuY29tIiwiaWF0IjoxNjMwMDkwMDY2LCJleHAiOjE2MzAxNzY0NjZ9.GTozhHRtrSEyNXMH0shdACzoNzzqstPTy81QI4dWK2s"
/// expires_in : 86400

class Usuario {
  User? _user;
  String? _accessToken;
  int? _expiresIn;

  User? get user => _user;

  String? get accessToken => _accessToken;

  int? get expiresIn => _expiresIn;

  set user(data) {
    _user = data;
  }

  Usuario({User? user, String? accessToken, int? expiresIn}) {
    _user = user;
    _accessToken = accessToken;
    _expiresIn = expiresIn;
  }

  Usuario.fromJson(dynamic json) {
    _user = json['user'] != null ? User.fromJson(json['user']) : null;
    _accessToken = json['access_token'];
    _expiresIn = json['expires_in'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_user != null) {
      map['user'] = _user?.toJson();
    }
    map['access_token'] = _accessToken;
    map['expires_in'] = _expiresIn;
    return map;
  }
}

/// id : 1
/// nombre : "Demo"
/// apellido : "User"
/// email : "wilmeralejandromarchanrojas@gmail.com"
/// foto : "https://placekitten.com/600/900"
/// telefono : "12345678"
/// fechaNacimiento : "1990-01-01T00:00:00.000Z"
/// tia : "123456"
/// puntos : 0
/// saldo : 0
/// notificaciones : true
/// emails : true
/// codigo_referido : "ABCDEF"
/// Acceso : {"id":2,"nombre":"admin","createdAt":"1990-01-01T00:00:00.000Z","updatedAt":"1990-01-01T00:00:00.000Z"}
/// Membresium : {"id":1,"name":"Primera Clase","shortDesc":"Activa tu membresía y recibe una serie de beneficios","longDesc":"Activa tu membresía Primera Clase con una recarga mínima de $1000 al mes y obtén $1,100 en tu billetera virtual ademas de una serie de beneficios exclusivos:\n\n- Adquiere un Kit Viajero a precio especial\n- Recargas en billetera virtual mas un abono extra del 10% \n- Acumula dinero de tus referidos \n- Acumula puntos de tus compras a la carta\n- Sorpresa el día de tu cumpleaños \n- Promociones exclusivas","imageUrl":null,"precio":null,"createdAt":"1990-01-01T00:00:00.000Z","updatedAt":"1990-01-01T00:00:00.000Z"}

class User {
  String? _id;
  String? _nombre;
  String? _apellido;
  String? _email;
  String? _foto;
  String? _telefono;
  bool? _notificaciones;
  bool? _emails;
  Acceso? _acceso;

  String? get id => _id;

  String? get nombre => _nombre;

  String? get apellido => _apellido;

  String? get email => _email;

  String? get foto => _foto;

  String? get telefono => _telefono;

  bool? get notificaciones => _notificaciones;

  bool? get emails => _emails;

  Acceso? get acceso => _acceso;

  User({
    String? id,
    String? nombre,
    String? apellido,
    String? email,
    String? foto,
    String? telefono,
    bool? notificaciones,
    bool? emails,
    Acceso? acceso,
  }) {
    _id = id;
    _nombre = nombre;
    _apellido = apellido;
    _email = email;
    _foto = foto;
    _telefono = telefono;
    _notificaciones = notificaciones;
    _emails = emails;
    _acceso = acceso;
  }

  User.fromJson(dynamic json) {
    _id = json['id'];
    _nombre = json['nombre'];
    _apellido = json['apellido'];
    _email = json['email'];
    _foto = json['foto'];
    _telefono = json['telefono'];

    _notificaciones = json['notificaciones'];
    _emails = json['emails'];
    _acceso = json['Acceso'] != null ? Acceso.fromJson(json['Acceso']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = _id;
    map['nombre'] = _nombre;
    map['apellido'] = _apellido;
    map['email'] = _email;
    map['foto'] = _foto;
    map['telefono'] = _telefono;

    map['notificaciones'] = _notificaciones;
    map['emails'] = _emails;
    if (_acceso != null) {
      map['Acceso'] = _acceso?.toJson();
    }

    return map;
  }
}

/// id : 2
/// nombre : "admin"
/// createdAt : "1990-01-01T00:00:00.000Z"
/// updatedAt : "1990-01-01T00:00:00.000Z"

class Acceso {
  int? _id;
  String? _nombre;
  String? _createdAt;
  String? _updatedAt;

  int? get id => _id;

  String? get nombre => _nombre;

  String? get createdAt => _createdAt;

  String? get updatedAt => _updatedAt;

  Acceso({int? id, String? nombre, String? createdAt, String? updatedAt}) {
    _id = id;
    _nombre = nombre;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
  }

  Acceso.fromJson(dynamic json) {
    _id = json['id'];
    _nombre = json['nombre'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = _id;
    map['nombre'] = _nombre;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    return map;
  }
}
