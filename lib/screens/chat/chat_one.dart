// ignore_for_file: non_constant_identifier_names

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:homie/client.dart';
import 'package:homie/models/chat/chat.dart';
import 'package:homie/models/chat/usuario.dart' as chatUser;
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';

import 'package:homie/screens/chat/chat_two.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:homie/singletons/AppData.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:homie/navigation/whatsapp_button.dart';

// import 'package:homie/screens/citas/citas_two.dart';

class Chat1 extends StatefulWidget {
  const Chat1({Key? key}) : super(key: key);

  @override
  _CitasMainScreenState createState() => _CitasMainScreenState();
}

class _CitasMainScreenState extends State<Chat1> {
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: const Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
        // ignore: prefer_const_constructors
        gradient: LinearGradient(
          colors: [Color(0xff83dcff), Color(0xffeffdff)],
          stops: [0, 1],
          begin: Alignment(-1.00, -0.02),
          end: Alignment(1.00, 0.02),
        ),
      ),
      child: SafeArea(
        child: Scaffold(
          key: _globalKey,
          endDrawer: Navbar(),
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              //NAVIGATING BACK TO THE HOMEPAGE
              onPressed: () => Navigator.of(context).pop(),
            ),
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 20.0.w),
                child: GestureDetector(
                  //THIS FUNCTION WILL OPEN MENU DRAWER
                  onTap: () {
                    _globalKey.currentState?.openEndDrawer();
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 58.41377258300781.w,
                        height: 57.504638671875.w,
                        decoration: const BoxDecoration(
                          color: Color(0xff009ade),
                          shape: BoxShape.circle,
                        ),
                      ),
                      SvgPicture.asset('assets/images/menu-icon.svg'),
                    ],
                  ),
                ),
              ),
            ],
            toolbarHeight: 120.h,
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 16.0.w),
                child: Text("Chat",
                    style: TextStyle(
                      color: const Color(0xff00013c),
                      fontSize: 23.sp,
                      fontWeight: FontWeight.w900,
                      fontStyle: FontStyle.normal,
                    )),
              ),
              SizedBox(
                height: 18.h,
              ),
              Column(
                children: [],
              ),

              /// SEARCH BAR Start here
              Center(
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 15),
                  width: 340,
                  height: 45,
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Color(0xff707070),
                        width: 2,
                      ),
                      color: const Color(0xffffffff),
                      borderRadius: BorderRadius.circular(30.r)),
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(10.w, 0.h, 0.w, 0.h),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        // SvgPicture.asset('assets/images/searchicon.svg'),
                        Expanded(
                          child: TextField(
                            decoration: const InputDecoration(
                                prefixIcon: Icon(
                                  Icons.search,
                                  color: Color(0xff000000),
                                ),
                                border: InputBorder.none,
                                hintText: 'Buscar',
                                fillColor: Color(0xff000000)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

              Expanded(
                child: Container(
                  width: 375.w,
                  decoration: const BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0x7c000000),
                          offset: Offset(5, -3),
                          blurRadius: 6,
                          spreadRadius: 0)
                    ],
                  ),
                  child: Container(
                    width: 43.w,
                    height: 188.h,
                    margin: EdgeInsets.fromLTRB(10, 15, 10, 34),
                    decoration: new BoxDecoration(
                      color: Color(0xffffffff),
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0x29000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                            spreadRadius: 0)
                      ],
                    ),
                    child: FutureBuilder(
                        future: Client().getChats(),
                        builder: (context, AsyncSnapshot<List<Chat>> snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          if (!snapshot.hasData || snapshot.data!.length == 0) {
                            return Center(
                              child: Text('No hay chats'),
                            );
                          }

                          return ListView.builder(
                            itemCount: snapshot.data!.length,
                            itemBuilder: (BuildContext context, int index) {
                              final Chat chat = snapshot.data![index];
                              chatUser.Usuario? usuario =
                                  chat.usuario1Id == appData.usuario.user!.id
                                      ? chat.usuario2
                                      : chat.usuario1;

                              return GestureDetector(
                                onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) => Chat2(chat: chat),
                                  ),
                                ),
                                child: Container(
                                  margin: EdgeInsets.symmetric(
                                      vertical: 10.h, horizontal: 12.w),
                                  width: 20.241455078125.w,
                                  decoration: BoxDecoration(
                                    color: const Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(7),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0xb3eaecf3),
                                          offset: Offset(0, 10),
                                          blurRadius: 20,
                                          spreadRadius: 0)
                                    ],
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: 67.32275390625.w,
                                          height: 82.32275390625.h,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                                image: NetworkImage(usuario!
                                                    .foto
                                                    .toString()), // User profile for Message chat
                                                fit: BoxFit.cover),
                                          ),
                                        ),
                                        SizedBox(width: 10.w),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(
                                                height: 10.h,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Expanded(
                                                    flex: 3,
                                                    child: Text(
                                                      '${usuario.nombre} ${usuario.apellido}', //User Name for MEssage chat
                                                      style: TextStyle(
                                                        color: const Color(
                                                            0xff222b45),
                                                        fontSize: 12.sp,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 60.w,
                                                  ),
                                                  if (chat.mensajes != null &&
                                                      chat.mensajes!.isNotEmpty)
                                                    Text(
                                                      timeago.format(
                                                          DateTime.parse(chat
                                                              .mensajes!
                                                              .first
                                                              .createdAt
                                                              .toString()),
                                                          locale:
                                                              "es"), // MEssage time
                                                      style: TextStyle(
                                                        color: const Color(
                                                            0xff8f9bb3),
                                                        fontSize: 12.sp,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                      ),
                                                    ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10.h,
                                              ),
                                              if (chat.mensajes != null &&
                                                  chat.mensajes!.isNotEmpty)
                                                Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.50,
                                                  child: Text(
                                                    chat.mensajes!.first
                                                        .contenido
                                                        .toString(), //Message Text
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 2,
                                                    style: TextStyle(
                                                      color: const Color(
                                                          0xff8f9bb3),
                                                      fontSize: 12.sp,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontStyle:
                                                          FontStyle.normal,
                                                    ),
                                                  ),
                                                ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          );
                        }),
                  ),
                ),
              ),
            ],
          ),

          //BOTTOM BAR WITH FLOATING BTN
          floatingActionButton: WhatsappButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: const BottomNav(),
          //Bottom bar ended
        ),
      ),
    );
  }
}
