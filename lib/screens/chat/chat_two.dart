import 'dart:io';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:homie/client.dart';
import 'package:homie/models/chat/chat.dart';
import 'package:homie/models/chat/mensaje.dart';
import 'package:homie/models/chat_message/chat_message.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:homie/singletons/AppData.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:socket_io_client/socket_io_client.dart';
import 'package:full_screen_image_null_safe/full_screen_image_null_safe.dart';

class Chat2 extends StatefulWidget {
  const Chat2({Key? key, required this.chat}) : super(key: key);
  final Chat chat;

  @override
  _CitasMainScreenState createState() => _CitasMainScreenState();
}

class _CitasMainScreenState extends State<Chat2> {
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  TextEditingController controller = TextEditingController();
  ScrollController _scrollController = ScrollController();

  String ChatTitle = 'Chat';
  bool loading = true;
  bool pickEmoji = false;

  void toggleEmoji() {
    setState(() {
      pickEmoji = !pickEmoji;
    });
  }

  void sendImage() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

    var message = await Client().sendMessage(FormData.fromMap({
      "file": await MultipartFile.fromFile(image!.path, filename: image.name),
      "tipo": "image",
      "ChatId": widget.chat.id,
      "UsuarioId": appData.usuario.user!.id,
    }));
    socket.emit('send_message', message!.toJson());
  }

  ChatMessage? chatMessage;
  late IO.Socket socket;
  void sendMessage() async {
    if (controller.text.isEmpty) return;

    var message = await Client().sendMessage({
      "contenido": controller.text,
      "tipo": "text",
      "ChatId": widget.chat.id,
      "UsuarioId": appData.usuario.user!.id,
    });
    controller.clear();
    socket.emit('send_message', message!.toJson());
  }

  void getChatMessages() async {
    socket = IO.io(
        baseUrl.replaceAll("3000", "3001"),
        OptionBuilder()
            .setQuery({"chatID": widget.chat.id})
            .setTransports(['websocket'])
            .disableAutoConnect()
            .enableForceNew()
            .build());

    socket.connect();
    socket.onConnectError((data) {
      print('socket error $data');
    });
    socket.onConnect((_) {
      print('connect');
    });
    var chat = await Client().getChatMessages(widget.chat.id);
    setState(() {
      loading = false;
      chatMessage = chat;
    });
    Future.delayed(Duration(milliseconds: 100), () {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    });
    socket.on('receive_message', (data) {
      print(data);
      setState(() {
        if (chatMessage != null) {
          chatMessage!.messages!.add(Mensaje.fromJson(data));
        }
      });
      Future.delayed(Duration(milliseconds: 100), () {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          curve: Curves.easeOut,
          duration: const Duration(milliseconds: 300),
        );
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getChatMessages();
  }

  @override
  dispose() {
    socket.disconnect();
    socket.dispose();
    super.dispose();
  }

  _chatbubble(Mensaje message, bool isMe, user) {
    if (isMe) {
      return Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              padding: const EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Color(0xff009ade),
                boxShadow: const [
                  BoxShadow(
                      color: Color(0x40939393),
                      offset: Offset(0, 5),
                      blurRadius: 8,
                      spreadRadius: 0),
                ],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  message.tipo == "text"
                      ? ConstrainedBox(
                          constraints: BoxConstraints(
                            maxWidth: MediaQuery.of(context).size.width * 0.6,
                          ),
                          child: Container(
                            margin: EdgeInsets.fromLTRB(8, 0, 0, 0),
                            child: Text(message.contenido.toString(),
                                maxLines: null,
                                style: TextStyle(
                                  color: Color(0xffffffff),
                                  fontSize: 10.sp,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                )),
                          ),
                        )
                      : FullScreenWidget(
                          child: Container(
                              width: 150.w,
                              padding: EdgeInsets.all(5),
                              child:
                                  Image.network(message.contenido.toString())),
                        ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SvgPicture.asset(
                            'assets/images/checklist.svg', // Message is sent or not icon
                            color: Color(0xffffffff)),
                        SizedBox(
                          width: 4.w,
                        ),
                        Text(
                            DateFormat.jm().format(
                                DateTime.parse(message.createdAt.toString())
                                    .toLocal()), // MESSAGE TIME
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              color: Color(0xffffffff),
                              fontSize: 8.sp,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
                width: 50.w,
                height: 50.h,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(user.foto
                            .toString()), // User profile for Message chat
                        fit: BoxFit.cover),
                    shape: BoxShape.circle,
                    color: Color(0xffc2c2c2))),
          ],
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
                width: 50.w,
                height: 50.h,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(user.foto
                            .toString()), // User profile for Message chat
                        fit: BoxFit.cover),
                    shape: BoxShape.circle,
                    color: Color(0xffc2c2c2))),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 3),
              child: Container(
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Color(0xffffffff),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0x40939393),
                        offset: Offset(0, 5),
                        blurRadius: 8,
                        spreadRadius: 0),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      message.tipo == "text"
                          ? ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth:
                                    MediaQuery.of(context).size.width * 0.6,
                              ),
                              child: Container(
                                width: double.infinity,
                                margin: EdgeInsets.fromLTRB(8, 0, 0, 0),
                                child: Text(message.contenido.toString(),
                                    maxLines: null,
                                    style: TextStyle(
                                      color: Color(0xff6f6f6f),
                                      fontSize: 10.sp,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )),
                              ),
                            )
                          : FullScreenWidget(
                              child: Container(
                                  width: 150.w,
                                  padding: EdgeInsets.all(5),
                                  child: Image.network(
                                      message.contenido.toString())),
                            ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 10, 2),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                                DateFormat.jm().format(
                                    DateTime.parse(message.createdAt.toString())
                                        .toLocal()),
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  color: Color(0xffacacac),
                                  fontSize: 8.sp,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                )),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return WillPopScope(
      onWillPop: () {
        if (pickEmoji) {
          setState(() {
            pickEmoji = false;
          });
          return Future.value(false);
        } else {
          return Future.value(true);
        }
      },
      child: Container(
        decoration: BoxDecoration(
            // ignore: prefer_const_constructors
            gradient: LinearGradient(
          colors: [Color(0xff83dcff), Color(0xffeffdff)],
          stops: [0, 1],
          begin: Alignment(-1.00, -0.02),
          end: Alignment(1.00, 0.02),
        )),
        child: SafeArea(
          child: Scaffold(
            key: _globalKey,
            endDrawer: Navbar(),

            backgroundColor: Colors.transparent,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: IconButton(
                icon: const Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                ),
                //NAVIGATING BACK TO THE HOMEPAGE
                onPressed: () => Navigator.of(context).pop(),
              ),
              actions: <Widget>[
                Padding(
                  padding: EdgeInsets.only(right: 20.0.w),
                  child: GestureDetector(
                    //THIS FUNCTION WILL OPEN MENU DRAWER
                    onTap: () {
                      _globalKey.currentState?.openEndDrawer();
                    },
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          width: 58.41377258300781.w,
                          height: 57.504638671875.w,
                          decoration: const BoxDecoration(
                            color: Color(0xff009ade),
                            shape: BoxShape.circle,
                          ),
                        ),
                        SvgPicture.asset('assets/images/menu-icon.svg'),
                      ],
                    ),
                  ),
                ),
              ],
              toolbarHeight: 120.h,
            ),
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 16.0.w),
                  child: Text(ChatTitle,
                      style: TextStyle(
                        color: Color(0xff00013c),
                        fontSize: 23.sp,
                        fontWeight: FontWeight.w900,
                        fontStyle: FontStyle.normal,
                      )),
                ),
                SizedBox(
                  height: 18.h,
                ),

                // Second Container is start here

                Expanded(
                  child: Container(
                    width: 375.w,
                    decoration: BoxDecoration(
                      color: Color(0xffffffff),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25)),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0x7c000000),
                            offset: Offset(5, -3),
                            blurRadius: 6,
                            spreadRadius: 0)
                      ],
                    ),
                    child: Container(
                      margin: EdgeInsets.fromLTRB(15, 10, 15, 32),
                      width: 143.w,
                      height: 100.h,
                      decoration: new BoxDecoration(
                        color: Color(0xffffffff),
                        borderRadius: BorderRadius.circular(16),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x29000000),
                              offset: Offset(0, 3),
                              blurRadius: 6,
                              spreadRadius: 0),
                        ],
                      ),
                      child: Column(
                        children: [
                          Expanded(
                            child: Column(
                              children: [
                                // LIST VIEW FOR CHAT BOX
                                Expanded(
                                    child: loading
                                        ? const Center(
                                            child: CircularProgressIndicator(),
                                          )
                                        : ListView.builder(
                                            controller: _scrollController,
                                            itemCount:
                                                chatMessage?.messages?.length ??
                                                    0,
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              final Mensaje message =
                                                  chatMessage!.messages![index];
                                              final bool isMe =
                                                  message.usuarioId ==
                                                      appData.usuario.user!.id;
                                              var me = widget
                                                          .chat.usuario1!.id ==
                                                      appData.usuario.user!.id
                                                  ? widget.chat.usuario1
                                                  : widget.chat.usuario2;
                                              var otherUser = widget
                                                          .chat.usuario1!.id ==
                                                      appData.usuario.user!.id
                                                  ? widget.chat.usuario2
                                                  : widget.chat.usuario1;

                                              return _chatbubble(message, isMe,
                                                  isMe ? me : otherUser);
                                            },
                                          ))
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(14.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Container(
                                  width: 230.w,
                                  height: 51.h,
                                  decoration: new BoxDecoration(
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(15),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x40939393),
                                          offset: Offset(0, 5),
                                          blurRadius: 8,
                                          spreadRadius: 0)
                                    ],
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8),
                                    child: Center(
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Expanded(
                                            flex: 4,
                                            child: TextField(
                                              controller: controller,
                                              style: const TextStyle(
                                                fontSize: 14,
                                              ),
                                              keyboardType:
                                                  TextInputType.multiline,
                                              maxLines: null,
                                              decoration: const InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText:
                                                      'Escriba algo…', // MESSAGE TEXT BOX variable will be here
                                                  fillColor: Color(0xff000000)),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: toggleEmoji,
                                            child: SvgPicture.asset(
                                                'assets/images/smile.svg',
                                                height: 24.h,
                                                width: 24.w),
                                          ),
                                          SizedBox(width: 10.w),
                                          GestureDetector(
                                            onTap: sendImage,
                                            child: SvgPicture.asset(
                                                'assets/images/camera.svg',
                                                height: 24.h,
                                                width: 24.w),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap:
                                      sendMessage, // MEssage sending operation button
                                  child: Container(
                                    width: 49,
                                    height: 51,
                                    decoration: new BoxDecoration(
                                      color: Color(0xff21409a),
                                      borderRadius: BorderRadius.circular(15),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(0x40939393),
                                            offset: Offset(0, 5),
                                            blurRadius: 8,
                                            spreadRadius: 0)
                                      ],
                                    ),
                                    child: Center(
                                        child: SvgPicture.asset(
                                            'assets/images/sendd.svg')),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),

            bottomNavigationBar: pickEmoji
                ? SizedBox(
                    height: 150.h,
                    child: EmojiPicker(
                      onEmojiSelected: (category, emoji) {
                        controller.text = controller.text + emoji.emoji;
                      },
                      onBackspacePressed: () {
                        controller.text = controller.text
                            .substring(0, controller.text.length - 2);
                      },
                      config: Config(
                          columns: 7,
                          emojiSizeMax: 32 *
                              (Platform.isIOS
                                  ? 1.30
                                  : 1.0), // Issue: https://github.com/flutter/flutter/issues/28894
                          verticalSpacing: 0,
                          horizontalSpacing: 0,
                          initCategory: Category.RECENT,
                          bgColor: Color(0xFFF2F2F2),
                          indicatorColor: Colors.blue,
                          iconColor: Colors.grey,
                          iconColorSelected: Colors.blue,
                          showRecentsTab: true,
                          recentsLimit: 28,
                          noRecents: Text(
                            "No Recents",
                            style: const TextStyle(
                                fontSize: 20, color: Colors.black26),
                          ),
                          tabIndicatorAnimDuration: kTabScrollDuration,
                          categoryIcons: const CategoryIcons(),
                          buttonMode: ButtonMode.MATERIAL),
                    ),
                  )
                : const BottomNav(),
            //Bottom bar ended
          ),
        ),
      ),
    );
  }
}
