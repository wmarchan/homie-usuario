// ignore_for_file: non_constant_identifier_names

import 'package:flutter_rating_stars/flutter_rating_stars.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class BuqsuedaThree extends StatefulWidget {
  const BuqsuedaThree({Key? key}) : super(key: key);

  @override
  _BuqsuedaThreeState createState() => _BuqsuedaThreeState();
}

class _BuqsuedaThreeState extends State<BuqsuedaThree> {
  var profileName = 'Robbin Hooda';
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  //Rating Stars Value
  var value = 3.5;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [Color(0xff83dcff), Color(0xffeffdff)],
          stops: [0, 1],
          begin: Alignment(-1.00, -0.02),
          end: Alignment(1.00, 0.02),
          // angle: 91,
          // scale: undefined,
        ),
      ),
      child: Scaffold(
        key: _globalKey,
        endDrawer: Navbar(),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black),
            //NAVIGATING BACK TO THE HOMEPAGE
            onPressed: () => Navigator.of(context).pop(),
          ),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20.0.w),
              child: GestureDetector(
                //THIS FUNCTION WILL OPEN MENU DRAWER
                onTap: () {
                  _globalKey.currentState?.openEndDrawer();
                },
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: 58.41377258300781.w,
                      height: 57.504638671875.w,
                      decoration: const BoxDecoration(
                        color: Color(0xff009ade),
                        shape: BoxShape.circle,
                      ),
                    ),
                    SvgPicture.asset('assets/images/menu-icon.svg'),
                  ],
                ),
              ),
            ),
          ],
          toolbarHeight: 80.h,
        ),

        backgroundColor: Colors.transparent,
        body: SafeArea(
          bottom: false,
          child: Column(
            children: [
              Column(
                children: [
                  Container(
                    height: 100.h,
                    width: 100.w,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/profile2.jpeg'),
                        fit: BoxFit.fitHeight,
                      ),
                      color: Colors.white,
                      shape: BoxShape.circle,
                    ),
                  ),
                  SizedBox(height: 1.0.h),
                  Text(
                    profileName,
                    style: TextStyle(
                      color: const Color(0xff252525),
                      fontSize: 25.sp,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                      letterSpacing: -0.75,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 90.w),
                    child: Row(
                      children: [
                        //Rating Stars
                        RatingStars(
                          value: value,
                          onValueChanged: (v) {
                            //
                            setState(() {
                              value = v;
                            });
                          },
                          starBuilder: (index, color) => Icon(
                            Icons.star,
                            color: color,
                          ),
                          starCount: 5,
                          starSize: 15.sp,
                          maxValue: 5,
                          starSpacing: 5,
                          valueLabelVisibility: false,
                          animationDuration: const Duration(milliseconds: 1000),
                          starOffColor: const Color(0xffe7e8ea),
                          starColor: Colors.yellow.shade600,
                        ),
                        SizedBox(width: 10.5.w),
                        Padding(
                          padding: EdgeInsets.only(top: 10.5.h),
                          child: Text(
                            "93 comentarios",
                            style: TextStyle(
                              color: const Color(0xff8f9bb3),
                              fontSize: 15.sp,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 15.0.h),
                  Center(
                    child: Text(
                      "                 Horario\nLu - Vi / 00:09 a 18:00 hrs",
                      style: TextStyle(
                        color: const Color(0xff222b45),
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20.h,
              ),
              Expanded(
                child: Container(
                  width: 375.w,
                  decoration: const BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0x7c000000),
                          offset: Offset(5, -3),
                          blurRadius: 6,
                          spreadRadius: 0)
                    ],
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 12.0.w,
                      vertical: 22.6.h,
                    ),
                    child: ListView(
                      children: [
                        ProfileTile(
                          title: 'Acerca de Robbin',
                          description:
                              "I love what I do ! Love the outdoors I'm always willing to go the extra mile to satisfy our customer. Call or message me same day services.",
                        ),
                        SizedBox(height: 11.3.h),
                        ProfileTile(
                          title: 'Especialidades',
                          description:
                              "Reparación de lavadoras, neveras, manejo de  frio, enfriadores domésticos e industriales, refrigeradores domésticos. ",
                        ),
                        SizedBox(height: 11.3.h),
                        Container(
                          width: 340.w,
                          height: 200.h,
                          decoration: BoxDecoration(
                            color: const Color(0xff009ade).withOpacity(
                              0.10000000149011612,
                            ),
                            borderRadius: BorderRadius.circular(30),
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 18.0.h, horizontal: 23.1.w),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Ubicación',
                                  style: TextStyle(
                                    color: const Color(0xff222b45),
                                    fontSize: 18.sp,
                                    fontWeight: FontWeight.w800,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                                SizedBox(height: 7.0.h),
                                /*Add the image of the location here
                                  I am putting a dummy image, you can call a the image from database
                                  */
                                Center(
                                  child:
                                      Image.asset('assets/images/location.png'),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 11.3.h),
                        Container(
                          width: 420.w,
                          height: 450.h,
                          decoration: BoxDecoration(
                            color: const Color(0xff009ade).withOpacity(
                              0.10000000149011612,
                            ),
                            borderRadius: BorderRadius.circular(30),
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 18.0.h, horizontal: 10.0.w),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                //Rating Stars
                                Center(
                                  child: RatingStars(
                                    value: value,
                                    onValueChanged: (v) {
                                      //
                                      setState(() {
                                        value = v;
                                      });
                                    },
                                    starBuilder: (index, color) => Icon(
                                      Icons.star,
                                      color: color,
                                    ),
                                    starCount: 5,
                                    starSize: 15.sp,
                                    maxValue: 5,
                                    starSpacing: 5,
                                    valueLabelVisibility: false,
                                    animationDuration:
                                        const Duration(milliseconds: 1000),
                                    starOffColor: const Color(0xffe7e8ea),
                                    starColor: Colors.yellow.shade600,
                                  ),
                                ),
                                SizedBox(height: 10.0.h),

                                //Rating and Comments
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "4.9",
                                      style: TextStyle(
                                        color: const Color(0xff222b45),
                                        fontSize: 15.sp,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                    SizedBox(width: 13.0.w),
                                    Text(
                                      "93 Revisiones",
                                      style: TextStyle(
                                        color: const Color(0xff8f9bb3),
                                        fontSize: 15.sp,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15.7.h),

                                //Reviews with their reviewer profile
                                ReviewTile(
                                  reviewerName: 'Georgie Johnston',
                                  reviewAbout: 'Lawn Mowing and Trimming',
                                  rating: 4.9,
                                  review:
                                      'Super easy to work with. My lawn looks \n awesome now!',
                                  date: 'Ene 24, 2021',
                                ),

                                SizedBox(height: 13.0.h),
                                //Reviews with their reviewer profile
                                ReviewTile(
                                  reviewerName: 'Louise Davis',
                                  reviewAbout: 'Lawn Mowing and Trimming',
                                  rating: 2.5,
                                  review:
                                      'What a great concept and value. \nI have wasted weeks trying interviewing.',
                                  date: 'Ene 24, 2020',
                                ),
                                Center(
                                  child: InkWell(
                                    onTap: () {
                                      //It will load more comments, in this case print more of the ReviewTile methods, a listview builder will be probbaly used.
                                    },
                                    child: Text(
                                      "Mas Comentarios",
                                      style: TextStyle(
                                        color: const Color(0xff252525),
                                        fontSize: 15.sp,
                                        fontWeight: FontWeight.w800,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: -0.3,
                                        decoration: TextDecoration.underline,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),

        //BOTTOM BAR WITH FLOATING BTN
        floatingActionButton: WhatsappButton(),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        bottomNavigationBar: const BottomNav(),
      ),
    );
  }

  SizedBox ReviewTile({
    String reviewerName = '',
    String reviewAbout = '',
    String review = '',
    String date = '',
    required double rating,

    //Image reviewerProfilePic = 'assets/images/'
  }) {
    return SizedBox(
      height: 150.h,
      width: 320.w,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 14.3.w,
          vertical: 5.0.h,
        ),
        child: Column(
          children: [
            //Row 1 Contains Profile Pic, Name, Description and Rating
            Row(
              children: [
                //This Column Contains the Image Container
                Column(
                  children: [
                    Container(
                      width: 61.w,
                      height: 70.h,
                      decoration: BoxDecoration(
                        color: const Color(0xffc5cee0),
                        borderRadius: BorderRadius.circular(
                          8,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(width: 12.0.w),
                //This column contains all text
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      reviewerName,
                      style: TextStyle(
                        color: const Color(0xff222b45),
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(height: 3.3.h),
                    Text(
                      reviewAbout,
                      style: TextStyle(
                        color: const Color(0xff8f9bb3),
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(height: 4.0.h),

                    //Rating Stars
                    RatingStars(
                      value: rating,
                      onValueChanged: (rating) {
                        //
                        setState(() {
                          value = rating;
                        });
                      },
                      starBuilder: (index, color) => Icon(
                        Icons.star,
                        color: color,
                      ),
                      starCount: 5,
                      starSize: 15.sp,
                      maxValue: 5,
                      starSpacing: 5,
                      valueLabelVisibility: false,
                      animationDuration: const Duration(milliseconds: 1000),
                      starOffColor: const Color(0xffe7e8ea),
                      starColor: Colors.yellow.shade600,
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: 6.7.h),
            //Row 2 Contains the review
            Row(
              children: [
                Text(
                  review,
                  style: TextStyle(
                    color: const Color(0xff222b45),
                    fontSize: 15.sp,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                  softWrap: true,
                ),
              ],
            ),
            SizedBox(height: 6.3.h),

            //Row 3 Contains the date
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  date,
                  style: TextStyle(
                    color: const Color(0xff8f9bb3),
                    fontSize: 13.sp,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Container ProfileTile({
    String title = '',
    String description = '',
  }) {
    return Container(
      width: 340.w,
      height: 122.h,
      decoration: BoxDecoration(
        color: const Color(0xff009ade).withOpacity(
          0.10000000149011612,
        ),
        borderRadius: BorderRadius.circular(30),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 18.0.h, horizontal: 23.1.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(
                color: const Color(0xff222b45),
                fontSize: 18.sp,
                fontWeight: FontWeight.w800,
                fontStyle: FontStyle.normal,
              ),
            ),
            SizedBox(height: 7.0.h),
            Text(
              description,
              style: TextStyle(
                color: const Color(0xff8f9bb3),
                fontSize: 12.sp,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
