// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:homie/screens/onboarding/onboarding.dart';

// ignore: must_be_immutable
class onboardslider extends StatelessWidget {
  String? image;
  String? title;
  String? description;
  int? slides;
  //Constructor created
  // ignore: use_key_in_widget_constructors
  onboardslider({this.image, this.title, this.description, this.slides});

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Padding(
      padding: EdgeInsets.only(top: 162.0.h),
      child: Container(
        // column containing image
        // title and description
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(image: AssetImage(image!)),
            SizedBox(height: 40.h),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(
                  slides!,
                  (index) {
                    return buildDot(index, context);
                  },
                ),
              ),
            ),
            SizedBox(
              height: 36.h,
            ),
            Text(
              title!,
              style: TextStyle(
                fontSize: 23.sp,
                color: const Color(0xff00013c),
                height: 1.1,
              ),
            ),
            SizedBox(height: 18.h),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 23.w),
              child: Text(
                description!,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 14.sp,
                  color: const Color(0xff828282),
                  height: 1.5,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Container buildDot(int index, BuildContext context) {
  // Another Container returned
  return Container(
    height: 10,
    width: currentIndex == index ? 25 : 10,
    margin: EdgeInsets.only(right: 5),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(4),
      color: currentIndex == index ? Color(0xff21409a) : Color(0xffd0d1fb),
    ),
  );
}
