import 'package:flutter/material.dart';
import 'package:homie/screens/login_screens/login.dart';
import 'package:homie/screens/onboarding/slider.dart';
import 'package:homie/models/slidermodel.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class OnboardingScreen extends StatefulWidget {
  const OnboardingScreen({Key? key}) : super(key: key);

  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

int currentIndex = 0;

class _OnboardingScreenState extends State<OnboardingScreen> {
  late PageController _controller;

  List<SliderModel> slides = List<SliderModel>.empty(growable: true);

  void initState() {
    super.initState();
    _controller = PageController(initialPage: 0);
    slides = getSlides();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
        colors: [Color(0xff83dcff), Color(0xffeffdff)],
        stops: [0, 1],
        begin: Alignment(-1.00, -0.02),
        end: Alignment(1.00, 0.02),
        // angle: 91,
        // scale: undefined,
      )),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Column(
          children: [
            Expanded(
                child: PageView.builder(
                    scrollDirection: Axis.horizontal,
                    onPageChanged: (value) {
                      setState(() {
                        currentIndex = value;
                      });
                    },
                    itemCount: slides.length,
                    itemBuilder: (context, index) {
                      // ignore: dead_code
                      return onboardslider(
                        image: slides[index].getImage(),
                        title: slides[index].getTitle(),
                        description: slides[index].getDescription(),
                        slides: slides.length,
                      );
                    })),
            currentIndex == slides.length - 1
                ? Container(
                    height: 55.h,
                    margin: EdgeInsets.all(40.w),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: const Color(0xff21409a),
                      boxShadow: const [
                        BoxShadow(
                            color: Color(0x29000000),
                            offset: Offset(5, 3),
                            blurRadius: 6,
                            spreadRadius: 0)
                      ],
                      borderRadius: BorderRadius.all(
                        Radius.circular(30.r),
                      ),
                    ),
                    child: TextButton(
                      child: Text(
                        "Iniciar",
                        style: TextStyle(
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w800,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                      onPressed: () {
                        if (currentIndex == slides.length - 1) {
                          // Navigate to next screen
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginScreen()),
                          );
                        }
                      },
                      style: TextButton.styleFrom(
                        textStyle: TextStyle(color: Colors.white),
                        shape: RoundedRectangleBorder(
                          side: const BorderSide(color: Colors.white, width: 1),
                          borderRadius: BorderRadius.circular(30.r),
                        ),
                      ),
                    ),
                  )
                : const SizedBox(),
          ],
        ),
      ),
    );
  }
}
