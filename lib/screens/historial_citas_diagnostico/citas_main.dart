import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:homie/client.dart';
import 'package:homie/models/visita_tecnica/visita_tecnica.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/citas/tarjeta_cita.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:homie/navigation/whatsapp_button.dart';

import 'cita_finalizada.dart';

class CitasHistorialScreen extends StatefulWidget {
  const CitasHistorialScreen({Key? key}) : super(key: key);

  @override
  _CitasHistorialScreenState createState() => _CitasHistorialScreenState();
}

class _CitasHistorialScreenState extends State<CitasHistorialScreen> {
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  String PageTitle = 'Historial Citas';
  bool IsagendadasSelected = true;
  bool IsconfirmadasSelected = false;
  bool IsEnCursoSelected = false;
  bool IsFinalizadosSelected = false;

  List<String> getStatus() {
    return ["Finalizado", "Completado"];
  }

  Widget getRoute(VisitaTecnica visita) {
    return CitasFinalizadaScreen();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
        colors: [Color(0xff83dcff), Color(0xffeffdff)],
        stops: [0, 1],
        begin: Alignment(-1.00, -0.02),
        end: Alignment(1.00, 0.02),
      )),
      child: Scaffold(
        key: _globalKey,
        endDrawer: Navbar(),
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black),
            //NAVIGATING BACK TO THE HOMEPAGE
            onPressed: () => Navigator.of(context).pop(),
          ),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20.0.w),
              child: GestureDetector(
                //THIS FUNCTION WILL OPEN MENU DRAWER
                onTap: () {
                  _globalKey.currentState?.openEndDrawer();
                },
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: 58.41377258300781.w,
                      height: 57.504638671875.w,
                      decoration: const BoxDecoration(
                        color: Color(0xff009ade),
                        shape: BoxShape.circle,
                      ),
                    ),
                    SvgPicture.asset('assets/images/menu-icon.svg'),
                  ],
                ),
              ),
            ),
          ],
          toolbarHeight: 120.h,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 16.0.w),
              child: Text(PageTitle,
                  style: TextStyle(
                    color: const Color(0xff00013c),
                    fontSize: 23.sp,
                    fontWeight: FontWeight.w900,
                    fontStyle: FontStyle.normal,
                  )),
            ),
            SizedBox(
              height: 24.h,
            ),
            Expanded(
              child: Container(
                width: 375.w,
                decoration: const BoxDecoration(
                  color: Color(0xffffffff),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25)),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0x7c000000),
                        offset: Offset(5, -3),
                        blurRadius: 6,
                        spreadRadius: 0)
                  ],
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 30.h,
                    ),
                    Expanded(
                      child: FutureBuilder(
                          future: Client().getVisitas(getStatus()),
                          builder: (context,
                              AsyncSnapshot<List<VisitaTecnica>> snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return const Center(
                                  child: CircularProgressIndicator());
                            }

                            return ListView.builder(
                              itemCount: snapshot.data!.length,
                              itemBuilder: (context, i) {
                                VisitaTecnica visita = snapshot.data![i];
                                return Container(
                                    margin: EdgeInsets.only(bottom: 18.h),
                                    child: TarjetaCita(
                                        fecha: visita.dia.toString(),
                                        titulo:
                                            "Evaluacion ${visita.subcategorium!.nombre}",
                                        bloque: visita.hora.toString(),
                                        status: visita.status.toString(),
                                        onTap: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    getRoute(visita),
                                                settings: RouteSettings(
                                                    arguments: visita)),
                                          );
                                        }));
                              },
                              padding:
                                  EdgeInsets.only(left: 23.0.w, right: 23.0.w),
                              shrinkWrap: true,
                              scrollDirection: Axis.vertical,
                            );
                          }),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
        //BOTTOM BAR WITH FLOATING BTN
        floatingActionButton: WhatsappButton(),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        bottomNavigationBar: BottomNav(),
      ),
    );
  }
}

Future<dynamic> ShowDialogBox(BuildContext context, String TitleOfDialogBox,
    String Description, bool haveTwoButtosns, bool haveOneButtons) {
  return showDialog(
      context: context,
      builder: (context) {
        /// DIALOG BOX
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.r),
          ),
          child: Container(
            width: 375.w,
            height: 450.w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: Color(0xffffffff),
              boxShadow: const [
                BoxShadow(
                    color: Color(0x7c000000),
                    offset: Offset(5, -3),
                    blurRadius: 6,
                    spreadRadius: 0)
              ],
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 10.h),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.close_outlined,
                            size: 30.w,
                          )),
                    ],
                  ),
                  SvgPicture.asset('assets/images/agendaDialoge.svg'),
                  SizedBox(
                    height: 30.h,
                  ),
                  Text(
                    TitleOfDialogBox,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xff222b45),
                      fontSize: 17.sp,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 48.0.w),
                    child: Text(Description,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xff414040),
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w300,
                          fontStyle: FontStyle.normal,
                        )),
                  ),
                  SizedBox(
                    height: 27.h,
                  ),
                  haveTwoButtosns
                      ? Twobuttons()
                      : SizedBox(
                          height: 15.h,
                        ),
                  haveOneButtons
                      ? Onebuttons()
                      : SizedBox(
                          height: 15.h,
                        ),
                ],
              ),
            ),
          ),
        );
      });
}

Row Twobuttons() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        width: 117.w,
        height: 35.w,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Color(0xffF5FAFC),
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(38.0.r),
          color: const Color(0xff009ade),
          boxShadow: const [
            BoxShadow(
              color: Color(0x29000000),
              offset: Offset(5, 3),
              blurRadius: 6,
              spreadRadius: 0,
            ),
          ],
        ),
        child: TextButton(
          //THIS WILL REDIRECT TO NEXT PAGE
          onPressed: () {},
          child: Text(
            "CALIFICAR",
            style: TextStyle(
              color: Color(0xffffffff),
              fontSize: 12.sp,
              fontWeight: FontWeight.w800,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
      SizedBox(
        width: 20.w,
      ),
      Container(
        width: 117.w,
        height: 35.w,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Color(0xffF5FAFC),
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(38.0.r),
          color: Color(0xffffca51),
          boxShadow: const [
            BoxShadow(
              color: Color(0x29000000),
              offset: Offset(5, 3),
              blurRadius: 6,
              spreadRadius: 0,
            ),
          ],
        ),
        child: TextButton(
          //THIS WILL REDIRECT TO NEXT PAGE
          onPressed: () {},
          child: Text(
            "INICIO",
            style: TextStyle(
              color: Color(0xffffffff),
              fontSize: 12.sp,
              fontWeight: FontWeight.w800,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
    ],
  );
}

Row Onebuttons() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        width: 212.w,
        height: 49.w,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Color(0xffF5FAFC),
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(38.0.r),
          color: const Color(0xff21409a),
          boxShadow: const [
            BoxShadow(
              color: Color(0x29000000),
              offset: Offset(5, 3),
              blurRadius: 6,
              spreadRadius: 0,
            ),
          ],
        ),
        child: TextButton(
          //THIS WILL REDIRECT TO NEXT PAGE
          onPressed: () {},
          child: Text(
            "Buscar",
            style: TextStyle(
              color: Color(0xffffffff),
              fontSize: 16.sp,
              fontWeight: FontWeight.w800,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
    ],
  );
}
