import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:homie/client.dart';
import 'package:homie/models/visita_tecnica/visita_tecnica.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:intl/intl.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class CitasFinalizadaScreen extends StatefulWidget {
  const CitasFinalizadaScreen({Key? key}) : super(key: key);

  @override
  _CitasFinalizadaScreenState createState() => _CitasFinalizadaScreenState();
}

class _CitasFinalizadaScreenState extends State<CitasFinalizadaScreen> {
  late VisitaTecnica visita;

  String getFormattedDate() {
    return DateFormat("MMMM dd, yyyy")
        .format(DateTime.parse(visita.dia.toString()));
  }

  @override
  Widget build(BuildContext context) {
    visita = ModalRoute.of(context)!.settings.arguments as VisitaTecnica;

    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
        colors: [Color(0xff83dcff), Color(0xffeffdff)],
        stops: [0, 1],
        begin: Alignment(-1.00, -0.02),
        end: Alignment(1.00, 0.02),
      )),
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              //NAVIGATING BACK TO THE HOMEPAGE
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Padding(
              padding: EdgeInsets.only(right: 5.0.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    //THIS FUNCTION WILL OPEN MENU DRAWER
                    onTap: () {},
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          width: 58.41377258300781.w,
                          height: 57.504638671875.w,
                          decoration: new BoxDecoration(
                            color: Color(0xff009ade),
                            shape: BoxShape.circle,
                          ),
                        ),
                        SvgPicture.asset('assets/images/menu-icon.svg'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            toolbarHeight: 120.h,
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 16.0.w),
                child: Text("Citas Finalizada",
                    style: TextStyle(
                      color: Color(0xff00013c),
                      fontSize: 23.sp,
                      fontWeight: FontWeight.w900,
                      fontStyle: FontStyle.normal,
                    )),
              ),
              SizedBox(
                height: 24.h,
              ),
              Expanded(
                child: Container(
                  width: 375.w,
                  decoration: BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0x7c000000),
                          offset: Offset(5, -3),
                          blurRadius: 6,
                          spreadRadius: 0)
                    ],
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 15.w,
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 21.0.w),
                                child: Row(
                                  children: [
                                    Stack(
                                      alignment: Alignment.center,
                                      children: [
                                        Container(
                                            width: 92.w,
                                            height: 92.w,
                                            decoration: new BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Color(0xfff7f7f7))),
                                        Container(
                                          width: 76.w,
                                          height: 76.w,
                                          decoration: new BoxDecoration(
                                            boxShadow: const [
                                              BoxShadow(
                                                  color: Color(0x23646464),
                                                  offset: Offset(-7, 3),
                                                  blurRadius: 4,
                                                  spreadRadius: 0)
                                            ],
                                            color: Color(0xff009ade),
                                            shape: BoxShape.circle,
                                          ),
                                          child: SvgPicture.asset(
                                            'assets/images/test-icon.svg',
                                            fit: BoxFit.scaleDown,
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      width: 20.w,
                                    ),
                                    Text("Orden #${visita.id}",
                                        style: TextStyle(
                                          color: Color(0xff222b45),
                                          fontSize: 25.sp,
                                          fontWeight: FontWeight.w600,
                                          fontStyle: FontStyle.normal,
                                        ))
                                  ],
                                ),
                              ),
                              Container(
                                width: 343.w,
                                decoration: new BoxDecoration(
                                  color: Color(0xffffffff),
                                  borderRadius: BorderRadius.circular(12),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Color(0x29000000),
                                        offset: Offset(0, 3),
                                        blurRadius: 6,
                                        spreadRadius: 0)
                                  ],
                                ),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16.w, vertical: 25.w),
                                  child: Column(children: [
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text("Día",
                                              style: TextStyle(
                                                color: Color(0xff8f9bb3),
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                              )),
                                          Text(getFormattedDate(),
                                              style: TextStyle(
                                                color: Color(0xff007aff),
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                              ))
                                        ]),
                                    SizedBox(
                                      height: 20.h,
                                    ),
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text("Hora",
                                              style: TextStyle(
                                                color: Color(0xff8f9bb3),
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                              )),
                                          Text(visita.hora.toString(),
                                              style: TextStyle(
                                                color: Color(0xff007aff),
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                              ))
                                        ]),
                                    SizedBox(
                                      height: 15.h,
                                    ),
                                    Divider(
                                      height: 2.w,
                                      color: Color(0xffD9DEE4),
                                      indent: 16.3.w,
                                    ),
                                    SizedBox(
                                      height: 20.h,
                                    ),
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text("Status",
                                              style: TextStyle(
                                                color: Color(0xff8f9bb3),
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                              )),
                                          Text(visita.status.toString(),
                                              style: TextStyle(
                                                color: Color(0xff222b45),
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                              ))
                                        ]),
                                    SizedBox(
                                      height: 20.h,
                                    ),
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text("Tipo",
                                              style: TextStyle(
                                                color: Color(0xff8f9bb3),
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                              )),
                                          Text(
                                              visita.tipoServicio!.nombre
                                                  .toString(),
                                              style: TextStyle(
                                                color: Color(0xff222b45),
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                              ))
                                        ]),
                                    SizedBox(
                                      height: 20.h,
                                    ),
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text("Problema",
                                              style: TextStyle(
                                                color: Color(0xff8f9bb3),
                                                fontSize: 15.sp,
                                                fontWeight: FontWeight.w400,
                                                fontStyle: FontStyle.normal,
                                              )),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                            child: Text(
                                                visita.descripcion.toString(),
                                                style: TextStyle(
                                                  color: Color(0xff222b45),
                                                  fontSize: 15.sp,
                                                  fontWeight: FontWeight.w400,
                                                  fontStyle: FontStyle.normal,
                                                )),
                                          )
                                        ]),
                                  ]),
                                ),
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(left: 34.0.w),
                                    child: Text("REPORTE",
                                        style: TextStyle(
                                          color: Color(0xff222b45),
                                          fontSize: 15.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                  ),
                                  SizedBox(height: 7.h),
                                  Container(
                                      width: 343.w,
                                      height: 131.h,
                                      decoration: BoxDecoration(
                                        color: Color(0xffffffff),
                                        borderRadius: BorderRadius.circular(12),
                                        boxShadow: const [
                                          BoxShadow(
                                              color: Color(0x29000000),
                                              offset: Offset(0, 3),
                                              blurRadius: 6,
                                              spreadRadius: 0)
                                        ],
                                      ),
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 13.w, vertical: 21.h),
                                        child: Text(visita.reporte.toString(),
                                            style: TextStyle(
                                              color: Color(0xff222b45),
                                              fontSize: 15.sp,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                      )),
                                  SizedBox(height: 30.h),
                                ],
                              ),
                              if (visita.status == "Completado")
                                Column(
                                  children: [
                                    Container(
                                      width: 160.w,
                                      height: 49.w,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          width: 1,
                                          color: Color(0xffF5FAFC),
                                          style: BorderStyle.solid,
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(38.0.r),
                                        color: const Color(0xff009ade),
                                        boxShadow: const [
                                          BoxShadow(
                                            color: Color(0x29000000),
                                            offset: Offset(5, 3),
                                            blurRadius: 6,
                                            spreadRadius: 0,
                                          ),
                                        ],
                                      ),
                                      child: TextButton(
                                        //THIS WILL REDIRECT TO NEXT PAGE
                                        onPressed: () async {
                                          ShowDialogBox(
                                              (context),
                                              'SERVICIO FINALIZADO',
                                              'Gracias por aceptar el fin del trabajo. Puede calificar al técnico o regresar al inicio',
                                              true,
                                              false,
                                              visita: visita);
                                        },
                                        child: Text(
                                          "SIGUIENTE",
                                          style: TextStyle(
                                            color: Color(0xffffffff),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w800,
                                            fontStyle: FontStyle.normal,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 20.h),
                                    InkWell(
                                      onTap: () {},
                                      child: Text("Contacto",
                                          style: TextStyle(
                                            decoration:
                                                TextDecoration.underline,
                                            color: Color(0xff707070),
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w800,
                                            fontStyle: FontStyle.normal,
                                          )),
                                    )
                                  ],
                                ),
                              SizedBox(height: 50.h),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
          //BOTTOM BAR WITH FLOATING BTN
          floatingActionButton: WhatsappButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: const BottomNav(),
        ),
      ),
    );
  }
}

Future<dynamic> ShowDialogBox(BuildContext context, String TitleOfDialogBox,
    String Description, bool haveTwoButtosns, bool haveOneButtons,
    {Function? onClose, VisitaTecnica? visita}) {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        /// DIALOG BOX
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.r),
          ),
          child: Container(
            width: 375.w,
            height: 550.h,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: const Color(0xffffffff),
              boxShadow: const [
                BoxShadow(
                    color: Color(0x7c000000),
                    offset: Offset(5, -3),
                    blurRadius: 6,
                    spreadRadius: 0)
              ],
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 10.h),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(
                          onPressed: () {
                            onClose != null
                                ? onClose()
                                : Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.close_outlined,
                            size: 30.w,
                          )),
                    ],
                  ),
                  SvgPicture.asset('assets/images/agendaDialoge.svg'),
                  SizedBox(
                    height: 30.h,
                  ),
                  Text(
                    TitleOfDialogBox,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xff222b45),
                      fontSize: 17.sp,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 48.0.w),
                    child: Text(Description,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xff414040),
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w300,
                          fontStyle: FontStyle.normal,
                        )),
                  ),
                  SizedBox(
                    height: 27.h,
                  ),
                  haveTwoButtosns
                      ? Twobuttons(context, visita)
                      : SizedBox(
                          height: 15.h,
                        ),
                  haveOneButtons
                      ? Onebuttons()
                      : SizedBox(
                          height: 15.h,
                        ),
                ],
              ),
            ),
          ),
        );
      });
}

Row Twobuttons(context, VisitaTecnica? visita) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        width: 117.w,
        height: 35.w,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Color(0xffF5FAFC),
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(38.0.r),
          color: const Color(0xff009ade),
          boxShadow: const [
            BoxShadow(
              color: Color(0x29000000),
              offset: Offset(5, 3),
              blurRadius: 6,
              spreadRadius: 0,
            ),
          ],
        ),
        child: TextButton(
          //THIS WILL REDIRECT TO NEXT PAGE
          onPressed: () {
            Navigator.of(context).pushReplacementNamed("/citas/calificar",
                arguments: {"visita": visita});
          },
          child: Text(
            "CALIFICAR",
            style: TextStyle(
              color: Color(0xffffffff),
              fontSize: 12.sp,
              fontWeight: FontWeight.w800,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
      SizedBox(
        width: 20.w,
      ),
      Container(
        width: 117.w,
        height: 35.w,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Color(0xffF5FAFC),
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(38.0.r),
          color: Color(0xffffca51),
          boxShadow: const [
            BoxShadow(
              color: Color(0x29000000),
              offset: Offset(5, 3),
              blurRadius: 6,
              spreadRadius: 0,
            ),
          ],
        ),
        child: TextButton(
          //THIS WILL REDIRECT TO NEXT PAGE
          onPressed: () {
            Navigator.of(context)
                .pushNamedAndRemoveUntil("/home", (route) => false);
          },
          child: Text(
            "INICIO",
            style: TextStyle(
              color: Color(0xffffffff),
              fontSize: 12.sp,
              fontWeight: FontWeight.w800,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
    ],
  );
}

Row Onebuttons() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        width: 212.w,
        height: 49.w,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Color(0xffF5FAFC),
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(38.0.r),
          color: const Color(0xff21409a),
          boxShadow: const [
            BoxShadow(
              color: Color(0x29000000),
              offset: Offset(5, 3),
              blurRadius: 6,
              spreadRadius: 0,
            ),
          ],
        ),
        child: TextButton(
          //THIS WILL REDIRECT TO NEXT PAGE
          onPressed: () {},
          child: Text(
            "Calificar",
            style: TextStyle(
              color: Color(0xffffffff),
              fontSize: 16.sp,
              fontWeight: FontWeight.w800,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
    ],
  );
}
