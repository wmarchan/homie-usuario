// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:homie/client.dart';
import 'package:homie/models/pago/pago.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:intl/intl.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class Pagos extends StatelessWidget {
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  Pagos({Key? key}) : super(key: key);

  final String PageTitle = 'Pagos Realizados';
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
        colors: const [Color(0xff83dcff), Color(0xffeffdff)],
        stops: const [0, 1],
        begin: Alignment(-1.00, -0.02),
        end: Alignment(1.00, 0.02),
      )),
      child: SafeArea(
        child: Scaffold(
          key: _globalKey,
          endDrawer: Navbar(),
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              //NAVIGATING BACK TO THE HOMEPAGE
              onPressed: () => Navigator.of(context).pop(),
            ),
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 20.0.w),
                child: GestureDetector(
                  //THIS FUNCTION WILL OPEN MENU DRAWER
                  onTap: () {
                    _globalKey.currentState?.openEndDrawer();
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 58.41377258300781.w,
                        height: 57.504638671875.w,
                        decoration: new BoxDecoration(
                          color: Color(0xff009ade),
                          shape: BoxShape.circle,
                        ),
                      ),
                      SvgPicture.asset('assets/images/menu-icon.svg'),
                    ],
                  ),
                ),
              ),
            ],
            toolbarHeight: 120.h,
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 16.0.w),
                child: Text(
                  PageTitle,
                  style: TextStyle(
                    color: const Color(0xff00013c),
                    fontSize: 23.sp,
                    fontWeight: FontWeight.w900,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              SizedBox(
                height: 24.h,
              ),
              Expanded(
                child: Container(
                    width: 375.w,
                    decoration: const BoxDecoration(
                      color: Color(0xffffffff),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25)),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0x7c000000),
                            offset: Offset(5, -3),
                            blurRadius: 6,
                            spreadRadius: 0)
                      ],
                    ),
                    child: FutureBuilder(
                        future: Client().getPagos(),
                        builder: (context, AsyncSnapshot<List<Pago>> snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          if (snapshot.data!.isEmpty) {
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(bottom: 50.2.h),
                                  child: Center(
                                    child: Image.asset(
                                        'assets/images/pagos_center_img.png'),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 260.h),
                                  child: Center(
                                    child: Text(
                                      "Aún no hay Pagos",
                                      style: TextStyle(
                                        color: const Color(0xff1a1824),
                                        fontSize: 36.sp,
                                        fontWeight: FontWeight.w900,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: -0.32,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            );
                          } else {
                            return Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 50),
                              child: ListView.builder(
                                  itemCount: snapshot.data!.length,
                                  itemBuilder: (context, i) {
                                    return TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pushNamed('/pago',
                                            arguments: snapshot.data![i]);
                                      },
                                      child: orderDetails(
                                        pago: snapshot.data![i],
                                      ),
                                    );
                                  }),
                            );
                          }
                        })),
              ),
            ],
          ),

          //BOTTOM BAR WITH FLOATING BTN
          floatingActionButton: WhatsappButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: const BottomNav(),
        ),
      ),
    );
  }

  Column orderDetails({
    required final Pago pago,
  }) {
    Color getColor(String status) {
      switch (status) {
        case 'Cancelado':
          return const Color(0xffff2650);
        case 'Confirmada':
          return const Color(0xffeba41d);
        case 'Completado':
          return const Color(0xff34c47c);
        default:
          return const Color(0xffeba41d);
      }
    }

    String getFormattedDate() {
      return DateFormat("MMMM dd, yyyy")
          .format(DateTime.parse(pago.visitaTecnica!.dia.toString()));
    }

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Order #N ${pago.id.toString()}',
                  style: TextStyle(
                    color: const Color(0xff242134),
                    fontSize: 15.sp,
                    fontWeight: FontWeight.w800,
                    fontStyle: FontStyle.normal,
                    letterSpacing: -0.13,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 25.w),
                  child: Text(
                    pago.visitaTecnica!.status.toString(),
                    style: TextStyle(
                      color: getColor(
                        pago.visitaTecnica!.status.toString(),
                      ),
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w800,
                      fontStyle: FontStyle.normal,
                      letterSpacing: -0.11,
                    ),
                  ),
                ),
              ],
            ),
            Expanded(child: Container()),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 10.0.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "\$${pago.total}",
                        style: TextStyle(
                          color: const Color(0xff34c47c),
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w800,
                          fontStyle: FontStyle.normal,
                          letterSpacing: -0.13,
                        ),
                      ),
                      Text(
                        getFormattedDate(),
                        style: TextStyle(
                          color: const Color(0xff1a1824),
                          fontSize: 13.sp,
                          fontWeight: FontWeight.w300,
                          fontStyle: FontStyle.normal,
                          letterSpacing: -0.11,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 5.w),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 6.0.h),
                      child: const Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.black,
                        size: 18,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
        SizedBox(height: 20.0.h),
      ],
    );
  }
}
