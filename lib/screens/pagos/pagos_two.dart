// ignore_for_file: recursive_getters, camel_case_types

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:homie/models/pago/pago.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:intl/intl.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class PagosTwo extends StatefulWidget {
  const PagosTwo({Key? key}) : super(key: key);

  @override
  _PagosTwoState createState() => _PagosTwoState();
}

class _PagosTwoState extends State<PagosTwo> {
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  // ignore: non_constant_identifier_names
  final String PageTitle = 'Pagos Realizados';

  String getFormattedDate() {
    return DateFormat("MMMM dd, yyyy")
        .format(DateTime.parse(pago!.visitaTecnica!.dia.toString()));
  }

  Pago? pago;

  @override
  Widget build(BuildContext context) {
    pago = ModalRoute.of(context)!.settings.arguments as Pago;
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
        // ignore: prefer_const_constructors
        gradient: LinearGradient(
          colors: [Color(0xff83dcff), Color(0xffeffdff)],
          stops: [0, 1],
          begin: Alignment(-1.00, -0.02),
          end: Alignment(1.00, 0.02),
        ),
      ),
      child: SafeArea(
        child: Scaffold(
          key: _globalKey,
          endDrawer: Navbar(),
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
              //NAVIGATING BACK TO THE HOMEPAGE
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Padding(
              padding: EdgeInsets.only(right: 5.0.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    //THIS FUNCTION WILL OPEN MENU DRAWER
                    onTap: () {
                      _globalKey.currentState?.openEndDrawer();
                    },
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          width: 58.41377258300781.w,
                          height: 57.504638671875.w,
                          decoration: const BoxDecoration(
                            color: Color(0xff009ade),
                            shape: BoxShape.circle,
                          ),
                        ),
                        SvgPicture.asset('assets/images/menu-icon.svg'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            toolbarHeight: 120.h,
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 16.0.w),
                child: Text(
                  PageTitle,
                  style: TextStyle(
                    color: const Color(0xff00013c),
                    fontSize: 23.sp,
                    fontWeight: FontWeight.w900,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              SizedBox(
                height: 24.h,
              ),

              //Outer Container
              Expanded(
                child: Container(
                  width: 375.w,
                  decoration: const BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Color(0x7c000000),
                        offset: Offset(5, -3),
                        blurRadius: 6,
                        spreadRadius: 0,
                      )
                    ],
                  ),

                  //Inner Container/Card
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(12.5.w, 12.5.h, 12.5.w, 14.h),
                    child: Container(
                      width: 343.w,
                      height: 488.h,
                      decoration: BoxDecoration(
                        color: const Color(0xffffffff),
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: const [
                          BoxShadow(
                            color: Color(0x29000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                            spreadRadius: 0,
                          ),
                        ],
                      ),
                      child: ListView(
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 10.w, vertical: 10.h),
                            child: Column(
                              children: [
                                if (pago?.visitaTecnica?.tecnico != null)
                                  Row(
                                    children: [
                                      Container(
                                        height: 100.h,
                                        width: 100.w,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                            image: NetworkImage(pago!
                                                .visitaTecnica!.tecnico!.foto
                                                .toString()),
                                            fit: BoxFit.fitHeight,
                                          ),
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                      SizedBox(width: 16.0.w),
                                      Text(
                                        '${pago!.visitaTecnica!.tecnico!.nombre} ${pago!.visitaTecnica!.tecnico!.apellido}',
                                        style: TextStyle(
                                          color: const Color(0xff222b45),
                                          fontSize: 25.sp,
                                          fontWeight: FontWeight.w600,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                    ],
                                  ),
                                SizedBox(height: 2.1.h),
                                Row(
                                  children: [
                                    Container(
                                      width: 324.w,
                                      height: 1.h,
                                      decoration: const BoxDecoration(
                                        color: Color(0xffedf1f7),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 20.h),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Horario',
                                      style: TextStyle(
                                        color: const Color(0xff8f9bb3),
                                        fontSize: 15.sp,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                    SizedBox(width: 100.0.w),
                                    Text(
                                      getFormattedDate(),
                                      style: TextStyle(
                                        color: const Color(0xff007aff),
                                        fontSize: 15.sp,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: 2.1.h),
                                Row(
                                  children: [
                                    Container(
                                      width: 325.w,
                                      height: 1.h,
                                      decoration: const BoxDecoration(
                                        color: Color(0xffedf1f7),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 20.h),
                                if (pago?.visitaTecnica?.tecnico != null)
                                  pagosTwoListTile(
                                    detail1: 'Técnico',
                                    detail2:
                                        '${pago?.visitaTecnica?.tecnico?.nombre} ${pago?.visitaTecnica?.tecnico?.apellido}',
                                  ),
                                SizedBox(height: 2.1.h),
                                Row(
                                  children: [
                                    Container(
                                      width: 325.w,
                                      height: 1.h,
                                      decoration: const BoxDecoration(
                                        color: Color(0xffedf1f7),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 20.h),
                                pagosTwoListTile(
                                  detail1: 'Tipo',
                                  detail2: pago!
                                      .visitaTecnica!.tipoServicio!.nombre
                                      .toString(),
                                ),
                                SizedBox(height: 2.1.h),
                                Row(
                                  children: [
                                    Container(
                                      width: 325.w,
                                      height: 1.h,
                                      decoration: const BoxDecoration(
                                        color: Color(0xffedf1f7),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 20.h),
                                pagosTwoListTile(
                                    detail1: 'Problema',
                                    detail2: pago!.visitaTecnica!.descripcion
                                        .toString()),
                                SizedBox(height: 2.1.h),
                                Row(
                                  children: [
                                    Container(
                                      width: 325.w,
                                      height: 1.h,
                                      decoration: const BoxDecoration(
                                        color: Color(0xffedf1f7),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 20.h),
                                pagosTwoListTile(
                                  detail1: 'ID',
                                  detail2: 'N ${pago!.visitaTecnica!.id}',
                                ),
                                SizedBox(height: 40.h),
                                Row(
                                  children: [
                                    Container(
                                      width: 325.w,
                                      height: 1.h,
                                      decoration: const BoxDecoration(
                                        color: Color(0xffedf1f7),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 20.h),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Precio',
                                      style: TextStyle(
                                        color: const Color(0xff222b45),
                                        fontSize: 15.sp,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                    SizedBox(width: 100.0.w),
                                    Text(
                                      '\$${pago!.total}',
                                      style: TextStyle(
                                        color: const Color(0xff007aff),
                                        fontSize: 15.sp,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: 40.h),
                                Row(
                                  children: [
                                    Container(
                                      width: 325.w,
                                      height: 1.h,
                                      decoration: const BoxDecoration(
                                        color: Color(0xffedf1f7),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 40.h),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Status',
                                      style: TextStyle(
                                        color: const Color(0xff222b45),
                                        fontSize: 15.sp,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                    SizedBox(width: 100.0.w),
                                    Text(
                                      pago!.visitaTecnica!.status.toString(),
                                      style: TextStyle(
                                        color: const Color(0xff0d9842),
                                        fontSize: 15.sp,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),

          //BOTTOM BAR WITH FLOATING BTN
          floatingActionButton: WhatsappButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: const BottomNav(),
        ),
      ),
    );
  }
}

Row pagosTwoListTile({
  required String detail1,
  required String detail2,
}) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        detail1,
        style: TextStyle(
          color: const Color(0xff8f9bb3),
          fontSize: 15.sp,
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
        ),
      ),
      SizedBox(width: 100.0.w),
      Text(
        detail2,
        style: TextStyle(
          color: const Color(0xff222b45),
          fontSize: 15.sp,
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
        ),
      )
    ],
  );
}
