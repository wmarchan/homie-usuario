import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:homie/client.dart';
import 'package:homie/singletons/AppData.dart';

class Navbar extends StatelessWidget {
  void confirmarEliminar(context) {
    // mostrar dialogo de confirmacion
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("¿Estás seguro de que quieres eliminar tu cuenta?"),
          actions: <Widget>[
            TextButton(
              child: Text("Cancelar"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text("Eliminar mi cuenta"),
              onPressed: () async {
                await Client().eliminarCuenta();
                await Client().logout(context);
                Navigator.of(context)
                    .pushNamedAndRemoveUntil("login", (route) => false);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );

    return Drawer(
      child: ListView(children: [
        SizedBox(
          height: 30.h,
        ),
        Padding(
          padding: EdgeInsets.all(10.0.w),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              // Drawer Header with picture
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10.w),
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Color(0xff00C9FF),
                    width: 6,
                  ),
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image:
                          NetworkImage(appData.usuario.user!.foto.toString()),
                      fit: BoxFit.fill),
                ),
              ),
              // Name title and email title in Drawer header
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                        '${appData.usuario.user!.nombre} ${appData.usuario.user!.apellido}',
                        maxLines: 2,
                        style: TextStyle(
                          color: Color(0xff000000),
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0.32,
                        )),
                    const SizedBox(
                      height: 5,
                    ),
                    Text("${appData.usuario.user!.email}",
                        style: TextStyle(
                          color: Color(0xffacb1c0),
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0.23466665267944334,
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20.h,
        ),
        Column(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.popAndPushNamed(context, '/home');
              },
              child: menuItem('assets/images/home--menu-icon.svg', 'Inicio'),
            ),
            SizedBox(
              height: 30.h,
            ),
            GestureDetector(
              onTap: () {
                Navigator.popAndPushNamed(context, '/citas');
              },
              child:
                  menuItem('assets/images/citas-icon.svg', 'Citas Diagnóstico'),
            ),
            SizedBox(
              height: 30.h,
            ),
            GestureDetector(
              onTap: () {
                Navigator.popAndPushNamed(context, '/citas_historial');
              },
              child: menuItem('assets/images/trabajos-icon.svg',
                  'Historial Citas Diagnóstico'),
            ),
            SizedBox(
              height: 30.h,
            ),
            GestureDetector(
              onTap: () {
                Navigator.popAndPushNamed(context, '/presupuesto');
              },
              child: menuItemwithNotificon('assets/images/presupuesto-icon.svg',
                  'Presupuesto', false, '2'),
            ),
            SizedBox(
              height: 30.h,
            ),
            GestureDetector(
              onTap: () {
                Navigator.popAndPushNamed(context, '/citas_ejecucion');
              },
              child: menuItem(
                  'assets/images/citas-icon.svg', 'Citas de Ejecución'),
            ),
            SizedBox(
              height: 30.h,
            ),
            GestureDetector(
              onTap: () {
                Navigator.popAndPushNamed(context, '/trabajos');
              },
              child: menuItem(
                  'assets/images/trabajos-icon.svg', 'Historial Trabajos'),
            ),
            SizedBox(
              height: 30.h,
            ),
            GestureDetector(
              onTap: () {
                Navigator.popAndPushNamed(context, '/pagos');
              },
              child:
                  menuItem('assets/images/pagos-icon.svg', 'Historial Pagos'),
            ),
            SizedBox(
              height: 30.h,
            ),
            FutureBuilder(
                future: Client().notificacionesNuevas(),
                builder: (context, snapshot) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.popAndPushNamed(context, '/notification');
                    },
                    child: menuItemwithNotificon(
                        'assets/images/notification-menu-icon.svg',
                        'Notificaciones',
                        true,
                        snapshot.hasData ? snapshot.data.toString() : "0"),
                  );
                }),
            SizedBox(
              height: 30.h,
            ),
            GestureDetector(
              onTap: () {
                Navigator.popAndPushNamed(context, '/chat');
              },
              child: menuItem('assets/images/chat-icon.svg', 'Chat'),
            ),
            SizedBox(
              height: 30.h,
            ),
            GestureDetector(
              onTap: () async {
                confirmarEliminar(context);
              },
              child:
                  menuItem('assets/images/log-out-icon.svg', 'Eliminar cuenta'),
            ),
            SizedBox(
              height: 30.h,
            ),
            GestureDetector(
              onTap: () async {
                await Client().logout(context);
                Navigator.of(context)
                    .pushNamedAndRemoveUntil("/loginScreen", (route) => false);
              },
              child: menuItem('assets/images/log-out-icon.svg', 'Salir'),
            ),
            SizedBox(
              height: 150.h,
            ),
            Column(
              children: [
                Text("Términos y condiciones",
                    style: TextStyle(
                      color: Color(0xff1a1824),
                      fontSize: 12,
                      fontWeight: FontWeight.w300,
                      fontStyle: FontStyle.normal,
                    )),
                SizedBox(
                  height: 15.w,
                ),
                Text("Política de Privacidad",
                    style: TextStyle(
                      color: Color(0xff1a1824),
                      fontSize: 12,
                      fontWeight: FontWeight.w300,
                      fontStyle: FontStyle.normal,
                    )),
              ],
            )
          ],
        ),
      ]),
    );
  }

  Padding menuItem(String iconPath, String itemName) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 23.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: 18,
          ),
          SizedBox(
            width: 10,
          ),
          SvgPicture.asset(iconPath),
          SizedBox(
            width: 20,
          ),
          Text(itemName,
              style: TextStyle(
                color: Color(0xff1e2432),
                fontSize: 17,
                fontWeight: FontWeight.w300,
                fontStyle: FontStyle.normal,
                letterSpacing: 0.3199999809265136,
              ))
        ],
      ),
    );
  }
}

Padding menuItemwithNotificon(
    String iconPath, String itemName, bool unread, String noOfreadmessages) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 23.0),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        unread
            ? Container(
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xff20B213),
                ),
                child: Center(
                  child: Text(
                    noOfreadmessages,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              )
            : Visibility(
                visible: true,
                child: Container(
                  width: 20,
                  height: 20,
                )),
        SizedBox(
          width: 10,
        ),
        SvgPicture.asset(iconPath),
        SizedBox(
          width: 20,
        ),
        Text(itemName,
            style: TextStyle(
              color: Color(0xff1e2432),
              fontSize: 17,
              fontWeight: FontWeight.w300,
              fontStyle: FontStyle.normal,
              letterSpacing: 0.3199999809265136,
            ))
      ],
    ),
  );
}
