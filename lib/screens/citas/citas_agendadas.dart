import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:homie/client.dart';
import 'package:homie/models/visita_tecnica/visita_tecnica.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:intl/intl.dart';
import 'package:oktoast/oktoast.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class CitaAgendasScreen extends StatefulWidget {
  const CitaAgendasScreen({Key? key}) : super(key: key);

  @override
  _CitaAgendasScreenState createState() => _CitaAgendasScreenState();
}

class _CitaAgendasScreenState extends State<CitaAgendasScreen> {
  late VisitaTecnica visita;

  String getFormattedDate() {
    return DateFormat("MMMM dd, yyyy")
        .format(DateTime.parse(visita.dia.toString()));
  }

  void cancelarDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: const Text("¿Está seguro que desea cancelar la cita?"),
              actions: [
                TextButton(
                  onPressed: () async {
                    await Client()
                        .updateVisita({"id": visita.id, "status": "Cancelado"});
                    Navigator.of(context)
                        .pushNamedAndRemoveUntil("/home", (route) => false);
                  },
                  child: const Text(
                    "Cancelar",
                    style: TextStyle(color: Colors.red),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text(
                    "No cancelar",
                    style: TextStyle(color: Colors.green),
                  ),
                )
              ]);
        });
  }

  void reagendarDialog() {
    final _keyform = GlobalKey<FormBuilderState>();

    List<String> daytime = const <String>[
      "Bloque mañana 1 (8:00 a 11:00)",
      "Bloque mañana 2 (11:00 a 14:00)",
      "Bloque tarde 3 (14:00 a 17:00)",
      "Bloque tarde 4 (17:00 a 20:00)"
    ];
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              content: Container(
                height: 200,
                child: FormBuilder(
                  key: _keyform,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 8.0.w),
                        child: Text('Fecha y Hora Estimada',
                            style: TextStyle(
                              color: Color(0xff3a3f47),
                              fontSize: 16.sp,
                              fontWeight: FontWeight.w800,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0.16,
                            )),
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      FormBuilderDateTimePicker(
                        name: "weekdays",
                        inputType: InputType.date,
                        selectableDayPredicate: (day) {
                          return day.weekday != 7;
                        },
                        initialValue: DateTime.parse(visita.dia.toString()),
                        firstDate: DateTime.now().add(Duration(days: 1)),
                        initialDate: DateTime.now().add(Duration(days: 1)),
                        decoration: const InputDecoration(
                          labelText: "Día de la semana",
                          border: InputBorder.none,
                        ),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                      ),
                      FormBuilderDropdown(
                        name: "daytime",
                        decoration: const InputDecoration(
                          labelText: "Hora del día",
                          border: InputBorder.none,
                        ),
                        initialValue: visita.hora,
                        hint: Text('Elige Hora del día'),
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required(context)]),
                        items: daytime
                            .map((daytime) => DropdownMenuItem(
                                value: daytime, child: Text("$daytime")))
                            .toList(),
                      ),
                    ],
                  ),
                ),
              ),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text(
                    "No reagendar",
                    style: TextStyle(color: Colors.red),
                  ),
                ),
                TextButton(
                  onPressed: () async {
                    var fields = _keyform.currentState!.fields;
                    await Client().updateVisita({
                      "id": visita.id,
                      "dia": fields["weekdays"]!.value.toString(),
                      "hora": fields["daytime"]!.value.toString()
                    });
                    Navigator.of(context)
                        .pushNamedAndRemoveUntil("/home", (route) => false);
                  },
                  child: const Text(
                    "Reagendar",
                    style: TextStyle(color: Colors.green),
                  ),
                )
              ]);
        });
  }

  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    visita = ModalRoute.of(context)!.settings.arguments as VisitaTecnica;
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return OKToast(
      child: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
          colors: [Color(0xff83dcff), Color(0xffeffdff)],
          stops: [0, 1],
          begin: Alignment(-1.00, -0.02),
          end: Alignment(1.00, 0.02),
        )),
        child: SafeArea(
          child: Scaffold(
            key: _globalKey,
            endDrawer: Navbar(),
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                //NAVIGATING BACK TO THE HOMEPAGE
                onPressed: () => Navigator.of(context).pop(),
              ),
              actions: <Widget>[
                Padding(
                  padding: EdgeInsets.only(right: 20.0.w),
                  child: GestureDetector(
                    //THIS FUNCTION WILL OPEN MENU DRAWER
                    onTap: () {
                      _globalKey.currentState?.openEndDrawer();
                    },
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          width: 58.41377258300781.w,
                          height: 57.504638671875.w,
                          decoration: new BoxDecoration(
                            color: Color(0xff009ade),
                            shape: BoxShape.circle,
                          ),
                        ),
                        SvgPicture.asset('assets/images/menu-icon.svg'),
                      ],
                    ),
                  ),
                ),
              ],
              toolbarHeight: 120.h,
            ),
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 16.0.w),
                  child: Text("Citas Agendadas",
                      style: TextStyle(
                        color: Color(0xff00013c),
                        fontSize: 23.sp,
                        fontWeight: FontWeight.w900,
                        fontStyle: FontStyle.normal,
                      )),
                ),
                SizedBox(
                  height: 24.h,
                ),
                Expanded(
                  child: Container(
                    width: 375.w,
                    decoration: BoxDecoration(
                      color: Color(0xffffffff),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25)),
                      boxShadow: const [
                        BoxShadow(
                            color: Color(0x7c000000),
                            offset: Offset(5, -3),
                            blurRadius: 6,
                            spreadRadius: 0)
                      ],
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(16.0.w),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Stack(
                                alignment: Alignment.center,
                                children: [
                                  Container(
                                      width: 92.w,
                                      height: 92.w,
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Color(0xfff7f7f7))),
                                  Container(
                                    width: 76.w,
                                    height: 76.w,
                                    decoration: const BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(0x23646464),
                                            offset: Offset(-7, 3),
                                            blurRadius: 4,
                                            spreadRadius: 0)
                                      ],
                                      color: Color(0xff009ade),
                                      shape: BoxShape.circle,
                                    ),
                                    child: SvgPicture.asset(
                                      'assets/images/test-icon.svg',
                                      fit: BoxFit.scaleDown,
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                width: 20.w,
                              ),
                              Text("Orden #${visita.id}",
                                  style: TextStyle(
                                    color: Color(0xff222b45),
                                    fontSize: 25.sp,
                                    fontWeight: FontWeight.w600,
                                    fontStyle: FontStyle.normal,
                                  ))
                            ],
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          Container(
                            width: 343.w,
                            decoration: BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(12),
                              boxShadow: const [
                                BoxShadow(
                                    color: Color(0x29000000),
                                    offset: Offset(0, 3),
                                    blurRadius: 6,
                                    spreadRadius: 0)
                              ],
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.w, vertical: 25.w),
                              child: Column(children: [
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Día",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text(getFormattedDate(),
                                          style: TextStyle(
                                            color: Color(0xff007aff),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]),
                                SizedBox(
                                  height: 20.h,
                                ),
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Hora",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text(visita.hora.toString(),
                                          style: TextStyle(
                                            color: Color(0xff007aff),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]),
                                SizedBox(
                                  height: 15.h,
                                ),
                                Divider(
                                  height: 2.w,
                                  color: Color(0xffD9DEE4),
                                  indent: 16.3.w,
                                ),
                                SizedBox(
                                  height: 20.h,
                                ),
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Status",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text(visita.status.toString(),
                                          style: TextStyle(
                                            color: Color(0xff222b45),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]),
                                SizedBox(
                                  height: 20.h,
                                ),
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Tipo",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text(
                                          visita.tipoServicio!.nombre
                                              .toString(),
                                          style: TextStyle(
                                            color: Color(0xff222b45),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]),
                                SizedBox(
                                  height: 20.h,
                                ),
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Problema",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text(visita.descripcion.toString(),
                                          style: TextStyle(
                                            color: Color(0xff222b45),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]),
                              ]),
                            ),
                          ),
                          SizedBox(
                            height: 28.h,
                          ),
                          //BUTTTONs
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: 160.w,
                                height: 49.w,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1,
                                    color: Color(0xffF5FAFC),
                                    style: BorderStyle.solid,
                                  ),
                                  borderRadius: BorderRadius.circular(38.0.r),
                                  color: const Color(0xff21409a),
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Color(0x29000000),
                                      offset: Offset(5, 3),
                                      blurRadius: 6,
                                      spreadRadius: 0,
                                    ),
                                  ],
                                ),
                                child: TextButton(
                                  //THIS WILL REDIRECT TO NEXT PAGE
                                  onPressed: reagendarDialog,
                                  child: Text(
                                    "REAGENDAR",
                                    style: TextStyle(
                                      color: Color(0xffffffff),
                                      fontSize: 15.sp,
                                      fontWeight: FontWeight.w800,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 20.w,
                              ),
                              Container(
                                width: 160.w,
                                height: 49.w,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1,
                                    color: Color(0xffF5FAFC),
                                    style: BorderStyle.solid,
                                  ),
                                  borderRadius: BorderRadius.circular(38.0.r),
                                  color: Color(0xffffca51),
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Color(0x29000000),
                                      offset: Offset(5, 3),
                                      blurRadius: 6,
                                      spreadRadius: 0,
                                    ),
                                  ],
                                ),
                                child: TextButton(
                                  //THIS WILL REDIRECT TO NEXT PAGE
                                  onPressed: cancelarDialog,
                                  child: Text(
                                    "CANCELAR",
                                    style: TextStyle(
                                      color: Color(0xffffffff),
                                      fontSize: 15.sp,
                                      fontWeight: FontWeight.w800,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
            //BOTTOM BAR WITH FLOATING BTN
            floatingActionButton: WhatsappButton(),
            floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
            bottomNavigationBar: const BottomNav(),
          ),
        ),
      ),
    );
  }
}
