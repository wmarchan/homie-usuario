import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:homie/models/visita_tecnica/visita_tecnica.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class CitasConfirmadaScreen extends StatefulWidget {
  const CitasConfirmadaScreen({Key? key}) : super(key: key);

  @override
  _CitasConfirmadaScreenState createState() => _CitasConfirmadaScreenState();
}

class _CitasConfirmadaScreenState extends State<CitasConfirmadaScreen> {
  late VisitaTecnica visita;

  String getFormattedDate() {
    return DateFormat("MMMM dd, yyyy")
        .format(DateTime.parse(visita.dia.toString()));
  }

  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  Widget build(BuildContext context) {
    visita = ModalRoute.of(context)!.settings.arguments as VisitaTecnica;

    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
        colors: [Color(0xff83dcff), Color(0xffeffdff)],
        stops: [0, 1],
        begin: Alignment(-1.00, -0.02),
        end: Alignment(1.00, 0.02),
      )),
      child: SafeArea(
        child: Scaffold(
          key: _globalKey,
          endDrawer: Navbar(),
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              //NAVIGATING BACK TO THE HOMEPAGE
              onPressed: () => Navigator.of(context).pop(),
            ),
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 20.0.w),
                child: GestureDetector(
                  //THIS FUNCTION WILL OPEN MENU DRAWER
                  onTap: () {
                    _globalKey.currentState?.openEndDrawer();
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 58.41377258300781.w,
                        height: 57.504638671875.w,
                        decoration: new BoxDecoration(
                          color: Color(0xff009ade),
                          shape: BoxShape.circle,
                        ),
                      ),
                      SvgPicture.asset('assets/images/menu-icon.svg'),
                    ],
                  ),
                ),
              ),
            ],
            toolbarHeight: 120.h,
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 16.0.w),
                child: Text("Citas Confirmada",
                    style: TextStyle(
                      color: Color(0xff00013c),
                      fontSize: 23.sp,
                      fontWeight: FontWeight.w900,
                      fontStyle: FontStyle.normal,
                    )),
              ),
              SizedBox(
                height: 24.h,
              ),
              Expanded(
                child: Container(
                  width: 375.w,
                  decoration: BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0x7c000000),
                          offset: Offset(5, -3),
                          blurRadius: 6,
                          spreadRadius: 0)
                    ],
                  ),
                  child: Padding(
                    padding:
                        EdgeInsets.only(top: 16.0.w, left: 10.w, right: 10.w),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Stack(
                                alignment: Alignment.center,
                                children: [
                                  Container(
                                      width: 92.w,
                                      height: 92.w,
                                      decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Color(0xfff7f7f7))),
                                  Container(
                                    width: 76.w,
                                    height: 76.w,
                                    decoration: new BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(0x23646464),
                                            offset: Offset(-7, 3),
                                            blurRadius: 4,
                                            spreadRadius: 0)
                                      ],
                                      color: Color(0xff009ade),
                                      shape: BoxShape.circle,
                                    ),
                                    child: SvgPicture.asset(
                                      'assets/images/test-icon.svg',
                                      fit: BoxFit.scaleDown,
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                width: 20.w,
                              ),
                              Text("Orden #${visita.id}",
                                  style: TextStyle(
                                    color: Color(0xff222b45),
                                    fontSize: 25.sp,
                                    fontWeight: FontWeight.w600,
                                    fontStyle: FontStyle.normal,
                                  ))
                            ],
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          Container(
                            width: 343.w,
                            decoration: new BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(12),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0x29000000),
                                    offset: Offset(0, 3),
                                    blurRadius: 6,
                                    spreadRadius: 0)
                              ],
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.w, vertical: 25.w),
                              child: Column(children: [
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Día",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text(getFormattedDate(),
                                          style: TextStyle(
                                            color: Color(0xff007aff),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]),
                                SizedBox(
                                  height: 20.h,
                                ),
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Hora",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text(visita.hora.toString(),
                                          style: TextStyle(
                                            color: Color(0xff007aff),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]),
                                SizedBox(
                                  height: 15.h,
                                ),
                                Divider(
                                  height: 2.w,
                                  color: Color(0xffD9DEE4),
                                  indent: 16.3.w,
                                ),
                                SizedBox(
                                  height: 20.h,
                                ),
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Status",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text(visita.status.toString(),
                                          style: TextStyle(
                                            color: Color(0xff222b45),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]),
                                SizedBox(
                                  height: 20.h,
                                ),
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Tipo",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text(
                                          visita.tipoServicio!.nombre
                                              .toString(),
                                          style: TextStyle(
                                            color: Color(0xff222b45),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]),
                                SizedBox(
                                  height: 20.h,
                                ),
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Problema",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        child:
                                            Text(visita.descripcion.toString(),
                                                style: TextStyle(
                                                  color: Color(0xff222b45),
                                                  fontSize: 15.sp,
                                                  fontWeight: FontWeight.w400,
                                                  fontStyle: FontStyle.normal,
                                                )),
                                      )
                                    ]),
                              ]),
                            ),
                          ),
                          SizedBox(height: 10.h),
                          //MAP IMAGE CARD
                          Container(
                            width: 319.0.w,
                            height: 112.0.w,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              color: const Color(0xFFF9F9FC),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black.withOpacity(0.16),
                                  offset: Offset(5.0, 3.0),
                                  blurRadius: 6.0,
                                ),
                              ],
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8.0.w, vertical: 8.0.h),
                              //inside the card
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(20.0),
                                child: GoogleMap(
                                  initialCameraPosition: CameraPosition(
                                    target: LatLng(
                                      visita.ubicacion!.coordinates![0],
                                      visita.ubicacion!.coordinates![1],
                                    ),
                                    zoom: 16,
                                  ),
                                  markers: {
                                    Marker(
                                        markerId: MarkerId("1"),
                                        position: LatLng(
                                            visita.ubicacion!.coordinates![0],
                                            visita.ubicacion!.coordinates![1]))
                                  },
                                  padding: EdgeInsets.only(bottom: 75),
                                  zoomControlsEnabled: false,
                                  myLocationEnabled: true,
                                  myLocationButtonEnabled: true,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20.h,
                          ),
                          Container(
                            width: 343.w,
                            height: 68.w,
                            decoration: new BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(12),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0x29000000),
                                    offset: Offset(0, 3),
                                    blurRadius: 6,
                                    spreadRadius: 0)
                              ],
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.w, vertical: 16.w),
                              child: Column(children: [
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Dirección",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Expanded(
                                        child: Text(visita.direccion.toString(),
                                            style: TextStyle(
                                              color: Color(0xff222b45),
                                              fontSize: 15.sp,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                      )
                                    ]),
                              ]),
                            ),
                          ),
                          SizedBox(
                            height: 20.h,
                          ),
                          Container(
                            width: 343.w,
                            height: 68.w,
                            decoration: new BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.circular(12),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0x29000000),
                                    offset: Offset(0, 3),
                                    blurRadius: 6,
                                    spreadRadius: 0)
                              ],
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Contactar al Técnico ",
                                        style: TextStyle(
                                          color: Color(0xff2c333e),
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    Text(
                                        "${visita.tecnico!.nombre} ${visita.tecnico!.apellido}",
                                        style: TextStyle(
                                          color: Color(0xff929fb3),
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        ))
                                  ],
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 8.0.w),
                                  child: Row(
                                    children: [
                                      IconButton(
                                        onPressed: () {
                                          launch(
                                              "tel://${visita.tecnico!.telefono}");
                                        },
                                        icon: SvgPicture.asset(
                                            "assets/images/call-icon.svg"),
                                      ),
                                      SizedBox(
                                        width: 8.w,
                                      ),
                                      IconButton(
                                        onPressed: () {
                                          launch(
                                              "sms://${visita.tecnico!.telefono}");
                                        },
                                        icon: SvgPicture.asset(
                                            "assets/images/message-icon.svg"),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 28.h,
                          ),

                          SizedBox(
                            height: 50.w,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          //BOTTOM BAR WITH FLOATING BTN
          floatingActionButton: WhatsappButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: const BottomNav(),
        ),
      ),
    );
  }
}
