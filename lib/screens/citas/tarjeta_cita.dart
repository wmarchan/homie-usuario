import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

import 'dashed_line_vertical_painter.dart';

class TarjetaCita extends StatelessWidget {
  TarjetaCita(
      {Key? key,
      required this.onTap,
      required this.fecha,
      required this.titulo,
      required this.bloque,
      required this.status})
      : super(key: key);

  Function onTap;
  String fecha;
  String titulo;
  String bloque;
  String status;

  String getFormattedDate() {
    return DateFormat("MMMM dd, yyyy").format(DateTime.parse(fecha));
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap(),
      child: SizedBox(
        height: 148.h,
        width: 345.w,
        child: Row(
          children: [
            Column(
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                        width: 60.w,
                        height: 60.w,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle, color: Color(0xfff7f7f7))),
                    Container(
                      width: 50.w,
                      height: 50.w,
                      decoration: const BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x23646464),
                              offset: Offset(-7, 3),
                              blurRadius: 4,
                              spreadRadius: 0)
                        ],
                        color: Color(0xff009ade),
                        shape: BoxShape.circle,
                      ),
                      child: SvgPicture.asset(
                        'assets/images/test-icon.svg',
                        fit: BoxFit.scaleDown,
                      ),
                    )
                  ],
                ),
                CustomPaint(
                    size: Size(1, 76.h), painter: DashedLineVerticalPainter())
              ],
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 12.0),
                            child: Text(getFormattedDate(),
                                style: TextStyle(
                                  color: const Color(0xff000000),
                                  fontSize: 15.sp,
                                  fontWeight: FontWeight.w300,
                                  fontStyle: FontStyle.normal,
                                )),
                          ),
                          Expanded(
                            child: Container(),
                          ),
                          SvgPicture.asset('assets/images/more-icon.svg')
                        ]),
                  ),
                  SizedBox(
                    height: 12.h,
                  ),
                  Container(
                    width: 267.w,
                    height: 100.w,
                    decoration: BoxDecoration(
                        color: Color(0xfffbfbfb),
                        border: Border.all(
                          color: Color(0xffE1E1E1),
                        ),
                        borderRadius: const BorderRadius.only(
                          topRight: Radius.circular(15),
                          bottomLeft: Radius.circular(15),
                          bottomRight: Radius.circular(15),
                        )),
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: 12.0.w,
                          top: 12.0.w,
                          bottom: 12.0.w,
                          right: 22.w),
                      child: Column(children: [
                        Row(children: [
                          Text(titulo,
                              style: TextStyle(
                                color: Color(0xff000000),
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w300,
                                fontStyle: FontStyle.normal,
                              )),
                        ]),
                        SizedBox(height: 8.h),
                        Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SvgPicture.asset("assets/images/time-icon.svg"),
                              SizedBox(width: 6.w),
                              Text(bloque,
                                  style: TextStyle(
                                    color: Color(0xff858585),
                                    fontSize: 11.sp,
                                    fontWeight: FontWeight.w300,
                                    fontStyle: FontStyle.normal,
                                  ))
                            ]),
                        SizedBox(
                          height: 10.h,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                  width: 27.w,
                                  height: 27.w,
                                  decoration: const BoxDecoration(
                                      color: Color(0xffd3d3d3),
                                      shape: BoxShape.circle),
                                  child: Icon(Icons.add),
                                ),
                              ),
                              Text(status,
                                  style: TextStyle(
                                    color: Color(0xff9f9c55),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w300,
                                    fontStyle: FontStyle.normal,
                                  ))
                            ])
                      ]),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
