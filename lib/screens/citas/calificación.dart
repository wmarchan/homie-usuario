// ignore_for_file: non_constant_identifier_names, file_names

import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_rating_stars/flutter_rating_stars.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:homie/client.dart';
import 'package:homie/models/presupuesto/presupuesto.dart';
import 'package:homie/models/visita_tecnica/visita_tecnica.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/citas/citas_main.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:homie/screens/trabajos/trabajos.dart';
import 'package:homie/singletons/AppData.dart';
import 'package:intl/intl.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class CalificacionVisita extends StatefulWidget {
  const CalificacionVisita({Key? key}) : super(key: key);

  @override
  _CalificacionVisitaState createState() => _CalificacionVisitaState();
}

class _CalificacionVisitaState extends State<CalificacionVisita> {
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormBuilderState>();

  String getFormattedDate() {
    return DateFormat("MMMM dd, yyyy")
        .format(DateTime.parse(visita!.dia.toString()));
  }

  VisitaTecnica? visita;
  Presupuesto? presupuesto;
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero).then((value) {
      Map data = ModalRoute.of(context)!.settings.arguments as Map;
      if (data["visita"] != null) {
        setState(() {
          visita = data["visita"] as VisitaTecnica;
        });
      }
      if (data["presupuesto"] != null) {
        setState(() {
          presupuesto = data["presupuesto"] as Presupuesto;
          visita = presupuesto!.visitaTecnica;
        });
      }
    });
  }

  void calificar() async {
    if (_formKey.currentState!.saveAndValidate()) {
      var fields = _formKey.currentState!.fields;
      await Client().calificar({
        "calificacion": fields["calificacion"]!.value,
        "comentarios": fields["comentarios"]!.value,
        "cumplioFecha": fields["cumplioFecha"]!.value,
        "explicoSolucion": fields["explicoSolucion"]!.value,
        "realizoPruebas": fields["realizoPruebas"]!.value,
        "recomendaciones": fields["recomendaciones"]!.value,
        "limpio": fields["limpio"]!.value,
        "UsuarioId": appData.usuario.user!.id,
        "TecnicoId": visita!.tecnico!.id,
        "VisitaTecnicaId": visita!.id,
        "PresupuestoId": presupuesto?.id
      });
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            /// DIALOG BOX
            return Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6.r),
              ),
              child: Container(
                width: 375.w,
                height: 450.w,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Color(0xffffffff),
                  boxShadow: const [
                    BoxShadow(
                        color: Color(0x7c000000),
                        offset: Offset(5, -3),
                        blurRadius: 6,
                        spreadRadius: 0)
                  ],
                ),
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 6.w, vertical: 10.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                              onPressed: () {
                                Navigator.of(context).pushNamedAndRemoveUntil(
                                    "/home", (route) => false);
                              },
                              icon: Icon(
                                Icons.close_outlined,
                                size: 30.w,
                              )),
                        ],
                      ),
                      SvgPicture.asset('assets/images/agendaDialoge.svg'),
                      SizedBox(
                        height: 30.h,
                      ),
                      Text(
                        "GRACIAS",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xff222b45),
                          fontSize: 17.sp,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 48.0.w),
                        child: Text("Ya recibimos su opinión.",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xff414040),
                              fontSize: 15.sp,
                              fontWeight: FontWeight.w300,
                              fontStyle: FontStyle.normal,
                            )),
                      ),
                      SizedBox(
                        height: 27.h,
                      ),
                    ],
                  ),
                ),
              ),
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return visita == null
        ? Container()
        : Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [Color(0xff83dcff), Color(0xffeffdff)],
                stops: [0, 1],
                begin: Alignment(-1.00, -0.02),
                end: Alignment(1.00, 0.02),
                // angle: 91,
                // scale: undefined,
              ),
            ),
            child: Scaffold(
              key: _globalKey,
              endDrawer: Navbar(),
              appBar: AppBar(
                backgroundColor: Colors.transparent,
                elevation: 0,
                leading: IconButton(
                  icon: const Icon(Icons.arrow_back, color: Colors.black),
                  //NAVIGATING BACK TO THE HOMEPAGE
                  onPressed: () => Navigator.of(context).pop(),
                ),
                actions: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 20.0.w),
                    child: GestureDetector(
                      //THIS FUNCTION WILL OPEN MENU DRAWER
                      onTap: () {
                        _globalKey.currentState?.openEndDrawer();
                      },
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            width: 58.41377258300781.w,
                            height: 57.504638671875.w,
                            decoration: const BoxDecoration(
                              color: Color(0xff009ade),
                              shape: BoxShape.circle,
                            ),
                          ),
                          SvgPicture.asset('assets/images/menu-icon.svg'),
                        ],
                      ),
                    ),
                  ),
                ],
                toolbarHeight: 80.h,
              ),

              backgroundColor: Colors.transparent,
              body: SafeArea(
                bottom: false,
                child: Column(
                  children: [
                    Text(
                      "Calificación",
                      style: TextStyle(
                        color: const Color(0xff00013c),
                        fontSize: 23.sp,
                        fontWeight: FontWeight.w900,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(height: 70.0.h),
                    Expanded(
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: [
                          //Background container containing the list
                          //The Profile Avatar will set upon this container
                          Container(
                            width: 375.w,
                            decoration: const BoxDecoration(
                              color: Color(0xffffffff),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(25),
                                  topRight: Radius.circular(25)),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0x7c000000),
                                    offset: Offset(5, -3),
                                    blurRadius: 6,
                                    spreadRadius: 0)
                              ],
                            ),
                            child: FormBuilder(
                              key: _formKey,
                              child: Column(
                                children: [
                                  Flexible(
                                      child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 15.0.w),
                                    child: ListView(
                                      children: [
                                        SizedBox(height: 49.4.h),
                                        ProfileTile(
                                          title: 'Información',
                                          description:
                                              "ID: ${visita!.id}\nFecha Visita: ${getFormattedDate()}",
                                        ),
                                        SizedBox(height: 17.h),
                                        Container(
                                          height: 62.0.h,
                                          decoration: BoxDecoration(
                                            color: const Color(0xff009ade)
                                                .withOpacity(
                                              0.10000000149011612,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(30),
                                          ),
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 18.0.h,
                                                horizontal: 23.1.w),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  "Calificación General",
                                                  style: TextStyle(
                                                    color:
                                                        const Color(0xff222b45),
                                                    fontSize: 17.sp,
                                                    fontWeight: FontWeight.w700,
                                                    fontStyle: FontStyle.normal,
                                                  ),
                                                ),
                                                SizedBox(width: 30.w),
                                                FormBuilderField(
                                                    initialValue: 5.0,
                                                    builder:
                                                        (FormFieldState field) {
                                                      return RatingStars(
                                                        value: field.value,
                                                        onValueChanged: (val) {
                                                          field.didChange(val);
                                                        },
                                                        starBuilder:
                                                            (index, color) =>
                                                                Icon(
                                                          Icons.star,
                                                          color: color,
                                                        ),
                                                        starCount: 5,
                                                        starSize: 15.sp,
                                                        maxValue: 5,
                                                        starSpacing: 5,
                                                        valueLabelVisibility:
                                                            false,
                                                        animationDuration:
                                                            const Duration(
                                                                milliseconds:
                                                                    1000),
                                                        starOffColor:
                                                            const Color(
                                                                0xffe7e8ea),
                                                        starColor: Colors
                                                            .yellow.shade600,
                                                      );
                                                    },
                                                    name: "calificacion")
                                              ],
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 17.h),
                                        Container(
                                          decoration: BoxDecoration(
                                            color: const Color(0xff009ade)
                                                .withOpacity(
                                              0.10000000149011612,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(30.r),
                                          ),
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                              vertical: 18.0.h,
                                              horizontal: 10,
                                            ),
                                            child: Column(
                                              children: [
                                                // True False Section
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 18),
                                                  child: Row(
                                                    children: [
                                                      Text(
                                                        "Servicio",
                                                        style: TextStyle(
                                                          color: const Color(
                                                              0xff222b45),
                                                          fontSize: 17.sp,
                                                          fontWeight:
                                                              FontWeight.w700,
                                                          fontStyle:
                                                              FontStyle.normal,
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: Container(),
                                                      ),
                                                      Text(
                                                        "Si",
                                                        style: TextStyle(
                                                          color: const Color(
                                                              0xff000000),
                                                          fontSize: 17.sp,
                                                          fontWeight:
                                                              FontWeight.w700,
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          letterSpacing: 0.14,
                                                        ),
                                                      ),
                                                      SizedBox(width: 28.w),
                                                      Text(
                                                        "No",
                                                        style: TextStyle(
                                                          color: const Color(
                                                              0xff000000),
                                                          fontSize: 17.sp,
                                                          fontWeight:
                                                              FontWeight.w700,
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          letterSpacing: 0.14,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                FormBuilderField(
                                                  name: "cumplioFecha",
                                                  initialValue: true,
                                                  builder:
                                                      (FormFieldState field) {
                                                    return Row(
                                                      children: [
                                                        Text(
                                                          "Se cumplió fecha y hora programada",
                                                          style: TextStyle(
                                                            color: const Color(
                                                                0xff8f9bb3),
                                                            fontSize: 12.sp,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            fontStyle: FontStyle
                                                                .normal,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(),
                                                        ),
                                                        Checkbox(
                                                          fillColor:
                                                              MaterialStateProperty
                                                                  .resolveWith(
                                                            (states) =>
                                                                const Color(
                                                                    0xff797777),
                                                          ),
                                                          value: field.value,
                                                          onChanged: (value) {
                                                            field.didChange(
                                                                true);
                                                          },
                                                        ),
                                                        Checkbox(
                                                          fillColor:
                                                              MaterialStateProperty
                                                                  .resolveWith(
                                                            (states) =>
                                                                const Color(
                                                                    0xff797777),
                                                          ),
                                                          value: !field.value,
                                                          onChanged: (value) {
                                                            field.didChange(
                                                                false);
                                                          },
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                ),
                                                FormBuilderField(
                                                  name: "explicoSolucion",
                                                  initialValue: true,
                                                  builder:
                                                      (FormFieldState field) {
                                                    return Row(
                                                      children: [
                                                        Text(
                                                          "Se explicó el posible problema/solución",
                                                          style: TextStyle(
                                                            color: const Color(
                                                                0xff8f9bb3),
                                                            fontSize: 12.sp,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            fontStyle: FontStyle
                                                                .normal,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(),
                                                        ),
                                                        Checkbox(
                                                          fillColor:
                                                              MaterialStateProperty
                                                                  .resolveWith(
                                                            (states) =>
                                                                const Color(
                                                                    0xff797777),
                                                          ),
                                                          value: field.value,
                                                          onChanged: (value) {
                                                            field.didChange(
                                                                true);
                                                          },
                                                        ),
                                                        Checkbox(
                                                          fillColor:
                                                              MaterialStateProperty
                                                                  .resolveWith(
                                                            (states) =>
                                                                const Color(
                                                                    0xff797777),
                                                          ),
                                                          value: !field.value,
                                                          onChanged: (value) {
                                                            field.didChange(
                                                                false);
                                                          },
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                ),
                                                FormBuilderField(
                                                  name: "realizoPruebas",
                                                  initialValue: true,
                                                  builder:
                                                      (FormFieldState field) {
                                                    return Row(
                                                      children: [
                                                        Text(
                                                          "Se realizó pruebas de funcionamiento",
                                                          style: TextStyle(
                                                            color: const Color(
                                                                0xff8f9bb3),
                                                            fontSize: 12.sp,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            fontStyle: FontStyle
                                                                .normal,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(),
                                                        ),
                                                        Checkbox(
                                                          fillColor:
                                                              MaterialStateProperty
                                                                  .resolveWith(
                                                            (states) =>
                                                                const Color(
                                                                    0xff797777),
                                                          ),
                                                          value: field.value,
                                                          onChanged: (value) {
                                                            field.didChange(
                                                                true);
                                                          },
                                                        ),
                                                        Checkbox(
                                                          fillColor:
                                                              MaterialStateProperty
                                                                  .resolveWith(
                                                            (states) =>
                                                                const Color(
                                                                    0xff797777),
                                                          ),
                                                          value: !field.value,
                                                          onChanged: (value) {
                                                            field.didChange(
                                                                false);
                                                          },
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                ),
                                                FormBuilderField(
                                                  name: "recomendaciones",
                                                  initialValue: true,
                                                  builder:
                                                      (FormFieldState field) {
                                                    return Row(
                                                      children: [
                                                        Text(
                                                          "Se realizaron recomendaciones de uso",
                                                          style: TextStyle(
                                                            color: const Color(
                                                                0xff8f9bb3),
                                                            fontSize: 12.sp,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            fontStyle: FontStyle
                                                                .normal,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(),
                                                        ),
                                                        Checkbox(
                                                          fillColor:
                                                              MaterialStateProperty
                                                                  .resolveWith(
                                                            (states) =>
                                                                const Color(
                                                                    0xff797777),
                                                          ),
                                                          value: field.value,
                                                          onChanged: (value) {
                                                            field.didChange(
                                                                true);
                                                          },
                                                        ),
                                                        Checkbox(
                                                          fillColor:
                                                              MaterialStateProperty
                                                                  .resolveWith(
                                                            (states) =>
                                                                const Color(
                                                                    0xff797777),
                                                          ),
                                                          value: !field.value,
                                                          onChanged: (value) {
                                                            field.didChange(
                                                                false);
                                                          },
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                ),
                                                FormBuilderField(
                                                  name: "limpio",
                                                  initialValue: true,
                                                  builder:
                                                      (FormFieldState field) {
                                                    return Row(
                                                      children: [
                                                        Text(
                                                          "Se dejó limpia el área de trabajo",
                                                          style: TextStyle(
                                                            color: const Color(
                                                                0xff8f9bb3),
                                                            fontSize: 12.sp,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            fontStyle: FontStyle
                                                                .normal,
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Container(),
                                                        ),
                                                        Checkbox(
                                                          fillColor:
                                                              MaterialStateProperty
                                                                  .resolveWith(
                                                            (states) =>
                                                                const Color(
                                                                    0xff797777),
                                                          ),
                                                          value: field.value,
                                                          onChanged: (value) {
                                                            field.didChange(
                                                                true);
                                                          },
                                                        ),
                                                        Checkbox(
                                                          fillColor:
                                                              MaterialStateProperty
                                                                  .resolveWith(
                                                            (states) =>
                                                                const Color(
                                                                    0xff797777),
                                                          ),
                                                          value: !field.value,
                                                          onChanged: (value) {
                                                            field.didChange(
                                                                false);
                                                          },
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 17.h),
                                        Stack(
                                          clipBehavior: Clip.none,
                                          children: [
                                            Container(
                                              width: 340.w,
                                              decoration: BoxDecoration(
                                                color: const Color(0xff009ade)
                                                    .withOpacity(
                                                  0.10000000149011612,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(30.r),
                                              ),
                                              child: Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 25.w,
                                                    vertical: 11.0.h),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      "Comentarios",
                                                      style: TextStyle(
                                                        color: const Color(
                                                            0xff222b45),
                                                        fontSize: 17.sp,
                                                        fontWeight:
                                                            FontWeight.w700,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                      ),
                                                    ),
                                                    SizedBox(height: 12.0.h),
                                                    Container(
                                                      width: 298.w,
                                                      decoration: BoxDecoration(
                                                        color: const Color(
                                                            0xffffffff),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10.r),
                                                      ),
                                                      child: Padding(
                                                        padding: EdgeInsets.all(
                                                            10.0.w),
                                                        // ignore: prefer_const_constructors
                                                        child:
                                                            FormBuilderTextField(
                                                          name: "comentarios",
                                                          maxLines: 3,
                                                          decoration:
                                                              const InputDecoration(
                                                            hintText:
                                                                'Por favor escriba su opinión o comentario……',
                                                            border: InputBorder
                                                                .none,
                                                          ),
                                                          textAlign:
                                                              TextAlign.start,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Positioned(
                                              top: 135.0.h,
                                              left: 90.0.w,
                                              child: InkWell(
                                                onTap: calificar,
                                                child: Container(
                                                  width: 160.w,
                                                  height: 49.h,
                                                  decoration: BoxDecoration(
                                                    color:
                                                        const Color(0xff009ade),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            80),
                                                    boxShadow: const [
                                                      BoxShadow(
                                                          color:
                                                              Color(0x29000000),
                                                          offset: Offset(5, 3),
                                                          blurRadius: 6,
                                                          spreadRadius: 0)
                                                    ],
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      "ACEPTAR",
                                                      style: TextStyle(
                                                        color: const Color(
                                                            0xffffffff),
                                                        fontSize: 12.sp,
                                                        fontWeight:
                                                            FontWeight.w800,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(height: 50.h),
                                      ],
                                    ),
                                  ))
                                ],
                              ),
                            ),
                          ),

                          //The Profile Avatar and Name
                          Positioned(
                            top: -60.0.h,
                            left: 55.5.h,
                            child: Row(
                              children: [
                                Container(
                                  height: 100.h,
                                  width: 100.w,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: NetworkImage(
                                          visita!.tecnico!.foto.toString()),
                                      fit: BoxFit.fitHeight,
                                    ),
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(width: 15.0.w),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 20.h),
                                  child: Text(
                                    "${visita!.tecnico!.nombre} ${visita!.tecnico!.apellido}",
                                    style: TextStyle(
                                      color: const Color(0xff252525),
                                      fontSize: 25.sp,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: -0.75,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              //BOTTOM BAR WITH FLOATING BTN
              floatingActionButton: WhatsappButton(),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.endFloat,
              bottomNavigationBar: const BottomNav(),
            ),
          );
  }

  Container ProfileTile({
    String title = '',
    String description = '',
  }) {
    return Container(
      width: 340.w,
      height: 122.h,
      decoration: BoxDecoration(
        color: const Color(0xff009ade).withOpacity(
          0.10000000149011612,
        ),
        borderRadius: BorderRadius.circular(30),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 18.0.h, horizontal: 23.1.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(
                color: const Color(0xff222b45),
                fontSize: 18.sp,
                fontWeight: FontWeight.w800,
                fontStyle: FontStyle.normal,
              ),
            ),
            SizedBox(height: 7.0.h),
            Text(
              description,
              style: TextStyle(
                color: const Color(0xff8f9bb3),
                fontSize: 12.sp,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
