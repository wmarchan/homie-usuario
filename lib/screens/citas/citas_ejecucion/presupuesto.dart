import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:homie/client.dart';
import 'package:homie/models/presupuesto/presupuesto.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/presupuesto/presupuesto_detalle_pagado.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:homie/screens/presupuesto/presupuesto_two.dart';
import 'package:homie/screens/presupuesto/presupuesto_detalle.dart';
import 'package:intl/intl.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class CitasEjecucion extends StatefulWidget {
  const CitasEjecucion({Key? key}) : super(key: key);

  @override
  _CitasEjecucionState createState() => _CitasEjecucionState();
}

class _CitasEjecucionState extends State<CitasEjecucion> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  String pageTitlebeforeConfirm = 'Citas de Ejecución';

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [Color(0xff83dcff), Color(0xffeffdff)],
          stops: [0, 1],
          begin: Alignment(-1.00, -0.02),
          end: Alignment(1.00, 0.02),
        ),
      ),
      child: Scaffold(
        key: _globalKey,
        endDrawer: Navbar(),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black),
            //NAVIGATING BACK TO THE HOMEPAGE
            onPressed: () => Navigator.of(context).pop(),
          ),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20.0.w),
              child: GestureDetector(
                //THIS FUNCTION WILL OPEN MENU DRAWER
                onTap: () {
                  _globalKey.currentState?.openEndDrawer();
                },
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: 58.41377258300781.w,
                      height: 57.504638671875.w,
                      decoration: const BoxDecoration(
                        color: Color(0xff009ade),
                        shape: BoxShape.circle,
                      ),
                    ),
                    SvgPicture.asset('assets/images/menu-icon.svg'),
                  ],
                ),
              ),
            ),
          ],
          toolbarHeight: 80.h,
        ),
        backgroundColor: Colors.transparent,
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: EdgeInsets.only(left: 16.0.w),
            child: Text(pageTitlebeforeConfirm,
                style: TextStyle(
                  color: Color(0xff00013c),
                  fontSize: 23.sp,
                  fontWeight: FontWeight.w900,
                  fontStyle: FontStyle.normal,
                )),
          ),
          SizedBox(
            height: 24.h,
          ),
          Expanded(
            child: Container(
              width: 375.w,
              decoration: BoxDecoration(
                color: Color(0xffffffff),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25)),
                boxShadow: [
                  BoxShadow(
                      color: Color(0x7c000000),
                      offset: Offset(5, -3),
                      blurRadius: 6,
                      spreadRadius: 0)
                ],
              ),
              child: Column(children: [
                SizedBox(
                  height: 35.h,
                ),

                // THIS LISTVIEW WHEN Recibidos IS SELECTED
                Expanded(
                  child: FutureBuilder(
                      future: Client().getPresupuestos(
                          ["Aprobada", "Agendado", "En curso"]),
                      builder:
                          (context, AsyncSnapshot<List<Presupuesto>> snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        return ListView.builder(
                          itemCount: snapshot.data!.length,
                          itemBuilder: (context, index) {
                            return Container(
                                margin: EdgeInsets.only(bottom: 10.w),
                                child: PresupuestoCard(snapshot.data![index]));
                          },
                          padding: EdgeInsets.only(left: 23.0.w, right: 23.0.w),
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                        );
                      }),
                )
              ]),
            ),
          ),
        ]),

        //BOTTOM BAR WITH FLOATING BTN
        floatingActionButton: WhatsappButton(),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        bottomNavigationBar: const BottomNav(),
      ),
    );
  }

  InkWell PresupuestoCard(Presupuesto presupuesto) {
    String getFormattedDate() {
      if (presupuesto.fecha != null) {
        return DateFormat("MMMM dd, yyyy")
            .format(DateTime.parse(presupuesto.fecha.toString()));
      } else {
        return "Pendiente";
      }
    }

    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => presupuesto.status == "Esperando Aprobación"
                ? PresupuestoDetalle()
                : PresupuestoDetallePagado(),
            settings: RouteSettings(
              arguments: presupuesto,
            ),
          ),
        );
      },
      child: SizedBox(
        height: 148.h,
        width: 345.w,
        child: Row(
          children: [
            Column(
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                        width: 60.w,
                        height: 60.w,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle, color: Color(0xfff7f7f7))),
                    Container(
                      width: 50.w,
                      height: 50.w,
                      decoration: const BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x23646464),
                              offset: Offset(-7, 3),
                              blurRadius: 4,
                              spreadRadius: 0)
                        ],
                        color: Color(0xff009ade),
                        shape: BoxShape.circle,
                      ),
                      child: SvgPicture.asset(
                        'assets/images/test-icon.svg',
                        fit: BoxFit.scaleDown,
                      ),
                    )
                  ],
                ),
                CustomPaint(
                    size: Size(1, 76.h), painter: DashedLineVerticalPainter())
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 12.0),
                    child: Text(getFormattedDate(),
                        style: TextStyle(
                          color: Color(0xff000000),
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w300,
                          fontStyle: FontStyle.normal,
                        )),
                  ),
                  SizedBox(
                    width: 130.w,
                  ),
                  SvgPicture.asset('assets/images/more-icon.svg')
                ]),
                SizedBox(
                  height: 12.h,
                ),
                Container(
                  width: 267.w,
                  height: 100.w,
                  decoration: BoxDecoration(
                      color: Color(0xfffbfbfb),
                      border: Border.all(
                        color: Color(0xffE1E1E1),
                      ),
                      borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(15),
                        bottomLeft: Radius.circular(15),
                        bottomRight: Radius.circular(15),
                      )),
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: 12.0.w, top: 12.0.w, bottom: 12.0.w, right: 22.w),
                    child: Column(children: [
                      Row(children: [
                        Text("${presupuesto.descripcion}",
                            maxLines: 1,
                            style: TextStyle(
                              color: Color(0xff000000),
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w300,
                              fontStyle: FontStyle.normal,
                            )),
                      ]),
                      SizedBox(height: 8.h),
                      Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset("assets/images/time-icon.svg"),
                            SizedBox(width: 6.w),
                            Text(presupuesto.hora ?? "Pendiente",
                                style: TextStyle(
                                  color: Color(0xff858585),
                                  fontSize: 11.sp,
                                  fontWeight: FontWeight.w300,
                                  fontStyle: FontStyle.normal,
                                ))
                          ]),
                      SizedBox(
                        height: 10.h,
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () {},
                              child: Container(
                                width: 27.w,
                                height: 27.w,
                                decoration: new BoxDecoration(
                                    color: Color(0xffd3d3d3),
                                    shape: BoxShape.circle),
                                child: Icon(Icons.add),
                              ),
                            ),
                            Text("${presupuesto.status}",
                                style: TextStyle(
                                  color: Color(0xff9f9c55),
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w300,
                                  fontStyle: FontStyle.normal,
                                ))
                          ])
                    ]),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class DashedLineVerticalPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    double dashHeight = 5, dashSpace = 8, startY = 0;
    final paint = Paint(), Color = Colors.grey[300], Widget = 1;
    while (startY < size.height) {
      canvas.drawLine(Offset(0, startY), Offset(0, startY + dashHeight), paint);
      startY += dashHeight + dashSpace;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

Future<dynamic> ShowDialogBox(BuildContext context, String TitleOfDialogBox,
    String Description, bool haveTwoButtosns, bool haveOneButtons,
    {onCalificar}) {
  return showDialog(
      context: context,
      builder: (context) {
        /// DIALOG BOX
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.r),
          ),
          child: IntrinsicHeight(
            child: Container(
              width: 375.w,
              decoration: new BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Color(0xffffffff),
                boxShadow: [
                  BoxShadow(
                      color: Color(0x7c000000),
                      offset: Offset(5, -3),
                      blurRadius: 6,
                      spreadRadius: 0)
                ],
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 20.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            icon: Icon(
                              Icons.close_outlined,
                              size: 30.w,
                            )),
                      ],
                    ),
                    SvgPicture.asset('assets/images/agendaDialoge.svg'),
                    SizedBox(
                      height: 30.h,
                    ),
                    Text(
                      TitleOfDialogBox,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Color(0xff222b45),
                        fontSize: 17.sp,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 48.0.w),
                      child: Text(Description,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0xff414040),
                            fontSize: 15.sp,
                            fontWeight: FontWeight.w300,
                            fontStyle: FontStyle.normal,
                          )),
                    ),
                    SizedBox(
                      height: 27.h,
                    ),
                    haveTwoButtosns
                        ? Twobuttons(context, onCalificar)
                        : SizedBox(
                            height: 15.h,
                          ),
                    haveOneButtons
                        ? Onebuttons(context)
                        : SizedBox(
                            height: 15.h,
                          ),
                  ],
                ),
              ),
            ),
          ),
        );
      });
}

Row Twobuttons(BuildContext context, onCalificar) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        width: 117.w,
        height: 35.w,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Color(0xffF5FAFC),
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(38.0.r),
          color: const Color(0xff009ade),
          boxShadow: const [
            BoxShadow(
              color: Color(0x29000000),
              offset: Offset(5, 3),
              blurRadius: 6,
              spreadRadius: 0,
            ),
          ],
        ),
        child: TextButton(
          //THIS WILL REDIRECT TO NEXT PAGE
          onPressed: () {
            onCalificar();
          },
          child: Text(
            "CALIFICAR",
            style: TextStyle(
              color: Color(0xffffffff),
              fontSize: 12.sp,
              fontWeight: FontWeight.w800,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
      SizedBox(
        width: 20.w,
      ),
      Container(
        width: 117.w,
        height: 35.w,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Color(0xffF5FAFC),
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(38.0.r),
          color: Color(0xffffca51),
          boxShadow: const [
            BoxShadow(
              color: Color(0x29000000),
              offset: Offset(5, 3),
              blurRadius: 6,
              spreadRadius: 0,
            ),
          ],
        ),
        child: TextButton(
          //THIS WILL REDIRECT TO NEXT PAGE
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          },
          child: Text(
            "INICIO",
            style: TextStyle(
              color: Color(0xffffffff),
              fontSize: 12.sp,
              fontWeight: FontWeight.w800,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
    ],
  );
}

Row Onebuttons(context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        width: 212.w,
        height: 49.w,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Color(0xffF5FAFC),
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(38.0.r),
          color: const Color(0xff009ADE),
          boxShadow: const [
            BoxShadow(
              color: Color(0x29000000),
              offset: Offset(5, 3),
              blurRadius: 6,
              spreadRadius: 0,
            ),
          ],
        ),
        child: TextButton(
          //THIS WILL REDIRECT TO NEXT PAGE
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          },
          child: Text(
            "Buscar",
            style: TextStyle(
              color: Color(0xffffffff),
              fontSize: 16.sp,
              fontWeight: FontWeight.w800,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
    ],
  );
}
