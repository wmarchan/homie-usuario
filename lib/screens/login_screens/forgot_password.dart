import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:homie/screens/login_screens/registration_screen.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

bool autoValidate = true;
bool readOnly = false;
bool showSegmentedControl = true;
final _formKey = GlobalKey<FormBuilderState>();

class _ForgotPasswordState extends State<ForgotPassword> {
  final String assetName = 'assets/images/login_screen.svg';

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Transform.translate(
              offset: const Offset(0.0, -100),
              child: SvgPicture.asset(
                assetName,
                height: 360.w,
                width: 400.w,
                fit: BoxFit.cover,
              ),
            ),
            Positioned(
              top: 55.h,
              left: 28.w,
              child: Text(
                "RECUPERAR CONTRASEÑA",
                style: TextStyle(
                  color: Color(0xffffffff),
                  fontSize: 23.sp,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ),
            Positioned(
              top: 92.5.h,
              left: 28.w,
              child: Text(
                " Si olvidó sus datos de ingreso, por favor déjenos \nsu email y le enviaremos la información para su \nrecuperación.",
                style: TextStyle(
                  color: Color(0xffffffff),
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ),
            FormBuilder(
              key: _formKey,
              autovalidateMode: AutovalidateMode.always,
              child: Column(
                children: [
                  SizedBox(
                    height: 280.h,
                  ),
                  Column(
                    children: [
                      //Email Field
                      Center(
                        child: SizedBox(
                          width: 250.w,
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 150),
                                child: Text(
                                  "Email",
                                  style: TextStyle(
                                    color: const Color(0xff414040),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                              FormBuilderTextField(
                                name: 'email',
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.email(context,
                                      errorText: 'Enter Valid Email!')
                                ]),
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(38.r),
                                  ),
                                  hintText: 'prueba@gmail.com',
                                  contentPadding: EdgeInsets.only(
                                    left: 18.w,
                                    top: 7.h,
                                    bottom: 7.h,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 280.h),
                      CTAbutton(context),
                      SizedBox(height: 80.0.h),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ///FIRST LINK BUTTON
                              InkWell(
                                onTap: () {
                                  //This one will naivgate to the Registration Screen
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const RegistrationScreen()),
                                  );
                                },
                                child: RichText(
                                    text: TextSpan(children: [
                                  TextSpan(
                                    text: "No tienes cuenta?, Crea una ",
                                    style: TextStyle(
                                      color: const Color(0xff323643),
                                      fontSize: 10.sp,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                  TextSpan(
                                      text: "AQUÍ",
                                      style: TextStyle(
                                        color: const Color(0xff323643),
                                        fontSize: 10.sp,
                                        decoration: TextDecoration.underline,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      )),
                                ])),
                              )
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

// ignore: non_constant_identifier_names
Container CTAbutton(BuildContext context) {
  return Container(
    width: 249.00497436523438.w,
    height: 39.642578125.h,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(38.0.r),
      color: const Color(0xff21409a),
      boxShadow: const [
        BoxShadow(
          color: Color(0x29000000),
          offset: Offset(5, 3),
          blurRadius: 6,
          spreadRadius: 0,
        ),
      ],
    ),
    child: TextButton(
      onPressed: () {
        _formKey.currentState?.save();
        if (_formKey.currentState!.validate()) {
          Navigator.popAndPushNamed(context, '/loginScreen');
        } else {
          print("validation failed");
        }

        //Navigate back to login screen
      },
      child: Text(
        "Recuperar",
        style: TextStyle(
          color: const Color(0xffffffff),
          fontSize: 15.sp,
          fontWeight: FontWeight.w800,
          fontStyle: FontStyle.normal,
        ),
      ),
    ),
  );
}
