import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:homie/client.dart';
import 'package:homie/screens/inicio/home.dart';
import 'package:homie/screens/login_screens/forgot_password.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:homie/screens/login_screens/login.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

bool autoValidate = true;
bool readOnly = false;
bool showSegmentedControl = true;
final _formKey = GlobalKey<FormBuilderState>();

class _RegistrationScreenState extends State<RegistrationScreen> {
  final String assetName = 'assets/images/login_screen.svg';
  LoginScreen button = const LoginScreen();
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: Stack(
        children: [
          Transform.translate(
            offset: const Offset(0.0, -100),
            child: SvgPicture.asset(
              assetName,
              height: 360.w,
              width: 400.w,
              fit: BoxFit.cover,
            ),
          ),
          Positioned(
            top: 55.h,
            left: 28.w,
            child: Text(
              "REGISTRO",
              style: TextStyle(
                color: const Color(0xffffffff),
                fontSize: 23.sp,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
            ),
          ),
          Positioned(
            top: 92.5.h,
            left: 28.w,
            child: Text(
              "Ingrese sus datos y cree su cuenta para poder \ncomenzar a disfrutar de los servicios de SERVIHOME",
              style: TextStyle(
                color: const Color(0xffffffff),
                fontSize: 12.sp,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
            ),
          ),
          FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.always,
            child: Column(
              children: [
                SizedBox(
                  height: 280.h,
                ),
                Column(
                  children: [
                    //Email Field
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 60.w),
                      child: SizedBox(
                        width: 255.w,
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(right: 180.w),
                              child: Text(
                                "Email",
                                style: TextStyle(
                                  color: const Color(0xff414040),
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ),
                            FormBuilderTextField(
                              name: 'email',
                              validator: FormBuilderValidators.compose([
                                FormBuilderValidators.email(
                                  context,
                                ),
                                FormBuilderValidators.required(
                                  context,
                                )
                              ]),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(38.r),
                                ),
                                hintText: 'example@gmail.com',
                                contentPadding: EdgeInsets.only(
                                  left: 18.w,
                                  top: 7.h,
                                  bottom: 7.h,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 21.7.h),

                    //First and Last Name fields
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Column(
                          children: [
                            SizedBox(
                              width: 120.w,
                              child: Column(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(right: 20.w),
                                    child: Text(
                                      "Nombre",
                                      style: TextStyle(
                                        color: const Color(0xff414040),
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                  ),
                                  FormBuilderTextField(
                                    name: 'firstname',
                                    validator: FormBuilderValidators.compose([
                                      FormBuilderValidators.required(
                                        context,
                                      )
                                    ]),
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(38.r),
                                      ),
                                      hintText: 'Christian',
                                      contentPadding: EdgeInsets.only(
                                        left: 25.w,
                                        top: 7.h,
                                        bottom: 7.h,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(width: 15.0.w),
                        Column(
                          children: [
                            SizedBox(
                              width: 120.w,
                              child: Column(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(right: 20.w),
                                    child: Text(
                                      "Apellido",
                                      style: TextStyle(
                                        color: const Color(0xff414040),
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                  ),
                                  FormBuilderTextField(
                                    name: 'lastname',
                                    validator: FormBuilderValidators.compose([
                                      FormBuilderValidators.required(
                                        context,
                                      )
                                    ]),
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(38.r),
                                      ),
                                      hintText: 'Rodriguez',
                                      contentPadding: EdgeInsets.only(
                                        left: 25.w,
                                        top: 7.h,
                                        bottom: 7.h,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 21.h),
                    Padding(
                      padding: EdgeInsets.only(left: 20.w, right: 190.w),
                      child: Text(
                        "Contraseña",
                        style: TextStyle(
                          color: const Color(0xff414040),
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 60.0.w),
                      child: FormBuilderTextField(
                        name: 'password',
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(
                            context,
                          )
                        ]),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.r),
                          ),
                          hintText: '********',
                          contentPadding: EdgeInsets.only(
                              left: 18.w, top: 8.h, bottom: 8.h),
                        ),
                        obscureText: true,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20.0.h),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 40.0.w),
                  child: Center(
                    child: FormBuilderCheckbox(
                        name: "terminos",
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(
                            context,
                          )
                        ]),
                        title: Text(
                          "Acepto términos y condiciones",
                          style: TextStyle(
                            color: const Color(0xff414040),
                            fontSize: 16.sp,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                          ),
                        )),
                  ),
                ),
                SizedBox(height: 20.0.h),
                CTAbutton(context),
                SizedBox(height: 42.0.h),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ///LAST LINK BUTTON
                        InkWell(
                          onTap: () {
                            //This one will naivgate to the Forgot Password Screen
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const ForgotPassword()),
                            );
                          },
                          child: RichText(
                              text: TextSpan(children: [
                            TextSpan(
                                text: "Olvidaste tu contraseña?, clic ",
                                style: TextStyle(
                                  color: const Color(0xff323643),
                                  fontSize: 10.sp,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                )),
                            TextSpan(
                                text: "AQUÍ",
                                style: TextStyle(
                                  color: const Color(0xff323643),
                                  fontSize: 10.sp,
                                  fontWeight: FontWeight.w400,
                                  decoration: TextDecoration.underline,
                                  fontStyle: FontStyle.normal,
                                )),
                          ])),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      )),
    );
  }
}

// ignore: non_constant_identifier_names
Container CTAbutton(BuildContext context) {
  return Container(
    width: 249.00497436523438.w,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(38.0.r),
      color: const Color(0xff21409a),
      boxShadow: const [
        BoxShadow(
          color: Color(0x29000000),
          offset: Offset(5, 3),
          blurRadius: 6,
          spreadRadius: 0,
        ),
      ],
    ),
    child: TextButton(
      onPressed: () async {
        _formKey.currentState?.save();
        if (_formKey.currentState!.validate()) {
          var fields = _formKey.currentState!.fields;
          await Client().registrar({
            "email": fields["email"]?.value,
            "contraseña": fields["password"]?.value,
            "nombre": fields["firstname"]?.value,
            "apellido": fields["lastname"]?.value,
          });
          await Client()
              .login(fields["email"]!.value, fields["password"]!.value);
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => HomeScreen()),
            (route) => false,
          );
        } else {
          print("validation failed");
        }

        //Navigate back to login screen
      },
      child: Text(
        "REGISTRARSE",
        style: TextStyle(
          color: const Color(0xffffffff),
          fontSize: 15.sp,
          fontWeight: FontWeight.w800,
          fontStyle: FontStyle.normal,
        ),
      ),
    ),
  );
}
