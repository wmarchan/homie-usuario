// ignore_for_file: prefer_const_constructors

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:homie/client.dart';
import 'package:homie/screens/inicio/home.dart';
import 'package:homie/screens/login_screens/forgot_password.dart';
import 'package:homie/screens/login_screens/registration_screen.dart';
import 'package:sign_button/sign_button.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  //Using SVG Images
  final String assetName = 'assets/images/login_screen.svg';

  bool loading = false;
  final _formKey = GlobalKey<FormBuilderState>();

  void loginGoogle(context) async {
    setState(() {
      loading = true;
    });
    var appData = await Client().loginWithGoogle();
    if (appData != null) {
      Navigator.of(context).pushNamedAndRemoveUntil("/home", (route) => false);
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  void loginFacebook(context) async {
    setState(() {
      loading = true;
    });
    var appData = await Client().loginWithFacebook();
    if (appData != null) {
      Navigator.of(context).pushNamedAndRemoveUntil("/home", (route) => false);
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  void loginApple(context) async {
    setState(() {
      loading = true;
    });
    var appData = await Client().loginWithApple();
    if (appData != null) {
      Navigator.of(context).pushNamedAndRemoveUntil("/home", (route) => false);
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Stack(
            children: [
              Transform.translate(
                offset: const Offset(0.0, -100),
                child: SvgPicture.asset(
                  assetName,
                  height: 360.w,
                  width: 400.w,
                  fit: BoxFit.cover,
                ),
              ),
              Positioned(
                top: 55.h,
                left: 28.w,
                child: Text(
                  "BIENVENIDO",
                  style: TextStyle(
                    color: Color(0xffffffff),
                    fontSize: 23.sp,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              Positioned(
                top: 92.5.h,
                left: 28.w,
                child: Text(
                  "Por favor ingrese su información y acceda a la\naplicación.",
                  style: TextStyle(
                    color: Color(0xffffffff),
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              FormBuilder(
                key: _formKey,
                child: Column(
                  children: [
                    SizedBox(
                      height: 280.h,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 42.0.w),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 15.w, right: 230.w),
                            child: Text(
                              "Email",
                              style: TextStyle(
                                color: Color(0xff414040),
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          ),
                          FormBuilderTextField(
                            name: 'email',
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.email(
                                context,
                              ),
                              FormBuilderValidators.required(
                                context,
                              )
                            ]),
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(38.r),
                                ),
                                hintText: 'example@gmail.com',
                                contentPadding: EdgeInsets.only(
                                  left: 18.w,
                                  top: 7.h,
                                  bottom: 7.h,
                                )),
                          ),
                          SizedBox(height: 21.7.h),
                          Padding(
                            padding: EdgeInsets.only(left: 8.w, right: 190.w),
                            child: Text(
                              "Contraseña",
                              style: TextStyle(
                                color: Color(0xff414040),
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                              ),
                            ),
                          ),
                          FormBuilderTextField(
                            name: 'password',
                            validator: FormBuilderValidators.compose([
                              FormBuilderValidators.required(
                                context,
                              )
                            ]),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30.r),
                              ),
                              hintText: '********',
                              contentPadding: EdgeInsets.only(
                                  left: 18.w, top: 8.h, bottom: 8.h),
                            ),
                            obscureText: true,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 65.h,
                    ),
                    CTAbutton(),
                    SizedBox(height: 17.1.h),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 70.w),
                            child: Divider(
                              height: 0.25.h,
                              thickness: 0.65,

                              color: Color(0xffe0e0e0),
                              //color: Color(0xffe0e0e0),
                            ),
                          ),
                        ),
                        Container(
                            width: 30.136260986328125.w,
                            height: 30.13623046875.h,
                            decoration: BoxDecoration(
                              color: Color(0x00e0e0e0),
                            ),
                            child: Center(child: Text("0"))),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(right: 70.w),
                            child: Divider(
                              height: 0.25.h,
                              thickness: 0.65,

                              color: Color(0xffe0e0e0),
                              //color: Color(0xffe0e0e0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 14.0.h),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SignInButton.mini(
                            buttonType: ButtonType.facebook,
                            onPressed: () {
                              loginFacebook(context);
                            },
                          ),
                          SignInButton.mini(
                            buttonType: ButtonType.google,
                            onPressed: () {
                              loginGoogle(context);
                            },
                          ),
                          SignInButton.mini(
                            buttonType: ButtonType.apple,
                            onPressed: () {
                              loginApple(context);
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 42.0.h),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ///FIRST LINK BUTTON
                            InkWell(
                              onTap: () {
                                //This one will naivgate to the Registration Screen
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          RegistrationScreen()),
                                );
                              },
                              child: RichText(
                                  text: TextSpan(children: [
                                TextSpan(
                                    text: "No tienes cuenta?, Crea una ",
                                    style: TextStyle(
                                      color: Color(0xff323643),
                                      fontSize: 10.sp,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )),
                                TextSpan(
                                    text: "AQUÍ",
                                    style: TextStyle(
                                      color: Color(0xff323643),
                                      fontSize: 10.sp,
                                      decoration: TextDecoration.underline,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )),
                              ])),
                            )
                          ],
                        ),
                        SizedBox(height: 12.h),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ///LAST LINK BUTTON
                            InkWell(
                              onTap: () {
                                //This one will naivgate to the Forgot Password Screen
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const ForgotPassword()),
                                );
                              },
                              child: RichText(
                                  text: TextSpan(children: [
                                TextSpan(
                                    text: "Olvidaste tu contraseña?, clic ",
                                    style: TextStyle(
                                      color: Color(0xff323643),
                                      fontSize: 10.sp,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )),
                                TextSpan(
                                    text: "AQUÍ",
                                    style: TextStyle(
                                      color: Color(0xff323643),
                                      fontSize: 10.sp,
                                      fontWeight: FontWeight.w400,
                                      decoration: TextDecoration.underline,
                                      fontStyle: FontStyle.normal,
                                    )),
                              ])),
                            )
                          ],
                        ),
                      ],
                    ),
                    //SizedBox(height: 8.0),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      //const SizedBox(height: 10.0),
    );
  }

  Container CTAbutton() {
    return Container(
      width: 249.00497436523438.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(38.0.r),
        color: const Color(0xff21409a),
        boxShadow: const [
          BoxShadow(
            color: Color(0x29000000),
            offset: Offset(5, 3),
            blurRadius: 6,
            spreadRadius: 0,
          ),
        ],
      ),
      child: TextButton(
        onPressed: loading
            ? null
            : () async {
                _formKey.currentState?.save();
                if (_formKey.currentState!.validate()) {
                  var fields = _formKey.currentState!.fields;
                  try {
                    setState(() {
                      loading = true;
                    });
                    await Client().login(
                        fields["email"]!.value, fields["password"]!.value);
                    Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => HomeScreen()),
                      (route) => false,
                    );
                  } catch (e) {
                    setState(() {
                      loading = false;
                    });
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text("Error al iniciar sesión")));
                  }
                } else {
                  print("validation failed");
                }
                // Navigate to home screen
              },
        child: loading
            ? Center(child: CircularProgressIndicator())
            : Text(
                "INGRESAR",
                style: TextStyle(
                  color: Color(0xffffffff),
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w800,
                  fontStyle: FontStyle.normal,
                ),
              ),
      ),
    );
  }
}
