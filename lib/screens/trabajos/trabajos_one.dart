// ignore_for_file: non_constant_identifier_names

import 'package:flutter_rating_stars/flutter_rating_stars.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:homie/screens/trabajos/trabajos.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class TrabajosOne extends StatefulWidget {
  const TrabajosOne({Key? key}) : super(key: key);

  @override
  _BuqsuedaThreeState createState() => _BuqsuedaThreeState();
}

class _BuqsuedaThreeState extends State<TrabajosOne> {
  var profileName = 'Robbin Hooda';
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  //Rating Stars Value
  var value = 3.5;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xff83dcff), Color(0xffeffdff)],
            stops: [0, 1],
            begin: Alignment(-1.00, -0.02),
            end: Alignment(1.00, 0.02),
            // angle: 91,
            // scale: undefined,
          ),
        ),
        child: Scaffold(
          key: _globalKey,
          endDrawer: Navbar(),
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: const Icon(Icons.arrow_back, color: Colors.black),
              //NAVIGATING BACK TO THE HOMEPAGE
              onPressed: () => Navigator.of(context).pop(),
            ),
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 20.0.w),
                child: GestureDetector(
                  //THIS FUNCTION WILL OPEN MENU DRAWER
                  onTap: () {
                    _globalKey.currentState?.openEndDrawer();
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 58.41377258300781.w,
                        height: 57.504638671875.w,
                        decoration: const BoxDecoration(
                          color: Color(0xff009ade),
                          shape: BoxShape.circle,
                        ),
                      ),
                      SvgPicture.asset('assets/images/menu-icon.svg'),
                    ],
                  ),
                ),
              ),
            ],
            toolbarHeight: 80.h,
          ),

          backgroundColor: Colors.transparent,
          body: SafeArea(
            bottom: false,
            child: Column(
              children: [
                Column(
                  children: [
                    Text("Reporte",
                        style: TextStyle(
                          color: Color(0xff00013c),
                          fontSize: 23.sp,
                          fontWeight: FontWeight.w900,
                          fontStyle: FontStyle.normal,
                        )),
                    SizedBox(height: 15.0.h),
                    Center(
                        child: Text("Orden #5436",
                            style: TextStyle(
                              color: Color(0xff252525),
                              fontSize: 25.sp,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                              letterSpacing: -0.75,
                            ))),
                  ],
                ),
                SizedBox(
                  height: 20.h,
                ),
                Expanded(
                  child: Container(
                    width: 375.w,
                    decoration: const BoxDecoration(
                      color: Color(0xffffffff),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25)),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0x7c000000),
                            offset: Offset(5, -3),
                            blurRadius: 6,
                            spreadRadius: 0)
                      ],
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 12.0.w,
                        vertical: 22.6.h,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 0,
                            child: Container(
                              width: 340.w,
                              decoration: new BoxDecoration(
                                  color: Color(0xffE5F5FC),
                                  borderRadius: BorderRadius.circular(30.r)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 23.0.w, vertical: 20.h),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Información",
                                        style: TextStyle(
                                          color: Color(0xff222b45),
                                          fontSize: 17.sp,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    SizedBox(
                                      height: 8.w,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text("Fecha Viita: 13 septiembre 2021",
                                            style: TextStyle(
                                              color: Color(0xff8f9bb3),
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                        SizedBox(
                                          height: 4.w,
                                        ),
                                        Text("Hora Visita: 10:30 am",
                                            style: TextStyle(
                                              color: Color(0xff8f9bb3),
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),

                          // Container 02
                          SizedBox(
                            height: 20.h,
                          ),
                          Expanded(
                            flex: 0,
                            child: Container(
                              width: 340.w,
                              decoration: new BoxDecoration(
                                  color: Color(0xffE5F5FC),
                                  borderRadius: BorderRadius.circular(30.r)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 23.0.w, vertical: 20.h),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Reporte",
                                        style: TextStyle(
                                          color: Color(0xff222b45),
                                          fontSize: 17.sp,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    SizedBox(
                                      height: 8.w,
                                    ),
                                    Text(
                                        "Se realizó una visita de diagnóstico, donde se analizó el problema reportado, encontrandose una falla en el sistema eléctrico para lo cual se enviará presupuesto.",
                                        style: TextStyle(
                                          color: Color(0xff8f9bb3),
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          // Container 02 is ended here
                          SizedBox(
                            height: 20.h,
                          ),
                          Expanded(
                            flex: 0,
                            child: Container(
                              width: 340.w,
                              decoration: new BoxDecoration(
                                  color: Color(0xffE5F5FC),
                                  borderRadius: BorderRadius.circular(30.r)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 23.0.w, vertical: 20.h),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Tiempo estimado piezas y partes",
                                        style: TextStyle(
                                          color: Color(0xff222b45),
                                          fontSize: 17.sp,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    SizedBox(
                                      height: 8.w,
                                    ),
                                    Text(
                                        "Las piezas y partes se estima tardarán 10 días.",
                                        style: TextStyle(
                                          color: Color(0xff8f9bb3),
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20.h,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              /// BUTTON CALCIFICAR aka Accepted
                              ///
                              GestureDetector(
                                onTap: () => {},
                                child: Container(
                                  width: 120.w,
                                  height: 40.h,
                                  decoration: BoxDecoration(
                                    color: const Color(0xff009ade),
                                    borderRadius: BorderRadius.circular(80),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Color(0x29000000),
                                          offset: Offset(5, 3),
                                          blurRadius: 6,
                                          spreadRadius: 0)
                                    ],
                                  ),
                                  child: Center(
                                    child: Text("CALIFICAR",
                                        style: TextStyle(
                                          color: const Color(0xffffffff),
                                          fontSize: 10.sp,
                                          fontWeight: FontWeight.w800,
                                          fontStyle: FontStyle.normal,
                                        )),
                                  ),
                                ),
                              ),

                              SizedBox(
                                width: 20.w,
                              ),

                              // BUTTON SOLICITER PRESUPUESTO aka Quote Requested

                              GestureDetector(
                                onTap: () {
                                  ShowDialogBox(
                                      (context),
                                      'COTIZACIÓN SOLICITADA',
                                      'Gracias…su solicitud fue\n enviada y prototípico le \nenviaremos su cotización. \nPor favor califique el servicio\n recibido',
                                      false,
                                      true);
                                },
                                child: Container(
                                  width: 120.w,
                                  height: 40.h,
                                  decoration: BoxDecoration(
                                    color: const Color(0xffffca51),
                                    borderRadius: BorderRadius.circular(80),
                                    boxShadow: const [
                                      BoxShadow(
                                        color: Color(0x29000000),
                                        offset: Offset(5, 3),
                                        blurRadius: 6,
                                        spreadRadius: 0,
                                      ),
                                    ],
                                  ),
                                  child: Center(
                                    child: Text(
                                      "SOLICITAR PRESUPUESTO",
                                      style: TextStyle(
                                        color: const Color(0xffffffff),
                                        fontSize: 10.sp,
                                        fontWeight: FontWeight.w800,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

          //BOTTOM BAR WITH FLOATING BTN
          floatingActionButton: WhatsappButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: const BottomNav(),
        ));
  }

  SizedBox ReviewTile({
    String reviewerName = '',
    String reviewAbout = '',
    String review = '',
    String date = '',
    required double rating,

    //Image reviewerProfilePic = 'assets/images/'
  }) {
    return SizedBox(
      height: 150.h,
      width: 320.w,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 14.3.w,
          vertical: 5.0.h,
        ),
        child: Column(
          children: [
            //Row 1 Contains Profile Pic, Name, Description and Rating
            Row(
              children: [
                //This Column Contains the Image Container
                Column(
                  children: [
                    Container(
                      width: 61.w,
                      height: 70.h,
                      decoration: BoxDecoration(
                        color: const Color(0xffc5cee0),
                        borderRadius: BorderRadius.circular(
                          8,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(width: 12.0.w),
                //This column contains all text
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      reviewerName,
                      style: TextStyle(
                        color: const Color(0xff222b45),
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(height: 3.3.h),
                    Text(
                      reviewAbout,
                      style: TextStyle(
                        color: const Color(0xff8f9bb3),
                        fontSize: 13.sp,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(height: 4.0.h),

                    //Rating Stars
                    RatingStars(
                      value: rating,
                      onValueChanged: (rating) {
                        //
                        setState(() {
                          value = rating;
                        });
                      },
                      starBuilder: (index, color) => Icon(
                        Icons.star,
                        color: color,
                      ),
                      starCount: 5,
                      starSize: 15.sp,
                      maxValue: 5,
                      starSpacing: 5,
                      valueLabelVisibility: false,
                      animationDuration: const Duration(milliseconds: 1000),
                      starOffColor: const Color(0xffe7e8ea),
                      starColor: Colors.yellow.shade600,
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: 6.7.h),
            //Row 2 Contains the review
            Row(
              children: [
                Text(
                  review,
                  style: TextStyle(
                    color: const Color(0xff222b45),
                    fontSize: 15.sp,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                  softWrap: true,
                ),
              ],
            ),
            SizedBox(height: 6.3.h),

            //Row 3 Contains the date
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  date,
                  style: TextStyle(
                    color: const Color(0xff8f9bb3),
                    fontSize: 13.sp,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Container ProfileTile({
    String title = '',
    String description = '',
  }) {
    return Container(
      width: 340.w,
      height: 122.h,
      decoration: BoxDecoration(
        color: const Color(0xff009ade).withOpacity(
          0.10000000149011612,
        ),
        borderRadius: BorderRadius.circular(30),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 18.0.h, horizontal: 23.1.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(
                color: const Color(0xff222b45),
                fontSize: 18.sp,
                fontWeight: FontWeight.w800,
                fontStyle: FontStyle.normal,
              ),
            ),
            SizedBox(height: 7.0.h),
            Text(
              description,
              style: TextStyle(
                color: const Color(0xff8f9bb3),
                fontSize: 12.sp,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Future<dynamic> ShowDialogBox(
  BuildContext context,
  String TitleOfDialogBox,
  String Description,
  bool haveTwoButtosns,
  bool haveOneButtons,
) {
  return showDialog(
      context: context,
      builder: (context) {
        /// DIALOG BOX
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.r),
          ),
          child: Container(
            width: 375.w,
            height: 550.h,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: const Color(0xffffffff),
              boxShadow: const [
                BoxShadow(
                    color: Color(0x7c000000),
                    offset: Offset(5, -3),
                    blurRadius: 6,
                    spreadRadius: 0)
              ],
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 10.h),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.close_outlined,
                            size: 30.w,
                          )),
                    ],
                  ),
                  SvgPicture.asset('assets/images/agendaDialoge.svg'),
                  SizedBox(
                    height: 30.h,
                  ),
                  Text(
                    TitleOfDialogBox,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xff222b45),
                      fontSize: 17.sp,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 48.0.w),
                    child: Text(Description,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xff414040),
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w300,
                          fontStyle: FontStyle.normal,
                        )),
                  ),
                  SizedBox(
                    height: 27.h,
                  ),
                  Onebuttons(context)
                ],
              ),
            ),
          ),
        );
      });
}

Row Onebuttons(
  BuildContext context,
) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        width: 117.w,
        height: 35.w,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Color(0xffF5FAFC),
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(38.0.r),
          color: const Color(0xff009ade),
          boxShadow: const [
            BoxShadow(
              color: Color(0x29000000),
              offset: Offset(5, 3),
              blurRadius: 6,
              spreadRadius: 0,
            ),
          ],
        ),
        child: TextButton(
          //THIS WILL REDIRECT TO NEXT PAGE
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => const TrabajosMainScreen()),
            );
          },

          child: Text(
            "CALIFICAR",
            style: TextStyle(
              color: Color(0xffffffff),
              fontSize: 12.sp,
              fontWeight: FontWeight.w800,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
    ],
  );
}
