import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:homie/client.dart';
import 'package:homie/models/presupuesto/presupuesto.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/presupuesto/presupuesto_detalle_pagado.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:homie/screens/trabajos/trabajos_one.dart';
import 'package:intl/intl.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class TrabajosMainScreen extends StatefulWidget {
  const TrabajosMainScreen({Key? key}) : super(key: key);

  @override
  _TrabajosMainScreenState createState() => _TrabajosMainScreenState();
}

class _TrabajosMainScreenState extends State<TrabajosMainScreen> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  bool isAceptadosSelected = false;
  bool isPoraceptarSelected = true;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );

    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
        colors: const [Color(0xff83dcff), Color(0xffeffdff)],
        stops: const [0, 1],
        begin: Alignment(-1.00, -0.02),
        end: Alignment(1.00, 0.02),
      )),
      child: SafeArea(
        child: Scaffold(
          key: _globalKey,
          endDrawer: Navbar(),
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              //NAVIGATING BACK TO THE HOMEPAGE
              onPressed: () => Navigator.of(context).pop(),
            ),
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 20.0.w),
                child: GestureDetector(
                  //THIS FUNCTION WILL OPEN MENU DRAWER
                  onTap: () {
                    _globalKey.currentState?.openEndDrawer();
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 58.41377258300781.w,
                        height: 57.504638671875.w,
                        decoration: new BoxDecoration(
                          color: Color(0xff009ade),
                          shape: BoxShape.circle,
                        ),
                      ),
                      SvgPicture.asset('assets/images/menu-icon.svg'),
                    ],
                  ),
                ),
              ),
            ],
            toolbarHeight: 120.h,
          ),
          body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Padding(
              padding: EdgeInsets.only(left: 16.0.w),
              child: Text('Trabajos Terminados',
                  style: TextStyle(
                    color: Color(0xff00013c),
                    fontSize: 23.sp,
                    fontWeight: FontWeight.w900,
                    fontStyle: FontStyle.normal,
                  )),
            ),
            SizedBox(
              height: 24.h,
            ),
            Expanded(
              child: Container(
                width: 375.w,
                decoration: BoxDecoration(
                  color: Color(0xffffffff),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25)),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0x7c000000),
                        offset: Offset(5, -3),
                        blurRadius: 6,
                        spreadRadius: 0)
                  ],
                ),
                child: Column(children: [
                  SizedBox(
                    height: 5.h,
                  ),
                  twoOptTabs(),
                  SizedBox(
                    height: 5.h,
                  ),
                  // THIS LISTVIEW WHEN Recibidos IS SELECTED
                  Expanded(
                    child: FutureBuilder(
                        future: Client().getPresupuestos(isPoraceptarSelected
                            ? ["Finalizado"]
                            : ["Completado"]),
                        builder: (context,
                            AsyncSnapshot<List<Presupuesto>> snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          return ListView.builder(
                            itemCount: snapshot.data!.length,
                            itemBuilder: (context, index) {
                              return Container(
                                  margin: EdgeInsets.only(bottom: 10.w),
                                  child:
                                      PresupuestoCard(snapshot.data![index]));
                            },
                            padding:
                                EdgeInsets.only(left: 23.0.w, right: 23.0.w),
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                          );
                        }),
                  )
                ]),
              ),
            ),
          ]),
          //BOTTOM BAR WITH FLOATING BTN
          floatingActionButton: WhatsappButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: const BottomNav(),
        ),
      ),
    );
  }

  Padding twoOptTabs() {
    return Padding(
      padding:
          EdgeInsets.only(top: 18.h, left: 29.w, right: 29.w, bottom: 34.w),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
        GestureDetector(
          onTap: () {
            setState(() {
              //CHANGING STATE AFTER SELECTION
              if (isPoraceptarSelected == true) {
                isPoraceptarSelected = false;
                isPoraceptarSelected = true;
              } else {
                isPoraceptarSelected = true;
                isAceptadosSelected = false;

                Opacity(
                  opacity: 0.0,
                );
              }
            });
          },
          child: Container(
            width: 153,
            height: 33.587890625.h,
            decoration: new BoxDecoration(
              color:
                  isPoraceptarSelected ? Color(0xff009ade) : Color(0xffeeeeee),
              boxShadow: [
                BoxShadow(
                    color: Color(0x29000000),
                    offset: Offset(5, 3),
                    blurRadius: 6,
                    spreadRadius: 0)
              ],
              borderRadius: BorderRadius.circular(6.r),
            ),
            child: Center(
              child: Text("Por aceptar",
                  style: TextStyle(
                    color: isPoraceptarSelected
                        ? Color(0xfffdfdfd)
                        : Color(0xff009ade),
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  )),
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              //CHANGING STATE AFTER SELECTION

              if (isAceptadosSelected == true) {
                isAceptadosSelected = false;
                isPoraceptarSelected = true;
              } else {
                isPoraceptarSelected = false;
                isAceptadosSelected = true;

                Opacity(
                  opacity: 0.0,
                );
              }
            });
          },
          child: Container(
            width: 153.w,
            height: 33.587890625.h,
            decoration: new BoxDecoration(
              color:
                  isAceptadosSelected ? Color(0xff009ade) : Color(0xffeeeeee),
              boxShadow: [
                BoxShadow(
                    color: Color(0x29000000),
                    offset: Offset(5, 3),
                    blurRadius: 6,
                    spreadRadius: 0)
              ],
              borderRadius: BorderRadius.circular(6.r),
            ),
            child: Center(
              child: Text("Aceptados",
                  style: TextStyle(
                    color: isAceptadosSelected
                        ? Color(0xfffdfdfd)
                        : Color(0xff009ade),
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  )),
            ),
          ),
        ),
      ]),
    );
  }

  InkWell PresupuestoCard(Presupuesto presupuesto) {
    String getFormattedDate() {
      return DateFormat("MMMM dd, yyyy")
          .format(DateTime.parse(presupuesto.fecha.toString()));
    }

    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => PresupuestoDetallePagado(),
            settings: RouteSettings(
              arguments: presupuesto,
            ),
          ),
        );
      },
      child: SizedBox(
        height: 148.h,
        width: 345.w,
        child: Row(
          children: [
            Column(
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                        width: 60.w,
                        height: 60.w,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle, color: Color(0xfff7f7f7))),
                    Container(
                      width: 50.w,
                      height: 50.w,
                      decoration: const BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x23646464),
                              offset: Offset(-7, 3),
                              blurRadius: 4,
                              spreadRadius: 0)
                        ],
                        color: Color(0xff009ade),
                        shape: BoxShape.circle,
                      ),
                      child: SvgPicture.asset(
                        'assets/images/test-icon.svg',
                        fit: BoxFit.scaleDown,
                      ),
                    )
                  ],
                ),
                CustomPaint(
                    size: Size(1, 76.h), painter: DashedLineVerticalPainter())
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 12.0),
                    child: Text(getFormattedDate(),
                        style: TextStyle(
                          color: Color(0xff000000),
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w300,
                          fontStyle: FontStyle.normal,
                        )),
                  ),
                  SizedBox(
                    width: 130.w,
                  ),
                  SvgPicture.asset('assets/images/more-icon.svg')
                ]),
                SizedBox(
                  height: 12.h,
                ),
                Container(
                  width: 267.w,
                  height: 100.w,
                  decoration: BoxDecoration(
                      color: Color(0xfffbfbfb),
                      border: Border.all(
                        color: Color(0xffE1E1E1),
                      ),
                      borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(15),
                        bottomLeft: Radius.circular(15),
                        bottomRight: Radius.circular(15),
                      )),
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: 12.0.w, top: 12.0.w, bottom: 12.0.w, right: 22.w),
                    child: Column(children: [
                      Row(children: [
                        Text("${presupuesto.descripcion}",
                            style: TextStyle(
                              color: Color(0xff000000),
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w300,
                              fontStyle: FontStyle.normal,
                            )),
                      ]),
                      SizedBox(height: 8.h),
                      Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset("assets/images/time-icon.svg"),
                            SizedBox(width: 6.w),
                            Text("${presupuesto.hora}",
                                style: TextStyle(
                                  color: Color(0xff858585),
                                  fontSize: 11.sp,
                                  fontWeight: FontWeight.w300,
                                  fontStyle: FontStyle.normal,
                                ))
                          ]),
                      SizedBox(
                        height: 10.h,
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () {},
                              child: Container(
                                width: 27.w,
                                height: 27.w,
                                decoration: new BoxDecoration(
                                    color: Color(0xffd3d3d3),
                                    shape: BoxShape.circle),
                                child: Icon(Icons.add),
                              ),
                            ),
                            Text("${presupuesto.status}",
                                style: TextStyle(
                                  color: Color(0xff9f9c55),
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w300,
                                  fontStyle: FontStyle.normal,
                                ))
                          ])
                    ]),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class DashedLineVerticalPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    double dashHeight = 5, dashSpace = 8, startY = 0;
    final paint = Paint(), Color = Colors.grey[300], Widget = 1;
    while (startY < size.height) {
      canvas.drawLine(Offset(0, startY), Offset(0, startY + dashHeight), paint);
      startY += dashHeight + dashSpace;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
