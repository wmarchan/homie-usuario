// ignore_for_file: prefer_const_literals_to_create_immutables, unnecessary_new, prefer_const_constructors, unnecessary_this

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/navigation/whatsapp_button.dart';
import 'package:homie/screens/busqueda_y_agenda/busqueda.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  late int currenttabindex;
  //first service
  late String firstservicename = 'Línea Blanca';
  late String firstserviceimage = 'assets/images/dashboard-icon-1.svg';
  //second service
  late String secondservicename = 'Electricidad';
  late String secondserviceimage = 'assets/images/dashboard-icon-2.svg';
  //third service
  late String thirdservicename = 'Plomería';
  late String thirdserviceimage = 'assets/images/dashboard-icon-3.svg';
  //fourth service
  late String fourthservicename = 'Aire Acondicionado';
  late String fourthserviceimage = 'assets/images/dashboard-icon-4.svg';

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: new BoxDecoration(
          gradient: LinearGradient(
        colors: [Color(0xff83dcff), Color(0xffeffdff)],
        stops: [0, 1],
        begin: Alignment(-1.00, -0.02),
        end: Alignment(1.00, 0.02),
        // angle: 91,
        // scale: undefined,
      )),
      child: Scaffold(
        key: _globalKey,
        endDrawer: Navbar(),
        backgroundColor: Colors.transparent,
        body: SafeArea(
          bottom: false,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 27.h, right: 27.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    //MENU BUTTON
                    GestureDetector(
                      onTap: () {
                        _globalKey.currentState!.openEndDrawer();
                      },
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            width: 58.41377258300781.w,
                            height: 57.504638671875.w,
                            decoration: new BoxDecoration(
                              color: Color(0xff009ade),
                              shape: BoxShape.circle,
                            ),
                          ),
                          SvgPicture.asset('assets/images/menu-icon.svg'),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 27.h, left: 27.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("Que servicio requiere\nel día de hoy?",
                        style: TextStyle(
                          color: Color(0xff414040),
                          fontSize: 23.sp,
                          fontWeight: FontWeight.w800,
                          fontStyle: FontStyle.normal,
                        ))
                  ],
                ),
              ),
              SizedBox(
                height: 62.h,
              ),
              // Dashboard Container
              Expanded(
                child: Container(
                  width: 375.w,
                  height: 50.h,
                  decoration: new BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0x7c000000),
                          offset: Offset(5, -3),
                          blurRadius: 6,
                          spreadRadius: 0)
                    ],
                  ),
                  child: Padding(
                    padding:
                        EdgeInsets.only(top: 52.h, left: 25.w, right: 15.w),
                    child: Column(
                      //DASHBOARD SERVICES
                      children: [
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    //NAVIGATING TO BUSQUEDONE SCREEN
                                    builder: (context) => BusquedaOne(
                                      nameofservice: this.firstservicename,
                                      imageurl: this.firstserviceimage,
                                      id: 1,
                                    ),
                                  ),
                                );
                              },
                              child: DashboardOpt(15, 45, 15, 45,
                                  firstserviceimage, firstservicename),
                            ),
                            SizedBox(
                              width: 8.w,
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    //NAVIGATING TO BUSQUEDONE SCREEN
                                    builder: (context) => BusquedaOne(
                                      nameofservice: this.secondservicename,
                                      imageurl: this.secondserviceimage,
                                      id: 2,
                                    ),
                                  ),
                                );
                              },
                              child: DashboardOpt(45, 15, 45, 15,
                                  secondserviceimage, secondservicename),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 8.h,
                        ),
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    //NAVIGATING TO BUSQUEDONE SCREEN
                                    builder: (context) => BusquedaOne(
                                      nameofservice: this.thirdservicename,
                                      imageurl: this.thirdserviceimage,
                                      id: 3,
                                    ),
                                  ),
                                );
                              },
                              child: DashboardOpt(15, 45, 15, 45,
                                  thirdserviceimage, thirdservicename),
                            ),
                            SizedBox(
                              width: 8.w,
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    //NAVIGATING TO BUSQUEDONE SCREEN
                                    builder: (context) => BusquedaOne(
                                      nameofservice: this.fourthservicename,
                                      imageurl: this.fourthserviceimage,
                                      id: 4,
                                    ),
                                  ),
                                );
                              },
                              child: DashboardOpt(45, 15, 45, 15,
                                  fourthserviceimage, fourthservicename),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: WhatsappButton(),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        bottomNavigationBar: BottomNav(),
      ),
    );
  }

  Stack DashboardOpt(int toplftRadius, int toprhtRadius, int bottomlftRadius,
      int bottomrhtRadius, String urlofimage, String nameofservice) {
    return Stack(alignment: Alignment.center, children: [
      Container(
        width: 157.45147705078125.w,
        height: 154.188232421875.h,
        decoration: new BoxDecoration(
            color: Color(0xffffffff),
            border: Border.all(
              width: 1.w,
              color: Color(0xffBFBEBE),
            ),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(toplftRadius.r),
              topRight: Radius.circular(toprhtRadius.r),
              bottomLeft: Radius.circular(bottomlftRadius.r),
              bottomRight: Radius.circular(bottomrhtRadius.r),
            )),
      ),
      Column(
        children: [
          SvgPicture.asset(
            urlofimage,
            height: 50.h,
            width: 50.w,
          ),
          SizedBox(
            height: 10.h,
          ),
          Text(nameofservice,
              style: TextStyle(
                color: Color(0xff252525),
                fontSize: 15.sp,
                fontWeight: FontWeight.w800,
                fontStyle: FontStyle.normal,
                letterSpacing: -0.3,
              ))
        ],
      ),
    ]);
  }
}
