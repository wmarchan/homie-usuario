// ignore_for_file: prefer_const_constructors, non_constant_identifier_names;
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:homie/client.dart';
import 'package:homie/components/mapSearch.dart';
import 'package:homie/models/categoria.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/screens/busqueda_y_agenda/preview_orden.dart';
import 'package:homie/singletons/AppData.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class BusquedaOne extends StatefulWidget {
  final String imageurl;
  final String nameofservice;
  final int id;
  const BusquedaOne(
      {Key? key,
      required this.imageurl,
      required this.nameofservice,
      required this.id})
      : super(key: key);

  @override
  _BusquedaOneState createState() => _BusquedaOneState();
}

class _BusquedaOneState extends State<BusquedaOne> {
  final _keyform = GlobalKey<FormBuilderState>();

  List<String> daytime = const <String>[
    "Bloque mañana 1 (8:00 a 11:00)",
    "Bloque mañana 2 (11:00 a 14:00)",
    "Bloque tarde 3 (14:00 a 17:00)",
    "Bloque tarde 4 (17:00 a 20:00)"
  ];

  Categoria? categoria;

  void getCategoria() {
    Client().getCategoria(widget.id).then((value) {
      setState(() {
        categoria = value;
      });
    });
  }

  void createVisitaTecnica() {
    if (_keyform.currentState!.validate()) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PreviewOrden(
                id: widget.id,
                nameofservice: widget.nameofservice,
                imageurl: widget.imageurl),
            settings: RouteSettings(arguments: _keyform.currentState!.fields)),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    getCategoria();
  }

  DateTime getInitialDate() {
    DateTime now = DateTime.now();
    if (now.weekday == 6) {
      return now.add(Duration(days: 2));
    } else {
      return now.add(Duration(days: 1));
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
        decoration: BoxDecoration(
            // ignore: prefer_const_constructors
            gradient: LinearGradient(
          colors: [Color(0xff83dcff), Color(0xffeffdff)],
          stops: [0, 1],
          begin: Alignment(-1.00, -0.02),
          end: Alignment(1.00, 0.02),
          // angle: 91,
          // scale: undefined,
        )),
        child: SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.transparent,
            //APP BAR WITH BACK BUTTON AND TITLE AND IMAGE
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                //NAVIGATING BACK TO THE HOMEPAGE
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Container(
                child: Column(
                  children: [
                    SvgPicture.asset(
                      widget.imageurl,
                      height: 50.0,
                      width: 50.0,
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    Text(widget.nameofservice,
                        style: TextStyle(
                          color: Color(0xff252525),
                          fontSize: 23.sp,
                          fontWeight: FontWeight.w800,
                          fontStyle: FontStyle.normal,
                          letterSpacing: -0.3,
                        ))
                  ],
                ),
              ),
              toolbarHeight: 120.h,
              centerTitle: true,
            ),
            body: Column(
              children: [
                Expanded(
                  child: Container(
                    width: 375.w,
                    decoration: BoxDecoration(
                      color: Color(0xffffffff),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25)),
                      boxShadow: const [
                        BoxShadow(
                            color: Color(0x7c000000),
                            offset: Offset(5, -3),
                            blurRadius: 6,
                            spreadRadius: 0)
                      ],
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          FormBuilder(
                            key: _keyform,
                            child: Column(
                              children: [
                                SizedBox(height: 20.h),
                                Expanded(
                                  flex: 0,
                                  child: Container(
                                    width: 319.0.w,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      color: const Color(0xFFF9F9FC),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black.withOpacity(0.16),
                                          offset: Offset(5.0, 3.0),
                                          blurRadius: 6.0,
                                        ),
                                      ],
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.w, vertical: 15.w),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding:
                                                EdgeInsets.only(left: 5.0.w),
                                            child: Text(
                                                'Categoría o Tipo de Equipo',
                                                style: TextStyle(
                                                  color: Color(0xff3a3f47),
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.w800,
                                                  fontStyle: FontStyle.normal,
                                                  letterSpacing: 0.16,
                                                )),
                                          ),
                                          SizedBox(
                                            height: 10.h,
                                          ),
                                          if (categoria != null)
                                            FormBuilderDropdown(
                                              name: 'categoria',
                                              items: categoria!.subcategoria!
                                                  .map((e) => DropdownMenuItem(
                                                        value: e.id,
                                                        child: Text(e.nombre
                                                            .toString()),
                                                      ))
                                                  .toList(),
                                              decoration: InputDecoration(
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            38.r),
                                                  ),
                                                  contentPadding:
                                                      EdgeInsets.only(
                                                    left: 18.w,
                                                    top: 7.h,
                                                    bottom: 7.h,
                                                  )),
                                              validator: FormBuilderValidators
                                                  .compose([
                                                FormBuilderValidators.required(
                                                    context),
                                              ]),
                                            )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10.h),
                                Expanded(
                                  flex: 0,
                                  child: Container(
                                    width: 319.0.w,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      color: const Color(0xFFF9F9FC),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black.withOpacity(0.16),
                                          offset: Offset(5.0, 3.0),
                                          blurRadius: 6.0,
                                        ),
                                      ],
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.w, vertical: 15.w),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding:
                                                EdgeInsets.only(left: 5.0.w),
                                            child: Text(
                                                'Descripción del Problema',
                                                style: TextStyle(
                                                  color: Color(0xff3a3f47),
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.w800,
                                                  fontStyle: FontStyle.normal,
                                                  letterSpacing: 0.16,
                                                )),
                                          ),
                                          SizedBox(
                                            height: 10.h,
                                          ),
                                          FormBuilderTextField(
                                            name: 'descripcion',
                                            maxLines: 5,
                                            maxLength: 255,
                                            decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(38.r),
                                              ),
                                              hintText:
                                                  'Describe el problema que debemos revisar (ej: equipo no prende, equipo no enfría, etc)',
                                            ),
                                            validator:
                                                FormBuilderValidators.compose([
                                              FormBuilderValidators.required(
                                                  context),
                                            ]),
                                            keyboardType:
                                                TextInputType.multiline,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10.h),
                                Expanded(
                                  flex: 0,
                                  child: Container(
                                    width: 319.0.w,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      color: const Color(0xFFF9F9FC),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black.withOpacity(0.16),
                                          offset: Offset(5.0, 3.0),
                                          blurRadius: 6.0,
                                        ),
                                      ],
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.w, vertical: 15.w),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding:
                                                EdgeInsets.only(left: 5.0.w),
                                            child: Text('Teléfono',
                                                style: TextStyle(
                                                  color: Color(0xff3a3f47),
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.w800,
                                                  fontStyle: FontStyle.normal,
                                                  letterSpacing: 0.16,
                                                )),
                                          ),
                                          SizedBox(
                                            height: 10.h,
                                          ),
                                          FormBuilderTextField(
                                            name: 'telefono',
                                            initialValue:
                                                appData.usuario.user!.telefono,
                                            decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(38.r),
                                              ),
                                              hintText: 'Teléfono de contacto',
                                            ),
                                            validator:
                                                FormBuilderValidators.compose([
                                              FormBuilderValidators.required(
                                                  context),
                                            ]),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10.h,
                                ),
                                Container(
                                  width: 319.0.w,
                                  height: 112.0.w,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20.0),
                                    color: const Color(0xFFF9F9FC),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black.withOpacity(0.16),
                                        offset: Offset(5.0, 3.0),
                                        blurRadius: 6.0,
                                      ),
                                    ],
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 8.0.w, vertical: 18.0.h),
                                    //inside the card
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(left: 8.0.w),
                                          child: Text('Tipo de Servicio',
                                              style: TextStyle(
                                                color: Color(0xff3a3f47),
                                                fontSize: 16.sp,
                                                fontWeight: FontWeight.w800,
                                                fontStyle: FontStyle.normal,
                                                letterSpacing: 0.16,
                                              )),
                                        ),
                                        SizedBox(
                                          height: 10.h,
                                        ),
                                        FormBuilderField(
                                            name: "tipo",
                                            validator:
                                                FormBuilderValidators.compose([
                                              FormBuilderValidators.required(
                                                  context),
                                            ]),
                                            builder: (FormFieldState field) {
                                              return Container(
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            25.0),
                                                    border: field.hasError
                                                        ? Border.all(
                                                            color: Colors.red)
                                                        : null),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    //OPTION 1
                                                    GestureDetector(
                                                      onTap: () {
                                                        field.didChange(1);
                                                      },
                                                      child: Container(
                                                        width:
                                                            90.413818359375.w,
                                                        height: 50.w,
                                                        decoration:
                                                            BoxDecoration(
                                                          color: field.value ==
                                                                  1
                                                              ? Color(
                                                                  0xff009ade)
                                                              : Color(
                                                                  0xfff5fafc),
                                                          boxShadow: const [
                                                            BoxShadow(
                                                                color: Color(
                                                                    0x29000000),
                                                                offset: Offset(
                                                                    5, 3),
                                                                blurRadius: 6,
                                                                spreadRadius: 0)
                                                          ],
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      15.r),
                                                        ),
                                                        child: Center(
                                                          child: Text(
                                                              "Diagnóstico o Cotización",
                                                              textAlign:
                                                                  TextAlign
                                                                      .center,
                                                              style: TextStyle(
                                                                color: field.value ==
                                                                        1
                                                                    ? Color(
                                                                        0xfffdfdfd)
                                                                    : Color(
                                                                        0xff252525),
                                                                fontSize: 12.sp,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                              )),
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 20.w,
                                                    ),
                                                    //OPTION 2
                                                    GestureDetector(
                                                      onTap: () {
                                                        setState(() {
                                                          field.didChange(2);
                                                        });
                                                      },
                                                      child: Container(
                                                        width:
                                                            90.413818359375.w,
                                                        height: 50.w,
                                                        decoration:
                                                            BoxDecoration(
                                                          color: field.value ==
                                                                  2
                                                              ? Color(
                                                                  0xff009ade)
                                                              : Color(
                                                                  0xfff5fafc),
                                                          boxShadow: const [
                                                            BoxShadow(
                                                                color: Color(
                                                                    0x29000000),
                                                                offset: Offset(
                                                                    5, 3),
                                                                blurRadius: 6,
                                                                spreadRadius: 0)
                                                          ],
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      15.r),
                                                        ),
                                                        child: Center(
                                                          child: Text(
                                                              "Urgencia o\nFuera de hora",
                                                              textAlign:
                                                                  TextAlign
                                                                      .center,
                                                              style: TextStyle(
                                                                color: field.value ==
                                                                        2
                                                                    ? Color(
                                                                        0xfffdfdfd)
                                                                    : Color(
                                                                        0xff252525),
                                                                fontSize: 12.sp,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                              )),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              );
                                            }),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10.h),
                                MapSearch(
                                  formKey: _keyform,
                                ),
                                SizedBox(height: 10.h),
                                Expanded(
                                  flex: 0,
                                  child: Container(
                                    width: 319.0.w,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      color: const Color(0xFFF9F9FC),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black.withOpacity(0.16),
                                          offset: Offset(5.0, 3.0),
                                          blurRadius: 6.0,
                                        ),
                                      ],
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.w, vertical: 15.w),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding:
                                                EdgeInsets.only(left: 5.0.w),
                                            child: Text('Referencias',
                                                style: TextStyle(
                                                  color: Color(0xff3a3f47),
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.w800,
                                                  fontStyle: FontStyle.normal,
                                                  letterSpacing: 0.16,
                                                )),
                                          ),
                                          SizedBox(
                                            height: 10.h,
                                          ),
                                          FormBuilderTextField(
                                            name: 'referencia',
                                            decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(38.r),
                                              ),
                                              hintText:
                                                  'Indicar alguna referencia de la ubicación del dominio',
                                            ),
                                            validator:
                                                FormBuilderValidators.compose([
                                              FormBuilderValidators.required(
                                                  context),
                                            ]),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10.h),
                                Expanded(
                                  flex: 0,
                                  child: Container(
                                    width: 319.0.w,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      color: const Color(0xFFF9F9FC),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black.withOpacity(0.16),
                                          offset: Offset(5.0, 3.0),
                                          blurRadius: 6.0,
                                        ),
                                      ],
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 8.0.w, vertical: 18.0.h),
                                      //inside the card
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding:
                                                EdgeInsets.only(left: 8.0.w),
                                            child: Text('Fecha y Hora Estimada',
                                                style: TextStyle(
                                                  color: Color(0xff3a3f47),
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.w800,
                                                  fontStyle: FontStyle.normal,
                                                  letterSpacing: 0.16,
                                                )),
                                          ),
                                          SizedBox(
                                            height: 10.h,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: FormBuilderDateTimePicker(
                                              name: "weekdays",
                                              inputType: InputType.date,
                                              selectableDayPredicate: (day) {
                                                return day.weekday != 7;
                                              },
                                              firstDate: DateTime.now()
                                                  .add(Duration(days: 1)),
                                              initialDate: getInitialDate(),
                                              decoration: InputDecoration(
                                                labelText: "Día de la semana",
                                                border: InputBorder.none,
                                              ),
                                              validator: FormBuilderValidators
                                                  .compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ]),
                                            ),
                                          ),
                                          Divider(
                                            endIndent: 20.0,
                                            indent: 20.0,
                                            color: Colors.black26,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: FormBuilderDropdown(
                                              name: "daytime",
                                              decoration: InputDecoration(
                                                labelText: "Hora del día",
                                                border: InputBorder.none,
                                              ),
                                              hint: Text('Elige Hora del día'),
                                              validator: FormBuilderValidators
                                                  .compose([
                                                FormBuilderValidators.required(
                                                    context)
                                              ]),
                                              items: daytime
                                                  .map((daytime) =>
                                                      DropdownMenuItem(
                                                          value: daytime,
                                                          child:
                                                              Text("$daytime")))
                                                  .toList(),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10.h,
                                ),
                                Container(
                                  width: 319.0.w,
                                  height: 180.w,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20.0),
                                    color: const Color(0xFFF9F9FC),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black.withOpacity(0.16),
                                        offset: Offset(5.0, 3.0),
                                        blurRadius: 6.0,
                                      ),
                                    ],
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 8.0.w, vertical: 10.0.h),
                                    //inside the card
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(left: 8.0.w),
                                          child: Text('Método de Pago',
                                              style: TextStyle(
                                                color: Color(0xff3a3f47),
                                                fontSize: 16.sp,
                                                fontWeight: FontWeight.w800,
                                                fontStyle: FontStyle.normal,
                                                letterSpacing: 0.16,
                                              )),
                                        ),
                                        SizedBox(
                                          height: 12.h,
                                        ),
                                        FormBuilderRadioGroup(
                                            name: "metodo_pago",
                                            controlAffinity:
                                                ControlAffinity.trailing,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                            ),
                                            validator:
                                                FormBuilderValidators.compose([
                                              FormBuilderValidators.required(
                                                  context),
                                            ]),
                                            options: [
                                              FormBuilderFieldOption(
                                                value: 'Tarjeta',
                                                child: Row(
                                                  children: [
                                                    SvgPicture.asset(
                                                        'assets/images/wallet.svg'),
                                                    SizedBox(
                                                      width: 25.w,
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                            "Tarjeta Credito o debito",
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff080f18),
                                                              fontSize: 14.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal,
                                                            )),
                                                        Text("Pago con tarjeta",
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff080f18),
                                                              fontSize: 12.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal,
                                                            )),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              FormBuilderFieldOption(
                                                value: 'Paypal',
                                                child: Row(
                                                  children: [
                                                    SvgPicture.asset(
                                                        'assets/images/money.svg'),
                                                    SizedBox(
                                                      width: 25.w,
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text("Pago con Paypal",
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff080f18),
                                                              fontSize: 14.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal,
                                                            )),
                                                        Text("Proximamente",
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff080f18),
                                                              fontSize: 12.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal,
                                                            )),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ]),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // THIRD FIELD

                          //PAYMENT OPTION CARD

                          SizedBox(
                            height: 14.h,
                          ),

                          //BUTTTON
                          Container(
                            width: 307.w,
                            height: 49.w,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(38.0.r),
                              color: const Color(0xff21409a),
                              boxShadow: const [
                                BoxShadow(
                                  color: Color(0x29000000),
                                  offset: Offset(5, 3),
                                  blurRadius: 6,
                                  spreadRadius: 0,
                                ),
                              ],
                            ),
                            child: TextButton(
                              //THIS WILL REDIRECT TO PAGO PAGE
                              onPressed: () {
                                createVisitaTecnica();
                              },
                              child: Text(
                                "SIGUIENTE",
                                style: TextStyle(
                                  color: Color(0xffffffff),
                                  fontSize: 15.sp,
                                  fontWeight: FontWeight.w800,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 54.h,
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),

            //BOTTOM BAR WITH FLOATING BTN

            floatingActionButton: WhatsappButton(),
            floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
            bottomNavigationBar: BottomNav(),
          ),
        ));
  }

  //card container with textfield input

  Container CardContainer(String CardName, String hintText, GlobalKey formkey) {
    return Container(
      width: 319.0.w,
      height: 112.0.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: const Color(0xFFF9F9FC),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.16),
            offset: Offset(5.0, 3.0),
            blurRadius: 6.0,
          ),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 8.0.w, vertical: 18.0.h),
        //inside the card
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 8.0.w),
              child: Text(CardName,
                  style: TextStyle(
                    color: Color(0xff3a3f47),
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w800,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0.16,
                  )),
            ),
            SizedBox(
              height: 10.h,
            ),
            TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(38.r),
                  ),
                  hintText: hintText,
                  contentPadding: EdgeInsets.only(
                    left: 18.w,
                    top: 7.h,
                    bottom: 7.h,
                  )),
            ),
          ],
        ),
      ),
    );
  }

  void _onChanged(String? value) {}
}
