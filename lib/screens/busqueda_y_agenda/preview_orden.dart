// ignore_for_file: non_constant_identifier_names, prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:homie/client.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/historial_citas_diagnostico/cita_finalizada.dart';
import 'package:homie/singletons/AppData.dart';
import "package:google_maps_webservice/places.dart";
import 'package:homie/navigation/whatsapp_button.dart';

class PreviewOrden extends StatefulWidget {
  final String imageurl;
  final String nameofservice;
  final int id;
  const PreviewOrden(
      {Key? key,
      required this.imageurl,
      required this.nameofservice,
      required this.id})
      : super(key: key);

  @override
  _BusquedaState createState() => _BusquedaState();
}

class _BusquedaState extends State<PreviewOrden> {
  void pagar() async {
    setState(() {
      loadingPayment = true;
    });

    final places =
        GoogleMapsPlaces(apiKey: "AIzaSyDfxwxpQsOSKNlIzqPe-KBlTCtwQEt_2xo");
    var location, place;
    if (fields!["map"]!.value.runtimeType == Prediction) {
      Prediction prediction = fields!["map"]!.value;
      location =
          await places.getDetailsByPlaceId(prediction.placeId.toString());
      place = location.result;
    } else {
      place = fields!["map"]!.value as PlacesSearchResult;
    }

    var visitaTecnica = await Client().payCreditVisita({
      "contacto": appData.usuario.user!.nombre,
      "descripcion": fields!["descripcion"]!.value,
      "referencia": fields!["referencia"]!.value,
      "SubcategoriumId": fields!["categoria"]!.value,
      "categoria": widget.id,
      "ubicacion": place.geometry!.location.toJson(),
      "direccion": place.formattedAddress,
      "dia": fields!["weekdays"]!.value.toString(),
      "hora": fields!["daytime"]!.value,
      "TipoServicioId": fields!["tipo"]!.value,
      "pago": fields!["metodo_pago"]!.value,
      "telefono": fields!["telefono"]!.value,
      "UsuarioId": appData.usuario.user!.id,
    });

    setState(() {
      loadingPayment = false;
    });
    if (visitaTecnica != null) {
      ShowDialogBox(
          (context),
          'Cita #${visitaTecnica.id} Agendada \n correctamente',
          'Puede Verificar el status\n en su perfil',
          false,
          false, onClose: () {
        Navigator.pushNamedAndRemoveUntil(context, "/home", (route) => false);
      });
    } else {
      ShowDialogBox(
          (context),
          'Error',
          'No se pudo agendar la cita, intente nuevamente su pago',
          false,
          false, onClose: () {
        // Navigator.pushNamedAndRemoveUntil(context, "/home", (route) => false);
      });
    }
  }

  bool loading = true;
  bool loadingPayment = false;
  double total = 0;
  Map<String, FormBuilderFieldState>? fields;
  void getTotal() async {
    fields = ModalRoute.of(context)!.settings.arguments
        as Map<String, FormBuilderFieldState>;
    var categoria = widget.id;
    var response = await Client().getConfiguracion(fields!["tipo"]!.value == 1
        ? "precio_diagnostico_$categoria"
        : "precio_urgente_$categoria");
    setState(() {
      total = double.parse(response);
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero).then((value) {
      getTotal();
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: const [Color(0xff83dcff), Color(0xffeffdff)],
          stops: const [0, 1],
          begin: Alignment(-1.00, -0.02),
          end: Alignment(1.00, 0.02),
        )),
        child: SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.black),
                //NAVIGATING BACK TO THE HOMEPAGE
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Container(
                child: Column(
                  children: [
                    SvgPicture.asset(
                      widget.imageurl,
                      height: 50.0,
                      width: 50.0,
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    Text("Pago",
                        style: TextStyle(
                          color: Color(0xff252525),
                          fontSize: 23.sp,
                          fontWeight: FontWeight.w800,
                          fontStyle: FontStyle.normal,
                          letterSpacing: -0.3,
                        ))
                  ],
                ),
              ),
              toolbarHeight: 120.h,
              centerTitle: true,
            ),
            body: loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 22.w, 0, 0),
                        width: 1100.w,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25.r),
                            topRight: Radius.circular(25.r),
                          ),
                          color: Color(0xffffffff),
                          boxShadow: const [
                            BoxShadow(
                                color: Color(0x7c000000),
                                offset: Offset(10, -3),
                                blurRadius: 20,
                                spreadRadius: 0)
                          ],
                        ),
                        child: Center(
                          child: Column(
                            children: [
                              SizedBox(
                                height: 40.h,
                              ),
                              Text('PEDIDO',
                                  style: TextStyle(
                                    color: Color(0xff00013c),
                                    fontSize: 22.sp,
                                    fontWeight: FontWeight.w300,
                                    fontStyle: FontStyle.normal,
                                  )),
                              SizedBox(
                                height: 10.h,
                              ),
                              Container(
                                width: 370.619384765625.w,
                                height: 160.450439453125.h,
                                margin:
                                    EdgeInsets.fromLTRB(10.w, 10.h, 10.w, 10.h),
                                decoration: BoxDecoration(
                                    color: Color(0xffffffff),
                                    border: Border.all(
                                      color: Color(0xffe0e0e0),
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.circular(5.r)),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 10.h,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 12.w, vertical: 4.h),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Visita Técnica",
                                            style: TextStyle(
                                              color: Color(0xff263238),
                                              fontSize: 18.sp,
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          ),
                                          Text('\$${total.toStringAsFixed(2)}',
                                              style: TextStyle(
                                                color: Color(0xff263238),
                                                fontSize: 16.sp,
                                                fontWeight: FontWeight.w700,
                                                fontStyle: FontStyle.normal,
                                              ))
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 12.w, vertical: 2.h),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Evaluación y presupuesto",
                                            style: TextStyle(
                                              color: Color(0xff90a4ae),
                                              fontSize: 13.sp,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 15,
                                          ),
                                          //DELETE BUTTON
                                          IconButton(
                                              // FUNCTION FOR DELETING
                                              onPressed: () {
                                                Navigator.of(context)
                                                    .pushNamedAndRemoveUntil(
                                                        "/home",
                                                        (route) => false);
                                              },
                                              icon: Icon(Icons
                                                  .delete_outline_outlined)),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 2.h,
                                    ),
                                    Container(
                                        width: 370.619384765625.w,
                                        height: 0.59423828125.h,
                                        decoration: BoxDecoration(
                                            color: Color(0xffe0e0e0)))
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 5.h,
                              ),
                              Container(
                                width: 370.619384765625.w,
                                height: 145.h,
                                margin:
                                    EdgeInsets.fromLTRB(10.h, 10.w, 10.h, 10.w),
                                decoration: BoxDecoration(
                                    color: Color(0xffffffff),
                                    border: Border.all(
                                      color: Color(0xffe0e0e0),
                                      width: 2.w,
                                    ),
                                    borderRadius: BorderRadius.circular(5.r)),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 5.h,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 12.w, vertical: 6.h),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Subtotal:",
                                            style: TextStyle(
                                              color: Color(0xff263238),
                                              fontSize: 18.sp,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          ),
                                          Text('\$${total.toStringAsFixed(2)}',
                                              style: TextStyle(
                                                color: Color(0xff263238),
                                                fontSize: 16.sp,
                                                fontWeight: FontWeight.w700,
                                                fontStyle: FontStyle.normal,
                                              ))
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10.h,
                                    ),
                                    Container(
                                        width: 370.619384765625.w,
                                        height: 0.59423828125.h,
                                        decoration: BoxDecoration(
                                            color: Color(0xffe0e0e0))),
                                    SizedBox(
                                      height: 43.h,
                                    ),
                                    Container(
                                        width: 370.619384765625.w,
                                        height: 0.59423828125.h,
                                        decoration: BoxDecoration(
                                            color: Color(0xffe0e0e0))),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 14.w, vertical: 10.h),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Total:",
                                            style: TextStyle(
                                              color: Color(0xff263238),
                                              fontSize: 20.sp,
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          ),
                                          Text('\$${total.toStringAsFixed(2)}',
                                              style: TextStyle(
                                                color: Color(0xff263238),
                                                fontSize: 20.sp,
                                                fontWeight: FontWeight.w700,
                                                fontStyle: FontStyle.normal,
                                              ))
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10.h,
                              ),
                              InkWell(
                                onTap: loadingPayment ? null : pagar,
                                child: Container(
                                  width: 307.w,
                                  height: 49.h,
                                  decoration: BoxDecoration(
                                    color: Color(0xff21409a),
                                    borderRadius: BorderRadius.circular(80.r),
                                    boxShadow: [
                                      BoxShadow(
                                          color: const Color(0x29000000),
                                          offset: Offset(5, 3),
                                          blurRadius: 6,
                                          spreadRadius: 0)
                                    ],
                                  ),
                                  child: Center(
                                    child: loadingPayment
                                        ? CircularProgressIndicator()
                                        : Text("SIGUIENTE",
                                            style: TextStyle(
                                              color: Color(0xffffffff),
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                  ),
                                ),
                              ),
                              SizedBox(height: 10.h),
                              Text(
                                'Se tomará a cuenta de la reparación total',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16.sp,
                                ),
                              ),
                              SizedBox(height: 50),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
            //BOTTOM BAR WITH FLOATING BTN
            floatingActionButton: WhatsappButton(),
            floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
            bottomNavigationBar: BottomNav(),
          ),
        ));
  }
}
