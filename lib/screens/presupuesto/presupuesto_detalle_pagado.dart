import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:homie/client.dart';
import 'package:homie/models/presupuesto/item_presupuesto.dart';
import 'package:homie/models/presupuesto/presupuesto.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/presupuesto/presupuesto.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:homie/screens/presupuesto/presupuesto_two.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class PresupuestoDetallePagado extends StatefulWidget {
  const PresupuestoDetallePagado({Key? key}) : super(key: key);

  @override
  _PresupuestoDetallePagadoState createState() =>
      _PresupuestoDetallePagadoState();
}

class _PresupuestoDetallePagadoState extends State<PresupuestoDetallePagado> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  late Presupuesto presupuesto;

  String getTotal() {
    double total = presupuesto.costo!.toDouble();
    for (var i = 0; i < presupuesto.itemPresupuestos!.length; i++) {
      total += presupuesto.itemPresupuestos![i].precio!.toDouble() *
          presupuesto.itemPresupuestos![i].cantidad!.toDouble();
    }
    total -= presupuesto.visitaTecnica!.pagado!.total!.toDouble();
    total < 0 ? total = 0 : total;
    return total.toStringAsFixed(2);
  }

  void aceptar() async {
    await Client().updatePresupuesto({
      "id": presupuesto.id,
      "status": "Completado",
    });
    ShowDialogBox(
        (context),
        'SERVICIO FINALIZADO',
        'Gracias por aceptar el fin del trabajo. Puede calificar al técnico o regresar al inicio',
        true,
        false, onCalificar: () {
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      Navigator.of(context).pushNamed("/citas/calificar",
          arguments: {"presupuesto": presupuesto});
    });
  }

  void rechazar() async {
    await Client().updatePresupuesto({
      "id": presupuesto.id,
      "status": "Rechazada",
    });
    ShowDialogBox(
        context,
        'Rechazaste el presupuesto',
        'Lamentamos que hayas rechazado el presupuesto que te enviamos. Pero no te preocupes, siempre puedes agendas la visita de otro técnico',
        false,
        true);
  }

  @override
  Widget build(BuildContext context) {
    presupuesto = ModalRoute.of(context)!.settings.arguments as Presupuesto;
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [Color(0xff83dcff), Color(0xffeffdff)],
          stops: [0, 1],
          begin: Alignment(-1.00, -0.02),
          end: Alignment(1.00, 0.02),
        ),
      ),
      child: Scaffold(
        key: _globalKey,
        endDrawer: Navbar(),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black),
            //NAVIGATING BACK TO THE HOMEPAGE
            onPressed: () => Navigator.of(context).pop(),
          ),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20.0.w),
              child: GestureDetector(
                //THIS FUNCTION WILL OPEN MENU DRAWER
                onTap: () {
                  _globalKey.currentState?.openEndDrawer();
                },
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: 58.41377258300781.w,
                      height: 57.504638671875.w,
                      decoration: const BoxDecoration(
                        color: Color(0xff009ade),
                        shape: BoxShape.circle,
                      ),
                    ),
                    SvgPicture.asset('assets/images/menu-icon.svg'),
                  ],
                ),
              ),
            ),
          ],
          toolbarHeight: 80.h,
        ),
        backgroundColor: Colors.transparent,
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
              padding: EdgeInsets.only(left: 16.0.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Presupuesto",
                      style: TextStyle(
                        color: Color(0xff00013c),
                        fontSize: 23.sp,
                        fontWeight: FontWeight.w900,
                        fontStyle: FontStyle.normal,
                      )),
                  Text("Orden Relacionada: #${presupuesto.visitaTecnicaId}",
                      style: TextStyle(
                        color: Color(0xff252525),
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        letterSpacing: -0.45,
                      )),
                ],
              )),
          SizedBox(
            height: 24.h,
          ),
          Expanded(
            child: Container(
              width: 375.w,
              decoration: BoxDecoration(
                color: Color(0xffffffff),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25)),
                boxShadow: [
                  BoxShadow(
                      color: Color(0x7c000000),
                      offset: Offset(5, -3),
                      blurRadius: 6,
                      spreadRadius: 0)
                ],
              ),
              child: Column(children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 21.0.w),
                          child: Row(
                            children: [
                              Stack(
                                alignment: Alignment.center,
                                children: [
                                  Container(
                                      width: 92.w,
                                      height: 92.w,
                                      decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Color(0xfff7f7f7))),
                                  Container(
                                    width: 76.w,
                                    height: 76.w,
                                    decoration: new BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(0x23646464),
                                            offset: Offset(-7, 3),
                                            blurRadius: 4,
                                            spreadRadius: 0)
                                      ],
                                      color: Color(0xff009ade),
                                      shape: BoxShape.circle,
                                    ),
                                    child: SvgPicture.asset(
                                      'assets/images/test-icon.svg',
                                      fit: BoxFit.scaleDown,
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                width: 20.w,
                              ),
                              Text("Orden #${presupuesto.visitaTecnicaId}",
                                  style: TextStyle(
                                    color: Color(0xff222b45),
                                    fontSize: 25.sp,
                                    fontWeight: FontWeight.w600,
                                    fontStyle: FontStyle.normal,
                                  ))
                            ],
                          ),
                        ),
                        Container(
                          width: 343.w,
                          height: 222.w,
                          decoration: new BoxDecoration(
                            color: Color(0xffffffff),
                            borderRadius: BorderRadius.circular(12),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0x29000000),
                                  offset: Offset(0, 3),
                                  blurRadius: 6,
                                  spreadRadius: 0)
                            ],
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 16.w, vertical: 25.w),
                            child: Column(children: [
                              if (presupuesto.status == "Agendado" ||
                                  presupuesto.status == "En curso" ||
                                  presupuesto.status == "Finalizado")
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Día",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text(
                                          DateFormat("MMMM dd,yyyy").format(
                                              DateTime.parse(presupuesto.fecha
                                                  .toString())),
                                          style: TextStyle(
                                            color: Color(0xff007aff),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]),
                              SizedBox(
                                height: 20.h,
                              ),
                              if (presupuesto.status == "Agendado" ||
                                  presupuesto.status == "En curso" ||
                                  presupuesto.status == "Finalizado")
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Hora",
                                          style: TextStyle(
                                            color: Color(0xff8f9bb3),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text("Bloque mañana 1 (8:00 a 11:00)",
                                          style: TextStyle(
                                            color: Color(0xff007aff),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ]),
                              SizedBox(
                                height: 15.h,
                              ),
                              Divider(
                                height: 2.w,
                                color: Color(0xffD9DEE4),
                                indent: 16.3.w,
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Status",
                                        style: TextStyle(
                                          color: Color(0xff8f9bb3),
                                          fontSize: 15.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    Text("${presupuesto.status}",
                                        style: TextStyle(
                                          color: Color(0xff222b45),
                                          fontSize: 15.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        ))
                                  ]),
                              SizedBox(
                                height: 20.h,
                              ),
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Tipo",
                                        style: TextStyle(
                                          color: Color(0xff8f9bb3),
                                          fontSize: 15.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    Text(
                                        "${presupuesto.visitaTecnica!.tipoServicio!.nombre}",
                                        style: TextStyle(
                                          color: Color(0xff222b45),
                                          fontSize: 15.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        ))
                                  ]),
                              SizedBox(
                                height: 20.h,
                              ),
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Problema",
                                        style: TextStyle(
                                          color: Color(0xff8f9bb3),
                                          fontSize: 15.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                      child: Text(
                                          "${presupuesto.visitaTecnica!.descripcion}",
                                          maxLines: 1,
                                          style: TextStyle(
                                            color: Color(0xff222b45),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w400,
                                            fontStyle: FontStyle.normal,
                                          )),
                                    )
                                  ]),
                            ]),
                          ),
                        ),
                        SizedBox(
                          height: 20.h,
                        ),
                        if (presupuesto.status == "Finalizado")
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 34.0.w),
                                child: Text("REPORTE",
                                    style: TextStyle(
                                      color: Color(0xff222b45),
                                      fontSize: 15.sp,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )),
                              ),
                              SizedBox(height: 7.h),
                              Container(
                                  width: 343.w,
                                  height: 131.h,
                                  decoration: new BoxDecoration(
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(12),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x29000000),
                                          offset: Offset(0, 3),
                                          blurRadius: 6,
                                          spreadRadius: 0)
                                    ],
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 13.w, vertical: 21.h),
                                    child: Text(presupuesto.reporte ?? "",
                                        style: TextStyle(
                                          color: Color(0xff222b45),
                                          fontSize: 15.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        )),
                                  )),
                              SizedBox(height: 30.h),
                            ],
                          ),
                        if (presupuesto.status == "Finalizado")
                          Column(
                            children: [
                              Container(
                                width: 160.w,
                                height: 49.w,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1,
                                    color: Color(0xffF5FAFC),
                                    style: BorderStyle.solid,
                                  ),
                                  borderRadius: BorderRadius.circular(38.0.r),
                                  color: const Color(0xff009ade),
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Color(0x29000000),
                                      offset: Offset(5, 3),
                                      blurRadius: 6,
                                      spreadRadius: 0,
                                    ),
                                  ],
                                ),
                                child: TextButton(
                                  //THIS WILL REDIRECT TO NEXT PAGE
                                  onPressed: aceptar,
                                  child: Text(
                                    "ACEPTAR",
                                    style: TextStyle(
                                      color: Color(0xffffffff),
                                      fontSize: 15.sp,
                                      fontWeight: FontWeight.w800,
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 20.h),
                              InkWell(
                                //CONTACTO TEXT BUTTON FUCTION WHICH CAN BE USED FOR DIRECTION
                                onTap: () {
                                  launch(
                                      "tel:${presupuesto.tecnico!.telefono}");
                                },
                                child: Text("Contacto",
                                    style: TextStyle(
                                      decoration: TextDecoration.underline,
                                      color: Color(0xff707070),
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w800,
                                      fontStyle: FontStyle.normal,
                                    )),
                              )
                            ],
                          ),
                        SizedBox(height: 50.h),
                      ],
                    ),
                  ),
                )
              ]),
            ),
          ),
        ]),

        //BOTTOM BAR WITH FLOATING BTN
        floatingActionButton: WhatsappButton(),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        bottomNavigationBar: const BottomNav(),
      ),
    );
  }
}
