// ignore_for_file: non_constant_identifier_names, prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:homie/client.dart';
import 'package:homie/models/presupuesto/presupuesto.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class PresupestoTwo extends StatefulWidget {
  const PresupestoTwo({Key? key}) : super(key: key);

  @override
  _PresupestoTwoState createState() => _PresupestoTwoState();
}

class _PresupestoTwoState extends State<PresupestoTwo> {
  late Presupuesto presupuesto;

  bool loading = false;

  String getTotal() {
    double total = presupuesto.costo!.toDouble();
    for (var i = 0; i < presupuesto.itemPresupuestos!.length; i++) {
      total += presupuesto.itemPresupuestos![i].precio!.toDouble() *
          presupuesto.itemPresupuestos![i].cantidad!.toDouble();
    }
    total -= presupuesto.visitaTecnica!.pagado!.total!.toDouble();
    total < 0 ? total = 0 : total;
    return total.toStringAsFixed(2);
  }

  void pagar() async {
    setState(() {
      loading = true;
    });
    await Client().payCreditPresupuesto(presupuesto);
    setState(() {
      loading = false;
    });
    ShowDialogBox(
      (context),
      'PAGO REALIZADO \n correctamente',
      'El técnico le confirmará la cita para\n ejecutar su trabajo \nEl status ha cambiado a\n CONFIRMADO Puede ver los detalles \n en su perfil.',
      false,
      false,
    );
  }

  @override
  Widget build(BuildContext context) {
    presupuesto = ModalRoute.of(context)!.settings.arguments as Presupuesto;

    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
        colors: const [Color(0xff83dcff), Color(0xffeffdff)],
        stops: const [0, 1],
        begin: Alignment(-1.00, -0.02),
        end: Alignment(1.00, 0.02),
      )),
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              //NAVIGATING BACK TO THE HOMEPAGE
              onPressed: () => Navigator.of(context).pop(),
            ),
            toolbarHeight: 120.h,
            centerTitle: true,
          ),
          body: Column(
            children: [
              Text(
                "PAGO",
                style: TextStyle(
                  color: Color(0xff252525),
                  fontSize: 25.sp,
                  fontWeight: FontWeight.w900,
                  fontStyle: FontStyle.normal,
                  letterSpacing: -0.3,
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 22.w, 0, 0),
                  width: 1100.w,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25.r),
                      topRight: Radius.circular(25.r),
                    ),
                    color: Color(0xffffffff),
                    boxShadow: const [
                      BoxShadow(
                        color: Color(0x7c000000),
                        offset: Offset(10, -3),
                        blurRadius: 20,
                        spreadRadius: 0,
                      )
                    ],
                  ),
                  child: Center(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          SizedBox(
                            height: 40.h,
                          ),
                          Text('PEDIDO',
                              style: TextStyle(
                                color: Color(0xff00013c),
                                fontSize: 22.sp,
                                fontWeight: FontWeight.w300,
                                fontStyle: FontStyle.normal,
                              )),
                          SizedBox(
                            height: 10.h,
                          ),
                          Column(
                            children: [
                              Container(
                                width: 370.619384765625.w,
                                height: 160.450439453125.h,
                                margin:
                                    EdgeInsets.fromLTRB(10.w, 10.h, 10.w, 10.h),
                                decoration: BoxDecoration(
                                    color: Color(0xffffffff),
                                    border: Border.all(
                                      color: Color(0xffe0e0e0),
                                      width: 2,
                                    ),
                                    borderRadius: BorderRadius.circular(5.r)),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 10.h,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 12.w, vertical: 4.h),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Ejecución Presupuesto\nN ${presupuesto.id}",
                                            style: TextStyle(
                                              color: Color(0xff263238),
                                              fontSize: 15.sp,
                                              fontWeight: FontWeight.w800,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          ),
                                          Text(
                                            '\$${getTotal()}',
                                            style: TextStyle(
                                              color: Color(0xff263238),
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w700,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                        horizontal: 12.w,
                                      ),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          SizedBox(
                                            width: 15.w,
                                          ),
                                          //DELETE BUTTON
                                          IconButton(
                                            // FUNCTION FOR DELETING
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            icon: Icon(
                                                Icons.delete_outline_outlined),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 2.h,
                                    ),
                                    Container(
                                        width: 370.619384765625.w,
                                        height: 0.59423828125.h,
                                        decoration: BoxDecoration(
                                            color: Color(0xffe0e0e0)))
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 5.h,
                          ),
                          Container(
                            width: 370.619384765625.w,
                            height: 145.h,
                            margin: EdgeInsets.fromLTRB(10.h, 10.w, 10.h, 10.w),
                            decoration: BoxDecoration(
                                color: Color(0xffffffff),
                                border: Border.all(
                                  color: Color(0xffe0e0e0),
                                  width: 2.w,
                                ),
                                borderRadius: BorderRadius.circular(5.r)),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 5.h,
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 12.w, vertical: 6.h),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Subtotal:",
                                        style: TextStyle(
                                          color: Color(0xff263238),
                                          fontSize: 18.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                      Text('\$${getTotal()}',
                                          style: TextStyle(
                                            color: Color(0xff263238),
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 10.h,
                                ),
                                Container(
                                    width: 370.619384765625.w,
                                    height: 0.59423828125.h,
                                    decoration: BoxDecoration(
                                        color: Color(0xffe0e0e0))),
                                SizedBox(
                                  height: 43.h,
                                ),
                                Container(
                                    width: 370.619384765625.w,
                                    height: 0.59423828125.h,
                                    decoration: BoxDecoration(
                                        color: Color(0xffe0e0e0))),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 14.w, vertical: 10.h),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Total:",
                                        style: TextStyle(
                                          color: Color(0xff263238),
                                          fontSize: 20.sp,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                      Text(
                                        '\$${getTotal()}',
                                        style: TextStyle(
                                          color: Color(0xff263238),
                                          fontSize: 20.sp,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          InkWell(
                            onTap: loading ? null : pagar,
                            child: Container(
                              width: 307.w,
                              height: 49.h,
                              decoration: BoxDecoration(
                                color: Color(0xff21409a),
                                borderRadius: BorderRadius.circular(80.r),
                                boxShadow: [
                                  BoxShadow(
                                      color: const Color(0x29000000),
                                      offset: Offset(5, 3),
                                      blurRadius: 6,
                                      spreadRadius: 0)
                                ],
                              ),
                              child: loading
                                  ? Center(
                                      child: CircularProgressIndicator(),
                                    )
                                  : Center(
                                      child: Text(
                                        "SIGUIENTE",
                                        style: TextStyle(
                                          color: Color(0xffffffff),
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                    ),
                            ),
                          ),
                          SizedBox(
                            height: 40.h,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),

          //BOTTOM BAR WITH FLOATING BTN
          floatingActionButton: WhatsappButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: const BottomNav(),
        ),
      ),
    );
  }

  Future<dynamic> ShowDialogBox(BuildContext context, String TitleOfDialogBox,
      String Description, bool haveTwoButtosns, bool haveOneButtons) {
    return showDialog(
        context: context,
        builder: (context) {
          /// DIALOG BOX
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6.r),
            ),
            child: Container(
              width: 375.w,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: const Color(0xffffffff),
                boxShadow: const [
                  BoxShadow(
                      color: Color(0x7c000000),
                      offset: Offset(5, -3),
                      blurRadius: 6,
                      spreadRadius: 0)
                ],
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 10.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                            },
                            icon: Icon(
                              Icons.close_outlined,
                              size: 30.w,
                            )),
                      ],
                    ),
                    SvgPicture.asset('assets/images/agendaDialoge.svg'),
                    SizedBox(
                      height: 30.h,
                    ),
                    Text(
                      TitleOfDialogBox,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: const Color(0xff222b45),
                        fontSize: 17.sp,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                    SizedBox(
                      height: 20.h,
                    ),
                    Text(Description,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: const Color(0xff414040),
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w300,
                          fontStyle: FontStyle.normal,
                        )),
                    SizedBox(
                      height: 12.h,
                    ),
                    haveTwoButtosns
                        ? Twobuttons()
                        : SizedBox(
                            height: 5.h,
                          ),
                    haveOneButtons
                        ? Onebuttons()
                        : SizedBox(
                            height: 5.h,
                          ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Row Twobuttons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 117.w,
          height: 35.w,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: const Color(0xffF5FAFC),
              style: BorderStyle.solid,
            ),
            borderRadius: BorderRadius.circular(38.0.r),
            color: const Color(0xff009ade),
            boxShadow: const [
              BoxShadow(
                color: Color(0x29000000),
                offset: Offset(5, 3),
                blurRadius: 6,
                spreadRadius: 0,
              ),
            ],
          ),
          child: TextButton(
            //THIS WILL REDIRECT TO NEXT PAGE
            onPressed: () {},
            child: Text(
              "CALIFICAR",
              style: TextStyle(
                color: const Color(0xffffffff),
                fontSize: 12.sp,
                fontWeight: FontWeight.w800,
                fontStyle: FontStyle.normal,
              ),
            ),
          ),
        ),
        SizedBox(
          width: 20.w,
        ),
        Container(
          width: 117.w,
          height: 35.w,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: const Color(0xffF5FAFC),
              style: BorderStyle.solid,
            ),
            borderRadius: BorderRadius.circular(38.0.r),
            color: const Color(0xffffca51),
            boxShadow: const [
              BoxShadow(
                color: Color(0x29000000),
                offset: Offset(5, 3),
                blurRadius: 6,
                spreadRadius: 0,
              ),
            ],
          ),
          child: TextButton(
            //THIS WILL REDIRECT TO NEXT PAGE
            onPressed: () {},
            child: Text(
              "INICIO",
              style: TextStyle(
                color: const Color(0xffffffff),
                fontSize: 12.sp,
                fontWeight: FontWeight.w800,
                fontStyle: FontStyle.normal,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

Row Onebuttons() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Container(
        width: 212.w,
        height: 49.w,
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: const Color(0xffF5FAFC),
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(38.0.r),
          color: const Color(0xff009ade),
          boxShadow: const [
            BoxShadow(
              color: Color(0x29000000),
              offset: Offset(5, 3),
              blurRadius: 6,
              spreadRadius: 0,
            ),
          ],
        ),
        child: TextButton(
          //THIS WILL REDIRECT TO NEXT PAGE
          onPressed: () {},
          child: Text(
            "Buscar",
            style: TextStyle(
              color: const Color(0xffffffff),
              fontSize: 16.sp,
              fontWeight: FontWeight.w800,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
    ],
  );
}
