import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/material.dart';
import 'package:homie/client.dart';
import 'package:homie/models/presupuesto/item_presupuesto.dart';
import 'package:homie/models/presupuesto/presupuesto.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/presupuesto/presupuesto.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:homie/screens/presupuesto/presupuesto_two.dart';
import 'package:intl/intl.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class PresupuestoDetalle extends StatefulWidget {
  const PresupuestoDetalle({Key? key}) : super(key: key);

  @override
  _PresupuestoDetalleState createState() => _PresupuestoDetalleState();
}

class _PresupuestoDetalleState extends State<PresupuestoDetalle> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  late Presupuesto presupuesto;

  String getTotal() {
    double total = presupuesto.costo!.toDouble();
    for (var i = 0; i < presupuesto.itemPresupuestos!.length; i++) {
      total += presupuesto.itemPresupuestos![i].precio!.toDouble() *
          presupuesto.itemPresupuestos![i].cantidad!.toDouble();
    }
    return total.toStringAsFixed(2);
  }

  String getTotalWithDiscount() {
    double total = presupuesto.costo!.toDouble();
    for (var i = 0; i < presupuesto.itemPresupuestos!.length; i++) {
      total += presupuesto.itemPresupuestos![i].precio!.toDouble() *
          presupuesto.itemPresupuestos![i].cantidad!.toDouble();
    }
    total -= presupuesto.visitaTecnica!.pagado!.total!.toDouble();
    total < 0 ? total = 0 : total;
    return total.toStringAsFixed(2);
  }

  void aceptar() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => const PresupestoTwo(),
          settings: RouteSettings(arguments: presupuesto)),
    );
  }

  void rechazar() async {
    await Client().updatePresupuesto({
      "id": presupuesto.id,
      "status": "Rechazada",
    });
    ShowDialogBox(
        context,
        'Rechazaste el presupuesto',
        'Lamentamos que hayas rechazado el presupuesto que te enviamos. Pero no te preocupes, siempre puedes agendas la visita de otro técnico',
        false,
        true);
  }

  @override
  Widget build(BuildContext context) {
    presupuesto = ModalRoute.of(context)!.settings.arguments as Presupuesto;
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [Color(0xff83dcff), Color(0xffeffdff)],
          stops: [0, 1],
          begin: Alignment(-1.00, -0.02),
          end: Alignment(1.00, 0.02),
        ),
      ),
      child: Scaffold(
        key: _globalKey,
        endDrawer: Navbar(),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.black),
            //NAVIGATING BACK TO THE HOMEPAGE
            onPressed: () => Navigator.of(context).pop(),
          ),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20.0.w),
              child: GestureDetector(
                //THIS FUNCTION WILL OPEN MENU DRAWER
                onTap: () {
                  _globalKey.currentState?.openEndDrawer();
                },
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: 58.41377258300781.w,
                      height: 57.504638671875.w,
                      decoration: const BoxDecoration(
                        color: Color(0xff009ade),
                        shape: BoxShape.circle,
                      ),
                    ),
                    SvgPicture.asset('assets/images/menu-icon.svg'),
                  ],
                ),
              ),
            ),
          ],
          toolbarHeight: 80.h,
        ),
        backgroundColor: Colors.transparent,
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
              padding: EdgeInsets.only(left: 16.0.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Presupuesto",
                      style: TextStyle(
                        color: Color(0xff00013c),
                        fontSize: 23.sp,
                        fontWeight: FontWeight.w900,
                        fontStyle: FontStyle.normal,
                      )),
                  Text("Orden Relacionada: #${presupuesto.visitaTecnicaId}",
                      style: TextStyle(
                        color: Color(0xff252525),
                        fontSize: 15.sp,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        letterSpacing: -0.45,
                      )),
                ],
              )),
          SizedBox(
            height: 24.h,
          ),
          Expanded(
            child: Container(
              width: 375.w,
              decoration: BoxDecoration(
                color: Color(0xffffffff),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25)),
                boxShadow: [
                  BoxShadow(
                      color: Color(0x7c000000),
                      offset: Offset(5, -3),
                      blurRadius: 6,
                      spreadRadius: 0)
                ],
              ),
              child: Column(children: [
                Expanded(
                    child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Expanded(
                        flex: 0,
                        child: Container(
                          width: 340.w,
                          decoration: new BoxDecoration(
                              color: Color(0xffE5F5FC),
                              borderRadius: BorderRadius.circular(30.r)),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 23.0.w, vertical: 20.h),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Descripción",
                                    style: TextStyle(
                                      color: Color(0xff222b45),
                                      fontSize: 17.sp,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                    )),
                                SizedBox(
                                  height: 8.w,
                                ),
                                Text("${presupuesto.descripcion}",
                                    style: TextStyle(
                                      color: Color(0xff8f9bb3),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 12.h,
                      ),
                      Container(
                          width: 343.w,
                          decoration: new BoxDecoration(
                            color: Color(0xffffffff),
                            borderRadius: BorderRadius.circular(12),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0x29000000),
                                  offset: Offset(0, 3),
                                  blurRadius: 6,
                                  spreadRadius: 0)
                            ],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 15.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text("Detalle",
                                    style: TextStyle(
                                      color: Color(0xff222b45),
                                      fontSize: 25.sp,
                                      fontWeight: FontWeight.w600,
                                      fontStyle: FontStyle.normal,
                                    )),
                                SizedBox(
                                  height: 10.w,
                                ),
                                Divider(
                                  color: Color(0xffedf1f7),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text("Piezas y Partes",
                                          style: TextStyle(
                                            color: Color(0xff101010),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                          )),
                                    ],
                                  ),
                                ),
                                ListView.builder(
                                  shrinkWrap: true,
                                  itemCount:
                                      presupuesto.itemPresupuestos!.length,
                                  itemBuilder: (context, i) {
                                    ItemPresupuesto itemPresupuesto =
                                        presupuesto.itemPresupuestos![i];
                                    return Column(
                                      children: [
                                        Divider(
                                          color: Color(0xffedf1f7),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 15.0, right: 10),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                  "${itemPresupuesto.cantidad} ${itemPresupuesto.nombre}",
                                                  style: TextStyle(
                                                    color: Color(0xff8f9bb3),
                                                    fontSize: 15.sp,
                                                    fontWeight: FontWeight.w400,
                                                    fontStyle: FontStyle.normal,
                                                  )),
                                              Text(
                                                  "\$${itemPresupuesto.precio}",
                                                  style: TextStyle(
                                                    color: Color(0xff222b45),
                                                    fontSize: 15.sp,
                                                    fontWeight: FontWeight.w400,
                                                    fontStyle: FontStyle.normal,
                                                  )),
                                            ],
                                          ),
                                        ),
                                        Divider(
                                          color: Color(0xffedf1f7),
                                        ),
                                      ],
                                    );
                                  },
                                ),
                                Divider(
                                  color: Color(0xffedf1f7),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Mano de obra",
                                          style: TextStyle(
                                            color: Color(0xff101010),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w700,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text(
                                        "\$${presupuesto.costo}",
                                        style: TextStyle(
                                          color: Color(0xff222b45),
                                          fontSize: 15.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 20.w,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Descuento visita",
                                          style: TextStyle(
                                            color: Color(0xff222b45),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w600,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Text(
                                          "-\$${presupuesto.visitaTecnica!.pagado!.total}",
                                          style: TextStyle(
                                            color: Color(0xff007aff),
                                            fontSize: 15.sp,
                                            fontWeight: FontWeight.w600,
                                            fontStyle: FontStyle.normal,
                                          ))
                                    ],
                                  ),
                                ),
                                Divider(
                                  color: Color(0xffedf1f7),
                                ),
                                SizedBox(
                                  height: 20.w,
                                ),
                                Divider(
                                  color: Color(0xffedf1f7),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "COSTO TOTAL",
                                        style: TextStyle(
                                          color: Color(0xff222b45),
                                          fontSize: 15.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                      RichText(
                                          text: new TextSpan(children: [
                                        new TextSpan(
                                            text: "\$",
                                            style: TextStyle(
                                              color: Color(0xff000000),
                                              fontSize: 17.sp,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                        new TextSpan(
                                            text: getTotal(),
                                            style: TextStyle(
                                              color: Color(0xff000000),
                                              fontSize: 18.sp,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                      ])),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "RESTO A PAGAR",
                                        style: TextStyle(
                                          color: Color(0xff222b45),
                                          fontSize: 15.sp,
                                          fontWeight: FontWeight.w400,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                      RichText(
                                          text: new TextSpan(children: [
                                        new TextSpan(
                                            text: "\$",
                                            style: TextStyle(
                                              color: Color(0xff000000),
                                              fontSize: 17.sp,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                        new TextSpan(
                                            text: getTotalWithDiscount(),
                                            style: TextStyle(
                                              color: Color(0xff000000),
                                              fontSize: 18.sp,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                            )),
                                      ])),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )),
                      SizedBox(
                        height: 40.h,
                      ),
                      //BUTTTONs
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 160.w,
                            height: 49.w,
                            decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: Color(0xffF5FAFC),
                                style: BorderStyle.solid,
                              ),
                              borderRadius: BorderRadius.circular(38.0.r),
                              color: const Color(0xff21409a),
                              boxShadow: const [
                                BoxShadow(
                                  color: Color(0x29000000),
                                  offset: Offset(5, 3),
                                  blurRadius: 6,
                                  spreadRadius: 0,
                                ),
                              ],
                            ),
                            child: TextButton(
                              //THIS WILL REDIRECT TO Presupesto Two
                              onPressed: aceptar,
                              child: Text(
                                "ACEPTAR",
                                style: TextStyle(
                                  color: Color(0xffffffff),
                                  fontSize: 15.sp,
                                  fontWeight: FontWeight.w800,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 20.w,
                          ),
                          Container(
                            width: 160.w,
                            height: 49.w,
                            decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: Color(0xffF5FAFC),
                                style: BorderStyle.solid,
                              ),
                              borderRadius: BorderRadius.circular(38.0.r),
                              color: Color(0xffffca51),
                              boxShadow: const [
                                BoxShadow(
                                  color: Color(0x29000000),
                                  offset: Offset(5, 3),
                                  blurRadius: 6,
                                  spreadRadius: 0,
                                ),
                              ],
                            ),
                            child: TextButton(
                              //THIS WILL REDIRECT TO NEXT PAGE
                              onPressed: rechazar,
                              child: Text(
                                "RECHAZAR",
                                style: TextStyle(
                                  color: Color(0xffffffff),
                                  fontSize: 15.sp,
                                  fontWeight: FontWeight.w800,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 50.w,
                      ),
                    ],
                  ),
                ))
              ]),
            ),
          ),
        ]),

        //BOTTOM BAR WITH FLOATING BTN
        floatingActionButton: WhatsappButton(),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        bottomNavigationBar: const BottomNav(),
      ),
    );
  }
}
