import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:homie/client.dart';
import 'package:homie/models/notificacion_model.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/busqueda_y_agenda/preview_orden.dart';
import 'package:homie/screens/citas/citas_agendadas.dart';
import 'package:homie/screens/notificaciones/notificacion.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class Notification1 extends StatefulWidget {
  const Notification1({Key? key}) : super(key: key);

  @override
  _CitasMainScreenState createState() => _CitasMainScreenState();
}

class _CitasMainScreenState extends State<Notification1> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  String NotificationTitle = 'Notificaciones';
  String notiMsg = 'Ver Mensaje';

  List<NotificacionModel> notificaciones = List.empty();

  void getNotifications() async {
    await Client().readNotifications();
    notificaciones = await Client().getNotificaciones();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getNotifications();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
        colors: [Color(0xff83dcff), Color(0xffeffdff)],
        stops: [0, 1],
        begin: Alignment(-1.00, -0.02),
        end: Alignment(1.00, 0.02),
      )),
      child: SafeArea(
        child: Scaffold(
          key: _globalKey,
          endDrawer: Navbar(),
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              //NAVIGATING BACK TO THE HOMEPAGE
              onPressed: () => Navigator.of(context).pop(),
            ),
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 20.0.w),
                child: GestureDetector(
                  //THIS FUNCTION WILL OPEN MENU DRAWER
                  onTap: () {
                    _globalKey.currentState?.openEndDrawer();
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 58.41377258300781.w,
                        height: 57.504638671875.w,
                        decoration: const BoxDecoration(
                          color: Color(0xff009ade),
                          shape: BoxShape.circle,
                        ),
                      ),
                      SvgPicture.asset('assets/images/menu-icon.svg'),
                    ],
                  ),
                ),
              ),
            ],
            toolbarHeight: 120.h,
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 16.0.w),
                child: Text(NotificationTitle,
                    style: TextStyle(
                      color: const Color(0xff00013c),
                      fontSize: 23.sp,
                      fontWeight: FontWeight.w900,
                      fontStyle: FontStyle.normal,
                    )),
              ),
              SizedBox(
                height: 24.h,
              ),
              Expanded(
                child: Container(
                  width: 375.w,
                  decoration: const BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0x7c000000),
                          offset: Offset(5, -3),
                          blurRadius: 6,
                          spreadRadius: 0)
                    ],
                  ),
                  child: Container(
                    margin: EdgeInsets.fromLTRB(15.w, 15.h, 15.w, 40.h),
                    width: 143.w,
                    height: 100.h,
                    decoration: BoxDecoration(
                      color: const Color(0xffffffff),
                      borderRadius: BorderRadius.circular(16),
                      boxShadow: const [
                        BoxShadow(
                          color: Color(0x29000000),
                          offset: Offset(0, 3),
                          blurRadius: 6,
                          spreadRadius: 0,
                        ),
                      ],
                    ),
                    child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ListView.builder(
                            itemCount: notificaciones.length,
                            itemBuilder: (context, index) {
                              NotificacionModel notificacion =
                                  notificaciones[index];
                              return GestureDetector(
                                onTap: () {
                                  setState(() {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                Notification2(),
                                            settings: RouteSettings(
                                                arguments: notificacion)));
                                  });
                                },
                                child: Container(
                                  width: 335.w,
                                  height: 80.h,
                                  decoration: BoxDecoration(
                                    color: const Color(0xffffffff),
                                    borderRadius: BorderRadius.circular(7.r),
                                    boxShadow: const [
                                      BoxShadow(
                                        color: Color(0xb3eaecf3),
                                        offset: Offset(0, 10),
                                        blurRadius: 20,
                                        spreadRadius: 0,
                                      )
                                    ],
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(
                                        20.w, 15.h, 0.w, 0.h),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                            width: 50.w,
                                            height: 50.h,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(100),
                                                image: DecorationImage(
                                                    image: NetworkImage(
                                                        notificacion.image
                                                            .toString())),
                                                color:
                                                    const Color(0xffeaecf3))),
                                        SizedBox(
                                          width: 20.w,
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 10.h,
                                            ),
                                            Text(
                                                notificacion.heading
                                                    .toString(), //User title variable
                                                style: TextStyle(
                                                  color:
                                                      const Color(0xff413d4a),
                                                  fontSize: 12.sp,
                                                  fontWeight: FontWeight.w900,
                                                  fontStyle: FontStyle.normal,
                                                )),
                                            SizedBox(
                                              height: 5.h,
                                            ),
                                            Text(
                                              notiMsg, //Notification msg variable
                                              style: TextStyle(
                                                color: const Color(0xffa7a7a7),
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w600,
                                                fontStyle: FontStyle.normal,
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            })),
                  ),
                ),
              )
            ],
          ),
          //BOTTOM BAR WITH FLOATING BTN
          floatingActionButton: WhatsappButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: const BottomNav(),
          //Bottom bar ended
        ),
      ),
    );
  }
}
