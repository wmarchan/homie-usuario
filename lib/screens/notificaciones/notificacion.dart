import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:homie/client.dart';
import 'package:homie/models/notificacion_model.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class Notification2 extends StatefulWidget {
  const Notification2({Key? key}) : super(key: key);

  @override
  _CitasMainScreenState createState() => _CitasMainScreenState();
}

class _CitasMainScreenState extends State<Notification2> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  // ignore: non_constant_identifier_names
  String NotificationTitle = 'Notificaciones';
  late NotificacionModel notificacionModel;
  late Map data;

  @override
  Widget build(BuildContext context) {
    notificacionModel =
        ModalRoute.of(context)!.settings.arguments as NotificacionModel;
    data = jsonDecode(notificacionModel.data.toString());
    print(data);
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: BoxDecoration(
          // ignore: prefer_const_constructors
          gradient: LinearGradient(
        colors: [Color(0xff83dcff), Color(0xffeffdff)],
        stops: [0, 1],
        begin: Alignment(-1.00, -0.02),
        end: Alignment(1.00, 0.02),
      )),
      child: SafeArea(
        child: Scaffold(
          key: _globalKey,
          endDrawer: Navbar(),
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              //NAVIGATING BACK TO THE HOMEPAGE
              onPressed: () => Navigator.of(context).pop(),
            ),
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 20.0.w),
                child: GestureDetector(
                  //THIS FUNCTION WILL OPEN MENU DRAWER
                  onTap: () {
                    _globalKey.currentState?.openEndDrawer();
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 58.41377258300781.w,
                        height: 57.504638671875.w,
                        decoration: const BoxDecoration(
                          color: Color(0xff009ade),
                          shape: BoxShape.circle,
                        ),
                      ),
                      SvgPicture.asset('assets/images/menu-icon.svg'),
                    ],
                  ),
                ),
              ),
            ],
            toolbarHeight: 120.h,
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 16.0.w),
                child: Text(NotificationTitle,
                    style: TextStyle(
                      color: Color(0xff00013c),
                      fontSize: 23.sp,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    )),
              ),
              SizedBox(
                height: 24.h,
              ),
              Expanded(
                child: Container(
                  width: 375.w,
                  decoration: BoxDecoration(
                    color: Color(0xffffffff),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0x7c000000),
                          offset: Offset(5, -3),
                          blurRadius: 6,
                          spreadRadius: 0)
                    ],
                  ),
                  child: Container(
                    margin: EdgeInsets.fromLTRB(15, 15, 15, 40),
                    width: 143,
                    height: 100,
                    decoration: new BoxDecoration(
                      color: Color(0xffffffff),
                      borderRadius: BorderRadius.circular(16),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0x29000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                            spreadRadius: 0),
                      ],
                    ),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 25,
                          ),
                          //Main Column
                          Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Center(
                                  child: Text(
                                      notificacionModel.heading
                                          .toString(), //User name variable
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Color(0xff413d4a),
                                        fontSize: 26,
                                        fontWeight: FontWeight.w900,
                                        fontStyle: FontStyle.normal,
                                      )),
                                ),
                                SizedBox(
                                  height: 50,
                                ),
                                Text(
                                    notificacionModel.content
                                        .toString() //Notification msg variable
                                    ,
                                    style: TextStyle(
                                      color: Color(0xff413d4a),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          //BOTTOM BAR WITH FLOATING BTN
          floatingActionButton: WhatsappButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: const BottomNav(),
          //Bottom bar ended
        ),
      ),
    );
  }
}
