// ignore_for_file: non_constant_identifier_names

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:homie/client.dart';
import 'package:homie/navigation/bottom_navigation.dart';
import 'package:homie/navigation/floating_action_button.dart';
import 'package:homie/singletons/AppData.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:homie/screens/sidemenu/sidebar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dio/dio.dart';
import 'package:homie/navigation/whatsapp_button.dart';

class Perfil extends StatefulWidget {
  const Perfil({Key? key}) : super(key: key);

  @override
  _CitasMainScreenState createState() => _CitasMainScreenState();
}

class _CitasMainScreenState extends State<Perfil> {
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormBuilderState> _form = new GlobalKey<FormBuilderState>();

  bool editPassword = false;
  var focusNode = FocusNode();
  bool notificaciones = appData.usuario.user!.notificaciones ?? false;
  bool loading = false;

  void actualizar() async {
    setState(() {
      loading = true;
    });

    await Client().actualizar({
      "id": appData.usuario.user!.id,
      "nombre": _form.currentState!.fields["nombre"]!.value,
      "apellido": _form.currentState!.fields["apellido"]!.value,
      "telefono": _form.currentState!.fields["telefono"]!.value,
      "contraseña": _form.currentState!.fields["contraseña"]!.value,
      "notificaciones": notificaciones,
    });

    setState(() {
      loading = false;
    });
  }

  void updateImage() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? image = await _picker.pickImage(
        source: ImageSource.gallery,
        imageQuality: 80,
        maxWidth: 1200,
        maxHeight: 1200);

    if (image != null) {
      setState(() {
        loading = true;
      });
      await Client().actualizar(FormData.fromMap({
        "id": appData.usuario.user!.id,
        "file": await MultipartFile.fromFile(image.path, filename: image.name),
      }));
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(375, 812),
    );
    return Container(
      decoration: const BoxDecoration(
          // ignore: prefer_const_constructors
          gradient: LinearGradient(
        colors: [Color(0xff83dcff), Color(0xffeffdff)],
        stops: [0, 1],
        begin: Alignment(-1.00, -0.02),
        end: Alignment(1.00, 0.02),
      )),
      child: SafeArea(
        child: Scaffold(
          key: _globalKey,
          endDrawer: Navbar(),

          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
              //NAVIGATING BACK TO THE HOMEPAGE
              onPressed: () => Navigator.of(context).pop(),
            ),
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 20.0.w),
                child: GestureDetector(
                  //THIS FUNCTION WILL OPEN MENU DRAWER
                  onTap: () {
                    _globalKey.currentState?.openEndDrawer();
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: 58.41377258300781.w,
                        height: 57.504638671875.w,
                        decoration: const BoxDecoration(
                          color: Color(0xff009ade),
                          shape: BoxShape.circle,
                        ),
                      ),
                      SvgPicture.asset('assets/images/menu-icon.svg'),
                    ],
                  ),
                ),
              ),
            ],
            toolbarHeight: 110.h,
          ),

          body: Stack(
            children: [
              Positioned(
                  top: 60.h,
                  left: 0.w,
                  right: 0.w,
                  child: Container(
                    width: 410.w,
                    height: 652.2529296875.h,
                    decoration: BoxDecoration(
                        color: const Color(0xffffffff),
                        boxShadow: const [
                          BoxShadow(
                              color: Color(0x7c000000),
                              offset: Offset(5, -3),
                              blurRadius: 6,
                              spreadRadius: 0)
                        ],
                        borderRadius: BorderRadius.circular(30.r)),
                  )),
              Positioned.fill(
                  top: 120.h,
                  left: 15.w,
                  right: 15.w,
                  child: Container(
                    width: 360.w,
                    decoration: BoxDecoration(
                      color: const Color(0xffffffff),
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: const [
                        BoxShadow(
                            color: Color(0x29000000),
                            offset: Offset(0, 3),
                            blurRadius: 6,
                            spreadRadius: 0)
                      ],
                    ),
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        SizedBox(
                          height: 25.h,
                        ),
                        Text(
                            "${appData.usuario.user!.nombre} ${appData.usuario.user!.apellido}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xff00013c),
                              fontSize: 18.sp,
                              fontWeight: FontWeight.w900,
                              fontStyle: FontStyle.normal,
                            )),
                        FormBuilder(
                          key: _form,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                        child: FormBuilderTextField(
                                      name: "nombre",
                                      initialValue:
                                          appData.usuario.user!.nombre,
                                      validator: FormBuilderValidators.compose([
                                        FormBuilderValidators.required(context),
                                      ]),
                                      decoration: InputDecoration(
                                          border: InputBorder.none,
                                          labelText: "Nombre"),
                                    )),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                        child: FormBuilderTextField(
                                      name: "apellido",
                                      initialValue:
                                          appData.usuario.user!.apellido,
                                      validator: FormBuilderValidators.compose([
                                        FormBuilderValidators.required(context),
                                      ]),
                                      decoration: InputDecoration(
                                          border: InputBorder.none,
                                          labelText: "Apellido"),
                                    )),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                        child: FormBuilderTextField(
                                      name: "email",
                                      initialValue: appData.usuario.user!.email,
                                      readOnly: true,
                                      validator: FormBuilderValidators.compose([
                                        FormBuilderValidators.required(context),
                                        FormBuilderValidators.email(context),
                                      ]),
                                      decoration: InputDecoration(
                                          border: InputBorder.none,
                                          labelText: "Email"),
                                    )),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                        child: FormBuilderTextField(
                                      name: "telefono",
                                      initialValue:
                                          appData.usuario.user!.telefono,
                                      validator: FormBuilderValidators.compose([
                                        FormBuilderValidators.required(context),
                                      ]),
                                      decoration: InputDecoration(
                                          border: InputBorder.none,
                                          labelText: "Teléfono"),
                                    )),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: FormBuilderTextField(
                                        name: "contraseña",
                                        obscureText: true,
                                        enabled: editPassword,
                                        focusNode: focusNode,
                                        keyboardType:
                                            TextInputType.visiblePassword,
                                        validator:
                                            FormBuilderValidators.compose([
                                          FormBuilderValidators.required(
                                              context),
                                        ]),
                                        decoration: InputDecoration(
                                            border: InputBorder.none,
                                            labelText: "Contraseña"),
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        FocusScope.of(context)
                                            .requestFocus(focusNode);
                                        setState(() {
                                          editPassword = true;
                                        });
                                      },
                                      child: Text("Cambiar",
                                          style: TextStyle(
                                            color: Color(0xff00013c),
                                          )),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: FormBuilderTextField(
                                        name: "notificaciones",
                                        initialValue: notificaciones
                                            ? "Habilitado"
                                            : "Deshabilitado",
                                        decoration: InputDecoration(
                                            border: InputBorder.none,
                                            labelText:
                                                "Recibir notificaciones"),
                                      ),
                                    ),
                                    Switch(
                                        activeColor: Color(0xff00013c),
                                        value: notificaciones,
                                        onChanged: (val) {
                                          setState(() {
                                            notificaciones = val;
                                            _form.currentState?.patchValue({
                                              "notificaciones": notificaciones
                                                  ? "Habilitado"
                                                  : "Deshabilitado"
                                            });
                                          });
                                        })
                                  ],
                                ),
                                TextButton(
                                  onPressed: () => actualizar(),
                                  child: Center(
                                    child: Text(
                                      "Editar",
                                      style: TextStyle(
                                          color: Color(0xff00013c),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                Visibility(
                                  child: Center(
                                    child: CircularProgressIndicator(),
                                  ),
                                  visible: loading,
                                )
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                      ],
                    ),
                  )),
              Positioned(
                top: 0.h,
                left: 130.w,
                child: Stack(
                  children: <Widget>[
                    Opacity(
                      opacity: 0.10000000149011612,
                      child: new Container(
                          width: 117.24365234375,
                          height: 117.24365234375,
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color(0xff1a1824))),
                    ),
                    Positioned(
                      top: 10,
                      left: 10,
                      child: Container(
                          width: 97.703125,
                          height: 97.703125,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                      appData.usuario.user!.foto.toString())),
                              color: Color(0xff009ade))),
                    ),
                  ],
                ),
              ),
              Positioned(
                  top: 40.h,
                  right: 130.w,
                  child: GestureDetector(
                    onTap: updateImage,
                    child: Stack(
                      children: <Widget>[
                        Opacity(
                          opacity: 0.800000011920929,
                          child: Container(
                              width: 40.w,
                              height: 40.h,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: const Color(0xff1a1824))),
                        ),
                        Positioned(
                          top: 11.h,
                          left: 12.w,
                          child: SvgPicture.asset(
                            'assets/images/camera.svg',
                            height: 17.h,
                            width: 17.w,
                          ),
                        ),
                      ],
                    ),
                  )),
              Positioned(
                top: 15.h,
                left: 30.w,
                child: Text("Perfil",
                    style: TextStyle(
                      color: Color(0xff00013c),
                      fontSize: 23.sp,
                      fontWeight: FontWeight.w900,
                      fontStyle: FontStyle.normal,
                    )),
              )
            ],
          ),

          //BOTTOM BAR WITH FLOATING BTN
          floatingActionButton: WhatsappButton(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          bottomNavigationBar: const BottomNav(),
          //Bottom bar ended
        ),
      ),
    );
  }
}
