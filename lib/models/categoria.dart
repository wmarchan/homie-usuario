/// id : 1
/// nombre : "Salones y terrazas"
/// createdAt : "2021-11-10T17:32:35.000Z"
/// updatedAt : "2021-11-10T17:32:36.000Z"
/// Subcategoria : [{"id":1,"nombre":"Salón de eventos","createdAt":"2021-11-10T19:57:01.000Z","updatedAt":"2021-11-10T19:57:03.000Z","CategoriumId":1},{"id":2,"nombre":"Terraza","createdAt":"2021-11-10T19:57:11.000Z","updatedAt":"2021-11-10T19:57:11.000Z","CategoriumId":1},{"id":3,"nombre":"Discoteca","createdAt":"2021-11-10T19:57:29.000Z","updatedAt":"2021-11-10T19:57:29.000Z","CategoriumId":1},{"id":4,"nombre":"Salón de fiestas","createdAt":"2021-11-10T19:57:36.000Z","updatedAt":"2021-11-10T19:57:36.000Z","CategoriumId":1},{"id":5,"nombre":"Bar","createdAt":"2021-11-10T19:57:40.000Z","updatedAt":"2021-11-10T19:57:41.000Z","CategoriumId":1},{"id":6,"nombre":"Club Campestre","createdAt":"2021-11-10T19:58:00.000Z","updatedAt":"2021-11-10T19:58:00.000Z","CategoriumId":1}]

class Categoria {
  Categoria({
    int? id,
    String? nombre,
    String? createdAt,
    String? updatedAt,
    List<Subcategoria>? subcategoria,
  }) {
    _id = id;
    _nombre = nombre;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _subcategoria = subcategoria;
  }

  Categoria.fromJson(dynamic json) {
    _id = json['id'];
    _nombre = json['nombre'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    if (json['Subcategoria'] != null) {
      _subcategoria = [];
      json['Subcategoria'].forEach((v) {
        _subcategoria?.add(Subcategoria.fromJson(v));
      });
    }
  }
  int? _id;
  String? _nombre;
  String? _createdAt;
  String? _updatedAt;
  List<Subcategoria>? _subcategoria;

  int? get id => _id;
  String? get nombre => _nombre;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  List<Subcategoria>? get subcategoria => _subcategoria;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nombre'] = _nombre;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    if (_subcategoria != null) {
      map['Subcategoria'] = _subcategoria?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 1
/// nombre : "Salón de eventos"
/// createdAt : "2021-11-10T19:57:01.000Z"
/// updatedAt : "2021-11-10T19:57:03.000Z"
/// CategoriumId : 1

class Subcategoria {
  Subcategoria({
    int? id,
    String? nombre,
    String? createdAt,
    String? updatedAt,
    int? categoriumId,
  }) {
    _id = id;
    _nombre = nombre;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _categoriumId = categoriumId;
  }

  Subcategoria.fromJson(dynamic json) {
    _id = json['id'];
    _nombre = json['nombre'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _categoriumId = json['CategoriumId'];
  }
  int? _id;
  String? _nombre;
  String? _createdAt;
  String? _updatedAt;
  int? _categoriumId;

  int? get id => _id;
  String? get nombre => _nombre;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  int? get categoriumId => _categoriumId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nombre'] = _nombre;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['CategoriumId'] = _categoriumId;
    return map;
  }
}
