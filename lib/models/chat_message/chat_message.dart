import 'package:homie/models/chat/mensaje.dart';

class ChatMessage {
  int? count;
  List<Mensaje>? messages;

  ChatMessage({this.count, this.messages});

  factory ChatMessage.fromJson(Map<String, dynamic> json) => ChatMessage(
        count: json['count'] as int?,
        messages: (json['rows'] as List<dynamic>?)
            ?.map((e) => Mensaje.fromJson(e as Map<String, dynamic>))
            .toList(),
      );

  Map<String, dynamic> toJson() => {
        'count': count,
        'rows': messages?.map((e) => e.toJson()).toList(),
      };
}
