class Subcategorium {
  int? id;
  String? nombre;
  String? createdAt;
  String? updatedAt;
  int? categoriumId;

  Subcategorium({
    this.id,
    this.nombre,
    this.createdAt,
    this.updatedAt,
    this.categoriumId,
  });

  factory Subcategorium.fromJson(Map<String, dynamic> json) => Subcategorium(
        id: json['id'] as int?,
        nombre: json['nombre'] as String?,
        createdAt: json['createdAt'] as String?,
        updatedAt: json['updatedAt'] as String?,
        categoriumId: json['CategoriumId'] as int?,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'nombre': nombre,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'CategoriumId': categoriumId,
      };
}
