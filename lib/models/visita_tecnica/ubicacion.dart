class Ubicacion {
  String? type;
  List<dynamic>? coordinates;

  Ubicacion({this.type, this.coordinates});

  factory Ubicacion.fromJson(Map<String, dynamic> json) => Ubicacion(
        type: json['type'] as String?,
        coordinates: json['coordinates'],
      );

  Map<String, dynamic> toJson() => {
        'type': type,
        'coordinates': coordinates,
      };
}
