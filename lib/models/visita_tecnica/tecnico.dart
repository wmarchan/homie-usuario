class Tecnico {
  String? nombre;
  String? apellido;
  String? id;
  String? email;
  dynamic telefono;
  String? foto;

  Tecnico({
    this.nombre,
    this.apellido,
    this.id,
    this.email,
    this.telefono,
    this.foto,
  });

  factory Tecnico.fromJson(Map<String, dynamic> json) => Tecnico(
        nombre: json['nombre'] as String?,
        apellido: json['apellido'] as String?,
        id: json['id'] as String?,
        email: json['email'] as String?,
        telefono: json['telefono'] as dynamic?,
        foto: json['foto'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'nombre': nombre,
        'apellido': apellido,
        'id': id,
        'email': email,
        'telefono': telefono,
        'foto': foto,
      };
}
