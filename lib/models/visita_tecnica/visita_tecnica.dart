import 'package:homie/models/presupuesto/pago.dart';

import 'subcategorium.dart';
import 'tecnico.dart';
import 'tipo_servicio.dart';
import 'ubicacion.dart';

class VisitaTecnica {
  int? id;
  String? contacto;
  String? descripcion;
  String? direccion;
  Ubicacion? ubicacion;
  String? dia;
  String? hora;
  String? pago;
  String? status;
  String? reporte;
  String? fechaVisita;
  String? incidentes;
  bool? requiereCotizacion;
  String? createdAt;
  String? updatedAt;
  int? subcategoriumId;
  int? tipoServicioId;
  String? usuarioId;
  String? tecnicoId;
  Subcategorium? subcategorium;
  TipoServicio? tipoServicio;
  Tecnico? tecnico;
  Pago? pagado;

  VisitaTecnica(
      {this.id,
      this.contacto,
      this.descripcion,
      this.direccion,
      this.ubicacion,
      this.dia,
      this.hora,
      this.pago,
      this.status,
      this.reporte,
      this.fechaVisita,
      this.incidentes,
      this.requiereCotizacion,
      this.createdAt,
      this.updatedAt,
      this.subcategoriumId,
      this.tipoServicioId,
      this.usuarioId,
      this.tecnicoId,
      this.subcategorium,
      this.tipoServicio,
      this.tecnico,
      this.pagado});

  factory VisitaTecnica.fromJson(Map<String, dynamic> json) => VisitaTecnica(
        id: json['id'] as int?,
        contacto: json['contacto'] as String?,
        descripcion: json['descripcion'] as String?,
        direccion: json['direccion'] as String?,
        ubicacion: json['ubicacion'] == null
            ? null
            : Ubicacion.fromJson(json['ubicacion'] as Map<String, dynamic>),
        dia: json['dia'] as String?,
        hora: json['hora'] as String?,
        pago: json['pago'] as String?,
        status: json['status'] as String?,
        reporte: json['reporte'] as String?,
        fechaVisita: json['fechaVisita'] as String?,
        incidentes: json['incidentes'] as String?,
        requiereCotizacion: json['requiereCotizacion'] as bool?,
        createdAt: json['createdAt'] as String?,
        updatedAt: json['updatedAt'] as String?,
        subcategoriumId: json['SubcategoriumId'] as int?,
        tipoServicioId: json['TipoServicioId'] as int?,
        usuarioId: json['UsuarioId'] as String?,
        tecnicoId: json['TecnicoId'] as String?,
        subcategorium: json['Subcategorium'] == null
            ? null
            : Subcategorium.fromJson(
                json['Subcategorium'] as Map<String, dynamic>),
        tipoServicio: json['TipoServicio'] == null
            ? null
            : TipoServicio.fromJson(
                json['TipoServicio'] as Map<String, dynamic>),
        tecnico: json['Tecnico'] == null
            ? null
            : Tecnico.fromJson(json['Tecnico'] as Map<String, dynamic>),
        pagado: json['Pago'] == null
            ? null
            : Pago.fromJson(json['Pago'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'contacto': contacto,
        'descripcion': descripcion,
        'direccion': direccion,
        'ubicacion': ubicacion?.toJson(),
        'dia': dia,
        'hora': hora,
        'pago': pago,
        'status': status,
        'reporte': reporte,
        'fechaVisita': fechaVisita,
        'incidentes': incidentes,
        'requiereCotizacion': requiereCotizacion,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'SubcategoriumId': subcategoriumId,
        'TipoServicioId': tipoServicioId,
        'UsuarioId': usuarioId,
        'TecnicoId': tecnicoId,
        'Subcategorium': subcategorium?.toJson(),
        'TipoServicio': tipoServicio?.toJson(),
        'Tecnico': tecnico?.toJson(),
      };
}
