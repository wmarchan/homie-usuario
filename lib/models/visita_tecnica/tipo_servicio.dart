class TipoServicio {
  int? id;
  String? nombre;
  String? createdAt;
  String? updatedAt;

  TipoServicio({this.id, this.nombre, this.createdAt, this.updatedAt});

  factory TipoServicio.fromJson(Map<String, dynamic> json) => TipoServicio(
        id: json['id'] as int?,
        nombre: json['nombre'] as String?,
        createdAt: json['createdAt'] as String?,
        updatedAt: json['updatedAt'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'nombre': nombre,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
      };
}
