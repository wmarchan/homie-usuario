import 'package:homie/models/chat/usuario.dart';

import 'mensaje.dart';

class Chat {
  int? id;
  String? createdAt;
  String? updatedAt;
  String? usuario1Id;
  String? usuario2Id;
  dynamic usuarioId;
  List<Mensaje>? mensajes;
  Usuario? usuario1;
  Usuario? usuario2;

  Chat({
    this.id,
    this.createdAt,
    this.updatedAt,
    this.usuario1Id,
    this.usuario2Id,
    this.usuarioId,
    this.mensajes,
    this.usuario1,
    this.usuario2,
  });

  factory Chat.fromJson(Map<String, dynamic> json) => Chat(
        id: json['id'] as int?,
        createdAt: json['createdAt'] as String?,
        updatedAt: json['updatedAt'] as String?,
        usuario1Id: json['Usuario1Id'] as String?,
        usuario2Id: json['Usuario2Id'] as String?,
        usuarioId: json['UsuarioId'] as dynamic?,
        mensajes: (json['Mensajes'] as List<dynamic>?)
            ?.map((e) => Mensaje.fromJson(e as Map<String, dynamic>))
            .toList(),
        usuario1: json['Usuario1'] == null
            ? null
            : Usuario.fromJson(json['Usuario1'] as Map<String, dynamic>),
        usuario2: json['Usuario2'] == null
            ? null
            : Usuario.fromJson(json['Usuario2'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'Usuario1Id': usuario1Id,
        'Usuario2Id': usuario2Id,
        'UsuarioId': usuarioId,
        'Mensajes': mensajes?.map((e) => e.toJson()).toList(),
        'Usuario1': usuario1?.toJson(),
        'Usuario2': usuario2?.toJson(),
      };
}
