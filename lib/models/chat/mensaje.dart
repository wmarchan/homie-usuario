class Mensaje {
  int? id;
  String? contenido;
  String? tipo;
  String? createdAt;
  String? updatedAt;
  int? chatId;
  String? usuarioId;

  Mensaje(
      {this.id,
      this.contenido,
      this.tipo,
      this.createdAt,
      this.updatedAt,
      this.chatId,
      this.usuarioId});

  factory Mensaje.fromJson(Map<String, dynamic> json) => Mensaje(
        id: json['id'] as int?,
        contenido: json['contenido'] as String?,
        tipo: json['tipo'] as String?,
        createdAt: json['createdAt'] as String?,
        updatedAt: json['updatedAt'] as String?,
        chatId: json['ChatId'] as int?,
        usuarioId: json['UsuarioId'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'contenido': contenido,
        'tipo': tipo,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'ChatId': chatId,
        'UsuarioId': usuarioId,
      };
}
