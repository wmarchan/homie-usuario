class Usuario {
  String? id;
  String? nombre;
  String? apellido;
  String? email;
  String? foto;

  Usuario({
    this.id,
    this.nombre,
    this.apellido,
    this.email,
    this.foto,
  });

  factory Usuario.fromJson(Map<String, dynamic> json) => Usuario(
        id: json['id'] as String?,
        nombre: json['nombre'] as String?,
        apellido: json['apellido'] as String?,
        email: json['email'] as String?,
        foto: json['foto'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'nombre': nombre,
        'apellido': apellido,
        'email': email,
        'foto': foto,
      };
}
