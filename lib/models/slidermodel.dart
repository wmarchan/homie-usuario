// ignore_for_file: unnecessary_new

import 'package:flutter/material.dart';

class SliderModel {
  String? image;
  String? title;
  String? description;

// Constructor for variables
  SliderModel({this.title, this.description, this.image});

  void setImage(String getImage) {
    image = getImage;
  }

  void setTitle(String getTitle) {
    title = getTitle;
  }

  void setDescription(String getDescription) {
    description = getDescription;
  }

  String? getImage() {
    return image;
  }

  String? getTitle() {
    return title;
  }

  String? getDescription() {
    return description;
  }
}

// List created
List<SliderModel> getSlides() {
// ignore: prefer_collection_literals, deprecated_member_use
  List<SliderModel> slides = List<SliderModel>.empty(growable: true);
// ignore: unnecessary_new
  SliderModel sliderModel = new SliderModel();

// Item 1
  sliderModel.setImage("assets/images/onboarding-1.png");
  sliderModel.setTitle("Busque y Agende");
  sliderModel.setDescription(
      "Los mejores técnicos en un solo lugar trabajando para ti");
  slides.add(sliderModel);

  sliderModel = new SliderModel();

// Item 2
  sliderModel.setImage("assets/images/onboarding-2.png");
  sliderModel.setTitle("Presupuesto y Pagos");
  sliderModel.setDescription(
      "Presupuestos y pagos de los servicios desde la comodidad de su hogar, contécnicos certificados y calidad  garantizada.");
  slides.add(sliderModel);

  sliderModel = new SliderModel();

// Item 3
  sliderModel.setImage("assets/images/onboarding-3.png");
  sliderModel.setTitle("Califique su servicio");
  sliderModel.setDescription(
      "Cuéntele a la comunidad la calidad del servicio recibido, para que tanto técnicos como usuarios mejoremos continuamente.");
  slides.add(sliderModel);

  sliderModel = new SliderModel();
  return slides;
}
