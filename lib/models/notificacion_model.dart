/// id : 1
/// content : "test"
/// heading : "test"
/// subtitle : "test"
/// image : "https://images.ctfassets.net/buvy887680uc/6AwYOb6X8nWJa5mLvbFxt6/e7d255445a3c62a34004737307d1674a/top-web_cintillo-primera-compra-enviogratis__1_.jpg"
/// createdAt : "2021-11-12T15:38:19.000Z"
/// updatedAt : "2021-11-12T15:38:19.000Z"
/// UsuarioId : "0ff5b673-8d4d-49f2-afeb-0099108e7f57"

class NotificacionModel {
  NotificacionModel({
    int? id,
    String? content,
    String? heading,
    String? subtitle,
    String? image,
    String? createdAt,
    String? updatedAt,
    String? usuarioId,
    String? data,
  }) {
    _id = id;
    _content = content;
    _heading = heading;
    _subtitle = subtitle;
    _image = image;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _usuarioId = usuarioId;
    _data = data;
  }

  NotificacionModel.fromJson(dynamic json) {
    _id = json['id'];
    _content = json['content'];
    _heading = json['heading'];
    _subtitle = json['subtitle'];
    _image = json['image'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _usuarioId = json['UsuarioId'];
    _data = json['data'];
  }
  int? _id;
  String? _content;
  String? _heading;
  String? _subtitle;
  String? _image;
  String? _createdAt;
  String? _updatedAt;
  String? _usuarioId;
  String? _data;

  int? get id => _id;
  String? get content => _content;
  String? get heading => _heading;
  String? get subtitle => _subtitle;
  String? get image => _image;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  String? get usuarioId => _usuarioId;
  String? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['content'] = _content;
    map['heading'] = _heading;
    map['subtitle'] = _subtitle;
    map['image'] = _image;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['UsuarioId'] = _usuarioId;
    map['data'] = _data;
    return map;
  }
}
