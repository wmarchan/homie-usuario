import 'package:homie/models/visita_tecnica/visita_tecnica.dart';

import 'item_presupuesto.dart';
import 'tecnico.dart';

class Presupuesto {
  int? id;
  String? descripcion;
  String? tiempoEstimadoPartes;
  String? tiempoEstimadoTrabajo;
  int? costo;
  String? fecha;
  String? hora;
  String? status;
  String? reporte;
  String? fechaVisita;
  String? incidentes;
  String? createdAt;
  String? updatedAt;
  int? visitaTecnicaId;
  String? usuarioId;
  String? tecnicoId;
  List<ItemPresupuesto>? itemPresupuestos;
  VisitaTecnica? visitaTecnica;
  Tecnico? tecnico;

  Presupuesto({
    this.id,
    this.descripcion,
    this.tiempoEstimadoPartes,
    this.tiempoEstimadoTrabajo,
    this.costo,
    this.fecha,
    this.hora,
    this.status,
    this.reporte,
    this.fechaVisita,
    this.incidentes,
    this.createdAt,
    this.updatedAt,
    this.visitaTecnicaId,
    this.usuarioId,
    this.tecnicoId,
    this.itemPresupuestos,
    this.visitaTecnica,
    this.tecnico,
  });

  factory Presupuesto.fromJson(Map<String, dynamic> json) => Presupuesto(
        id: json['id'] as int?,
        descripcion: json['descripcion'] as String?,
        tiempoEstimadoPartes: json['tiempoEstimadoPartes'] as String?,
        tiempoEstimadoTrabajo: json['tiempoEstimadoTrabajo'] as String?,
        costo: json['costo'] as int?,
        fecha: json['fecha'] as String?,
        hora: json['hora'] as String?,
        status: json['status'] as String?,
        reporte: json['reporte'] as String?,
        fechaVisita: json['fechaVisita'] as String?,
        incidentes: json['incidentes'] as String?,
        createdAt: json['createdAt'] as String?,
        updatedAt: json['updatedAt'] as String?,
        visitaTecnicaId: json['VisitaTecnicaId'] as int?,
        usuarioId: json['UsuarioId'] as String?,
        tecnicoId: json['TecnicoId'] as String?,
        itemPresupuestos: (json['ItemPresupuestos'] as List<dynamic>?)
            ?.map((e) => ItemPresupuesto.fromJson(e as Map<String, dynamic>))
            .toList(),
        visitaTecnica: json['VisitaTecnica'] == null
            ? null
            : VisitaTecnica.fromJson(
                json['VisitaTecnica'] as Map<String, dynamic>),
        tecnico: json['Tecnico'] == null
            ? null
            : Tecnico.fromJson(json['Tecnico'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'descripcion': descripcion,
        'tiempoEstimadoPartes': tiempoEstimadoPartes,
        'tiempoEstimadoTrabajo': tiempoEstimadoTrabajo,
        'costo': costo,
        'fecha': fecha,
        'hora': hora,
        'status': status,
        'reporte': reporte,
        'fechaVisita': fechaVisita,
        'incidentes': incidentes,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'VisitaTecnicaId': visitaTecnicaId,
        'UsuarioId': usuarioId,
        'TecnicoId': tecnicoId,
        'ItemPresupuestos': itemPresupuestos?.map((e) => e.toJson()).toList(),
        'VisitaTecnica': visitaTecnica?.toJson(),
        'Tecnico': tecnico?.toJson(),
      };
}
