class Pago {
  int? total;

  Pago({this.total});

  factory Pago.fromJson(Map<String, dynamic> json) => Pago(
        total: json['total'] as int?,
      );

  Map<String, dynamic> toJson() => {
        'total': total,
      };
}
