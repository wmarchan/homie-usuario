class ItemPresupuesto {
  int? id;
  String? nombre;
  int? precio;
  int? cantidad;
  String? createdAt;
  String? updatedAt;
  int? presupuestoId;

  ItemPresupuesto({
    this.id,
    this.nombre,
    this.precio,
    this.cantidad,
    this.createdAt,
    this.updatedAt,
    this.presupuestoId,
  });

  factory ItemPresupuesto.fromJson(Map<String, dynamic> json) {
    return ItemPresupuesto(
      id: json['id'] as int?,
      nombre: json['nombre'] as String?,
      precio: json['precio'] as int?,
      cantidad: json['cantidad'] as int?,
      createdAt: json['createdAt'] as String?,
      updatedAt: json['updatedAt'] as String?,
      presupuestoId: json['PresupuestoId'] as int?,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'nombre': nombre,
        'precio': precio,
        'cantidad': cantidad,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'PresupuestoId': presupuestoId,
      };
}
