import 'package:homie/models/presupuesto/presupuesto.dart';
import 'package:homie/models/visita_tecnica/visita_tecnica.dart';
import 'package:homie/screens/presupuesto/presupuesto_two.dart';

class Pago {
  int? id;
  int? total;
  String? createdAt;
  String? updatedAt;
  String? usuarioId;
  int? visitaTecnicaId;
  VisitaTecnica? visitaTecnica;
  Presupuesto? presupuesto;

  Pago(
      {this.id,
      this.total,
      this.createdAt,
      this.updatedAt,
      this.usuarioId,
      this.visitaTecnicaId,
      this.visitaTecnica,
      this.presupuesto});

  @override
  String toString() {
    return 'Pago(id: $id, total: $total, createdAt: $createdAt, updatedAt: $updatedAt, usuarioId: $usuarioId, visitaTecnicaId: $visitaTecnicaId, visitaTecnica: $visitaTecnica)';
  }

  factory Pago.fromJson(Map<String, dynamic> json) {
    return Pago(
      id: json['id'] as int?,
      total: json['total'] as int?,
      createdAt: json['createdAt'] as String?,
      updatedAt: json['updatedAt'] as String?,
      usuarioId: json['UsuarioId'] as String?,
      visitaTecnicaId: json['VisitaTecnicaId'] as int?,
      visitaTecnica: json['VisitaTecnica'] == null
          ? null
          : VisitaTecnica.fromJson(
              json['VisitaTecnica'] as Map<String, dynamic>),
      presupuesto: json['Presupuesto'] == null
          ? null
          : Presupuesto.fromJson(json['Presupuesto'] as Map<String, dynamic>),
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'total': total,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'UsuarioId': usuarioId,
        'VisitaTecnicaId': visitaTecnicaId,
        'VisitaTecnica': visitaTecnica?.toJson(),
        'Presupuesto': presupuesto?.toJson(),
      };
}
