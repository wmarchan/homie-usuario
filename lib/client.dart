import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:homie/models/categoria.dart';
import 'package:homie/models/chat/chat.dart';
import 'package:homie/models/chat/mensaje.dart';
import 'package:homie/models/chat_message/chat_message.dart';
import 'package:homie/models/notificacion_model.dart';

import 'package:dio/dio.dart';
import 'package:homie/models/pago/pago.dart';
import 'package:homie/models/presupuesto/presupuesto.dart';
import 'package:homie/models/visita_tecnica/visita_tecnica.dart';
import 'package:homie/singletons/AppData.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

// final baseUrl = "http://192.168.18.4:3000";
final baseUrl = "http://54.82.114.54:3000";

BaseOptions options =
    new BaseOptions(baseUrl: baseUrl, headers: {"accept": 'application/json'});

final Dio dio = Dio(options);

//methods

GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      "https://www.googleapis.com/auth/userinfo.profile",
    ],
    clientId:
        "417609917208-cac2nfnbmuj7su590tob5nok5f5vgu7b.apps.googleusercontent.com");

class Client {
  Future<AppData> login(String email, String password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var response = await dio.post("/login",
        data: {"email": email, "password": password, "acceso": 1});

    dio.options.headers["x-token"] = response.data["access_token"];

    appData.usuario = Usuario.fromJson(response.data);

    prefs.setString("email", email);
    prefs.setString("password", password);
    OneSignal.shared.setExternalUserId(appData.usuario.user!.id ?? "");
    return appData;
  }

  Future<bool> logout(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    GoogleSignIn().signOut();
    FacebookAuth.instance.logOut();
    await prefs.clear();
    return true;
  }

  Future<void> updateUser() async {
    var response = await dio.get("/user/getUser",
        queryParameters: {"id": appData.usuario.user!.id});

    appData.usuario.user = User.fromJson(response.data);
  }

  Future<AppData?> loginWithGoogle() async {
    try {
      await _googleSignIn.signOut();
      GoogleSignInAccount? account = await _googleSignIn.signIn();
      if (account != null) {
        GoogleSignInAuthentication authentication =
            await account.authentication;

        print(authentication.idToken);

        var response = await dio
            .post("/loginGoogle", data: {"token": authentication.idToken});

        dio.options.headers["x-token"] = response.data["access_token"];

        appData.usuario = Usuario.fromJson(response.data);
        OneSignal.shared.setExternalUserId(appData.usuario.user!.id ?? "");
        return appData;
      }
    } catch (error) {
      print(error);
      return null;
    }
  }

  Future<AppData?> loginWithFacebook() async {
    final LoginResult result = await FacebookAuth.instance.login(
      permissions: [
        "email",
        "public_profile",
      ],
    ); // by default we request the email and the public profile

    if (result.status == LoginStatus.success) {
      // you are logged
      final AccessToken accessToken = result.accessToken!;
      var response =
          await dio.post("/loginFacebook", data: {"token": accessToken.token});

      dio.options.headers["x-token"] = response.data["access_token"];

      appData.usuario = Usuario.fromJson(response.data);
      OneSignal.shared.setExternalUserId(appData.usuario.user!.id ?? "");
      return appData;
    } else {
      return null;
    }
  }

  Future<AppData?> loginWithApple() async {
    try {
      print(Uri.parse(
          "https://admin.tenedordelcielo.com/callbacks/sign_in_with_apple"));
      final credential = await SignInWithApple.getAppleIDCredential(
          scopes: [
            AppleIDAuthorizationScopes.email,
            AppleIDAuthorizationScopes.fullName,
          ],
          webAuthenticationOptions: WebAuthenticationOptions(
              clientId: "com.tenedorcielo.login",
              redirectUri: Uri.https("admin.tenedordelcielo.com",
                  "/callbacks/sign_in_with_apple")));

      SharedPreferences prefs = await SharedPreferences.getInstance();

      var response = await dio.post("/loginApple", data: {
        "code": credential.authorizationCode,
        "token": credential.identityToken,
        'firstName': credential.givenName,
        'lastName': credential.familyName,
        "email": credential.email,
        'useBundleId': Platform.isIOS || Platform.isMacOS ? 'true' : 'false',
      });

      dio.options.headers["x-token"] = response.data["access_token"];

      appData.usuario = Usuario.fromJson(response.data);
      OneSignal.shared.setExternalUserId(appData.usuario.user!.id ?? "");
      return appData;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<bool> registrar(dynamic json) async {
    try {
      var response = await dio.post("/user/registrar", data: json);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> actualizar(dynamic json) async {
    try {
      var response = await dio.post("/user/actualizar", data: json);

      appData.usuario.user = User.fromJson(response.data);

      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> calificar(dynamic json) async {
    try {
      var response = await dio.post("/calificar", data: json);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<List<NotificacionModel>> getNotificaciones() async {
    var response = await dio.get("/notificaciones", queryParameters: {
      "id": appData.usuario.user!.id,
    });
    List<NotificacionModel> notificaciones = List.empty(growable: true);
    response.data.forEach((e) {
      notificaciones.add(NotificacionModel.fromJson(e));
    });
    return notificaciones;
  }

  Future<Categoria> getCategoria(id) async {
    var response = await dio.get("/categoria", queryParameters: {"id": id});
    return Categoria.fromJson(response.data);
  }

  Future<bool> checkLogged() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    AccessToken? facebookAccessToken = await FacebookAuth.instance.accessToken;

    if (appData.usuario.accessToken != null) {
      return true;
    } else if (prefs.containsKey("email") && prefs.containsKey("password")) {
      try {
        await Client().login(
            prefs.getString("email") ?? "", prefs.getString("password") ?? "");
        return true;
      } catch (e) {
        return false;
      }
    } else if (await _googleSignIn.isSignedIn()) {
      Completer completer = new Completer();
      _googleSignIn.onCurrentUserChanged
          .listen((GoogleSignInAccount? account) async {
        if (account != null) {
          GoogleSignInAuthentication authentication =
              await account.authentication;

          var response = await dio
              .post("/loginGoogle", data: {"token": authentication.idToken});

          dio.options.headers["x-token"] = response.data["access_token"];

          appData.usuario = Usuario.fromJson(response.data);
          completer.complete(true);
        } else {
          completer.complete(false);
        }
      });
      _googleSignIn.signInSilently();
      return await completer.future;
    } else if (facebookAccessToken != null) {
      var response = await dio
          .post("/loginFacebook", data: {"token": facebookAccessToken.token});

      dio.options.headers["x-token"] = response.data["access_token"];

      appData.usuario = Usuario.fromJson(response.data);
      return true;
    } else {
      return false;
    }
  }

  Future<bool> forgotPassword(email) async {
    try {
      await dio.post("/forgot-password", data: {
        "email": email,
      });
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> verifyCode(code) async {
    try {
      await dio.post("/verify-code", data: {
        "code": code,
      });
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> updatePassword(data) async {
    try {
      await dio.post("/user/new-password", data: data);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<dynamic> getConfiguracion(key) async {
    var response =
        await dio.get("/get-configuracion", queryParameters: {"key": key});

    return response.data["value"];
  }

  Future<VisitaTecnica?> payCreditVisita(Map orden) async {
    try {
      var response = await dio.post("/pay-credit",
          data: {"orden": orden, "client": appData.usuario.user!.id});
      await Stripe.instance.initPaymentSheet(
          paymentSheetParameters: SetupPaymentSheetParameters(
        style: ThemeMode.light,
        merchantDisplayName: 'Homie',
        customerId: response.data!['customer'],
        paymentIntentClientSecret: response.data!['paymentIntent'],
        customerEphemeralKeySecret: response.data!['ephemeralKey'],
      ));
      await Future<void>.delayed(const Duration(milliseconds: 400));
      await Stripe.instance.presentPaymentSheet();

      var visitaResponse = await dio.post("/success-visita-stripe", data: {
        "orden": orden,
        "client": appData.usuario.user!.id,
        "paymentIntent": response.data!['id']
      });

      return VisitaTecnica.fromJson(visitaResponse.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<VisitaTecnica>> getVisitas(List<String> status) async {
    var response = await dio.get("/visitas",
        queryParameters: {"status": status, "id": appData.usuario.user!.id});
    List<VisitaTecnica> visitas = List.empty(growable: true);
    response.data.forEach((e) {
      visitas.add(VisitaTecnica.fromJson(e));
    });
    return visitas;
  }

  Future<bool> updateVisita(data) async {
    try {
      await dio.post("/visita/update", data: data);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<List<Pago>> getPagos() async {
    var response = await dio
        .get("/pagos", queryParameters: {"id": appData.usuario.user!.id});
    List<Pago> pagos = List.empty(growable: true);
    response.data.forEach((e) {
      pagos.add(Pago.fromJson(e));
    });
    return pagos;
  }

  Future<int> notificacionesNuevas() async {
    var response = await dio.get("/notificaciones_badge",
        queryParameters: {"id": appData.usuario.user!.id});

    return response.data;
  }

  Future<List<Presupuesto>> getPresupuestos(List<String> status) async {
    var response = await dio.get("/presupuesto/usuario/all",
        queryParameters: {"id": appData.usuario.user!.id, "status": status});
    List<Presupuesto> presupuestos = List.empty(growable: true);
    response.data.forEach((e) {
      presupuestos.add(Presupuesto.fromJson(e));
    });
    return presupuestos;
  }

  Future<bool> updatePresupuesto(data) async {
    try {
      await dio.post("/presupuesto/update", data: data);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<void> readNotifications() async {
    await dio.get("/read_notifications",
        queryParameters: {"id": appData.usuario.user!.id});

    return;
  }

  Future<VisitaTecnica?> payCreditPresupuesto(Presupuesto presupuesto) async {
    try {
      var response = await dio.post("/pay-credit-presupuesto",
          data: {"id": presupuesto.id, "client": appData.usuario.user!.id});
      await Stripe.instance.initPaymentSheet(
          paymentSheetParameters: SetupPaymentSheetParameters(
        style: ThemeMode.light,
        merchantDisplayName: 'Homie',
        customerId: response.data!['customer'],
        paymentIntentClientSecret: response.data!['paymentIntent'],
        customerEphemeralKeySecret: response.data!['ephemeralKey'],
      ));

      await Stripe.instance.presentPaymentSheet();

      var visitaResponse = await dio.post("/success-presupuesto-stripe", data: {
        "id": presupuesto.id,
        "visitaId": presupuesto.visitaTecnicaId,
        "client": appData.usuario.user!.id,
        "paymentIntent": response.data!['id']
      });

      return VisitaTecnica.fromJson(visitaResponse.data);
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<Chat>> getChats() async {
    var response = await dio
        .get("/chats", queryParameters: {"id": appData.usuario.user!.id});

    List<Chat> chats = List.empty(growable: true);
    response.data.forEach((e) {
      chats.add(Chat.fromJson(e));
    });
    return chats;
  }

  Future<ChatMessage> getChatMessages(id) async {
    var response = await dio.get("/chat-messages", queryParameters: {"id": id});

    return ChatMessage.fromJson(response.data);
  }

  Future<Mensaje?> sendMessage(data) async {
    try {
      var response = await dio.post("/chat-message", data: data);
      var mensaje = Mensaje.fromJson(response.data);
      return mensaje;
    } catch (e) {
      return null;
    }
  }

  Future<bool> eliminarCuenta() async {
    try {
      await dio
          .post("/eliminar-cuenta", data: {"id": appData.usuario.user!.id});
      return true;
    } catch (e) {
      return false;
    }
  }
}
